/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_system.c
 * @brief   System code.
 * @details Contains system initialization and shutdown routines
 *          and system shell commands.
 *
 * @addtogroup aos_system
 * @{
 */

#include <amiroos.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#if (AMIROOS_CFG_TESTS_ENABLE == true)
#include <ch_test.h>
#include <rt_test_root.h>
#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/**
 * @brief   Period of the system timer.
 */
#define SYSTIMER_PERIOD               (TIME_MAX_SYSTIME - CH_CFG_ST_TIMEDELTA)

/**
 * @brief   Width of the printable system info text.
 */
#define SYSTEM_INFO_WIDTH             80

/**
 * @brief   Width of the name column of the system info table.
 */
#define SYSTEM_INFO_NAMEWIDTH         20

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   Number of entries in the system shell input buffer.
 */
#define SYSTEM_SHELL_BUFFERENTRIES    (1 + AMIROOS_CFG_SHELL_HISTLENGTH)

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/**
 * @brief   Global system object.
 */
aos_system_t aos;

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * forward declarations
 */
#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
static int _shellcmd_configcb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _shellcmd_infocb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _shellcmd_shutdowncb(BaseSequentialStream* stream, int argc, char* argv[]);
#if (((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE)) || defined(__DOXYGEN__)
static int _shellcmd_cpuloadcb(BaseSequentialStream* stream, int argc, char* argv[]);
#endif /* ((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE) */
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)
static int _shellcmd_kerneltestcb(BaseSequentialStream* stream, int argc, char* argv[]);
#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/**
 * @brief   Timer to accumulate system uptime.
 */
static virtual_timer_t _systimer;

/**
 * @brief   Accumulated system uptime.
 */
static aos_timestamp_t _uptime;

/**
 * @brief   Timer register value of last accumulation.
 */
static systime_t _synctime;

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   System shell name.
 */
static const char _shell_name[] = "system shell";

/**
 * @brief   Shell thread working area.
 */
static THD_WORKING_AREA(_shell_wa, AMIROOS_CFG_SHELL_STACKSIZE);

/**
 * @brief   Shell input buffer.
 */
static char _shell_buffer[SYSTEM_SHELL_BUFFERENTRIES * AMIROOS_CFG_SHELL_LINEWIDTH];

/**
 * @brief   Shell command to retrieve system information.
 */
static AOS_SHELL_COMMAND(_shellcmd_info, "module:info", _shellcmd_infocb);

/**
 * @brief   Shell command to set or retrieve system configuration.
 */
static AOS_SHELL_COMMAND(_shellcmd_config, "module:config", _shellcmd_configcb);

/**
 * @brief   Shell command to shutdown the system.
 */
#if (AMIROOS_CFG_SSSP_ENABLE == true) || defined(__DOXYGEN__)
static AOS_SHELL_COMMAND(_shellcmd_shutdown, "system:shutdown", _shellcmd_shutdowncb);
#else /* (AMIROOS_CFG_SSSP_ENABLE == true) */
static AOS_SHELL_COMMAND(_shellcmd_shutdown, "module:shutdown", _shellcmd_shutdowncb);
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */

#if (((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE)) || defined(__DOXYGEN__)

/**
 * @brief   Shell command to read out CPU load.
 */
static AOS_SHELL_COMMAND(_shellcmd_cpuload, "module:cpuload", _shellcmd_cpuloadcb);

#endif /* ((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE) */

#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   Shell command to run a test of the ChibiOS/RT kernel.
 */
static AOS_SHELL_COMMAND(_shellcmd_kerneltest, "kernel:test", _shellcmd_kerneltestcb);

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Print a separator line.
 *
 * @param[in] stream    Stream to print to or NULL to print to all system streams.
 * @param[in] c         Character to use.
 * @param[in] n         Length of the separator line.
 *
 * @return  Number of characters printed.
 */
static unsigned int _printSystemInfoSeparator(BaseSequentialStream* stream, const char c, const unsigned int n)
{
  aosDbgCheck(stream != NULL);

  // print the specified character n times
  for (unsigned int i = 0; i < n; ++i) {
    streamPut(stream, (uint8_t)c);
  }
  streamPut(stream, '\n');

  return n+1;
}

/**
 * @brief   Print a system information line.
 * @details Prints a system information line with the following format:
 *            "<name>[spaces]fmt"
 *          The combined width of "<name>[spaces]" can be specified in order to align <fmt> on multiple lines.
 *          Note that there is not trailing newline added implicitely.
 *
 * @param[in] stream      Stream to print to or NULL to print to all system streams.
 * @param[in] name        Name of the entry/line.
 * @param[in] namewidth   Minimum width of the name column.
 * @param[in] fmt         Formatted string of information content.
 *
 * @return  Number of characters printed.
 */
static unsigned int _printSystemInfoLine(BaseSequentialStream* stream, const char* name, const unsigned int namewidth, const char* fmt, ...)
{
  aosDbgCheck(stream != NULL);
  aosDbgCheck(name != NULL);

  unsigned int n = 0;
  va_list ap;

  va_start(ap, fmt);
  n += (unsigned int)chprintf(stream, name);
  // print at least a single space character
  do {
    streamPut(stream, ' ');
    ++n;
  } while (n < namewidth);
  n += (unsigned int)chvprintf(stream, fmt, ap);
  va_end(ap);

  streamPut(stream, '\n');
  ++n;

  return n;
}

/**
 * @brief   Prints information about the system.
 *
 * @param[in] stream    Stream to print to.
 */
static void _printSystemInfo(BaseSequentialStream* stream)
{
  aosDbgCheck(stream != NULL);

  // local variables
#if (HAL_USE_RTC == TRUE)
  struct tm dt;
  aosSysGetDateTime(&dt);
#endif /* (HAL_USE_RTC == TRUE) */

  // print static information about module and operating system
  _printSystemInfoSeparator(stream, '=', SYSTEM_INFO_WIDTH);
  _printSystemInfoLine(stream, "Module", SYSTEM_INFO_NAMEWIDTH, "%s", BOARD_NAME);
#if defined(PLATFORM_NAME)
  _printSystemInfoLine(stream, "Platform", SYSTEM_INFO_NAMEWIDTH, "%s", PLATFORM_NAME);
#endif /* defined(PLATFORM_NAME) */
#if defined(PORT_CORE_VARIANT_NAME)
  _printSystemInfoLine(stream, "Core Variant", SYSTEM_INFO_NAMEWIDTH, "%s", PORT_CORE_VARIANT_NAME);
#endif /* defined(PORT_CORE_VARIANT_NAME) */
  _printSystemInfoLine(stream, "Architecture", SYSTEM_INFO_NAMEWIDTH, "%s", PORT_ARCHITECTURE_NAME);
  _printSystemInfoLine(stream, "Core Frequency", SYSTEM_INFO_NAMEWIDTH, "%u MHz", SystemCoreClock / 1000000);
  _printSystemInfoSeparator(stream, '-', SYSTEM_INFO_WIDTH);
#if (AMIROOS_CFG_SSSP_ENABLE == true)
  _printSystemInfoLine(stream, "AMiRo-OS" , SYSTEM_INFO_NAMEWIDTH, "%u.%u.%u %s (SSSP %u.%u)", AMIROOS_VERSION_MAJOR, AMIROOS_VERSION_MINOR, AMIROOS_VERSION_PATCH, AMIROOS_RELEASE_TYPE, AOS_SSSP_VERSION_MAJOR, AOS_SSSP_VERSION_MINOR);
#else /* (AMIROOS_CFG_SSSP_ENABLE == true) */
  _printSystemInfoLine(stream, "AMiRo-OS" , SYSTEM_INFO_NAMEWIDTH, "%u.%u.%u %s", AMIROOS_VERSION_MAJOR, AMIROOS_VERSION_MINOR, AMIROOS_VERSION_PATCH, AMIROOS_RELEASE_TYPE);
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */
  _printSystemInfoLine(stream, "AMiRo-LLD" , SYSTEM_INFO_NAMEWIDTH, "%u.%u.%u %s (periphAL %u.%u)", AMIROLLD_VERSION_MAJOR, AMIROLLD_VERSION_MINOR, AMIROLLD_VERSION_PATCH, AMIROLLD_RELEASE_TYPE, PERIPHAL_VERSION_MAJOR, PERIPHAL_VERSION_MINOR);
  _printSystemInfoLine(stream, "ChibiOS/RT" , SYSTEM_INFO_NAMEWIDTH, "%u.%u.%u %s", CH_KERNEL_MAJOR, CH_KERNEL_MINOR, CH_KERNEL_PATCH, (CH_KERNEL_STABLE == 1) ? "stable" : "non-stable");
  _printSystemInfoLine(stream, "ChibiOS/HAL", SYSTEM_INFO_NAMEWIDTH, "%u.%u.%u %s", CH_HAL_MAJOR, CH_HAL_MINOR, CH_HAL_PATCH, (CH_HAL_STABLE == 1) ? "stable" : "non-stable");
  _printSystemInfoLine(stream, "build type", SYSTEM_INFO_NAMEWIDTH,"%s", (AMIROOS_CFG_DBG == true) ? "debug" : "release");
  _printSystemInfoLine(stream, "Compiler" , SYSTEM_INFO_NAMEWIDTH, "%s %u.%u.%u", "GCC", __GNUC__, __GNUC_MINOR__, __GNUC_PATCHLEVEL__); // TODO: support other compilers than GCC
  _printSystemInfoLine(stream, "Compiled" , SYSTEM_INFO_NAMEWIDTH, "%s - %s", __DATE__, __TIME__);

  // print static information about the bootloader
#if (AMIROOS_CFG_BOOTLOADER != AOS_BOOTLOADER_NONE)
  _printSystemInfoSeparator(stream, '-', SYSTEM_INFO_WIDTH);
#endif /* (AMIROOS_CFG_BOOTLOADER != AOS_BOOTLOADER_NONE) */
#if (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)
  if (BL_CALLBACK_TABLE_ADDRESS->magicNumber == BL_MAGIC_NUMBER) {
    _printSystemInfoLine(stream, "AMiRo-BLT", SYSTEM_INFO_NAMEWIDTH, "%u.%u.%u %s (SSSP %u.%u)", BL_CALLBACK_TABLE_ADDRESS->vBootloader.major, BL_CALLBACK_TABLE_ADDRESS->vBootloader.minor, BL_CALLBACK_TABLE_ADDRESS->vBootloader.patch,
                         (BL_CALLBACK_TABLE_ADDRESS->vBootloader.identifier == BL_VERSION_ID_AMiRoBLT_Release) ? "stable" :
                         (BL_CALLBACK_TABLE_ADDRESS->vBootloader.identifier == BL_VERSION_ID_AMiRoBLT_ReleaseCandidate) ? "release candidate" :
                         (BL_CALLBACK_TABLE_ADDRESS->vBootloader.identifier == BL_VERSION_ID_AMiRoBLT_Beta) ? "beta" :
                         (BL_CALLBACK_TABLE_ADDRESS->vBootloader.identifier == BL_VERSION_ID_AMiRoBLT_Alpha) ? "alpha" :
                         (BL_CALLBACK_TABLE_ADDRESS->vBootloader.identifier == BL_VERSION_ID_AMiRoBLT_PreAlpha) ? "pre-alpha" :
                         "<release type unknown>",
                         BL_CALLBACK_TABLE_ADDRESS->vSSSP.major, BL_CALLBACK_TABLE_ADDRESS->vSSSP.minor);
#if (AMIROOS_CFG_SSSP_ENABLE == true)
    if (BL_CALLBACK_TABLE_ADDRESS->vSSSP.major != AOS_SSSP_VERSION_MAJOR) {
      const char* msg = "WARNING: AMiRo-BLT and AMiRo-OS implement incompatible SSSP versions!\n";
      stream ? chprintf(stream, msg) : aosprintf(msg);
    }
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */
    _printSystemInfoLine(stream, "Compiler", SYSTEM_INFO_NAMEWIDTH, "%s %u.%u.%u", (BL_CALLBACK_TABLE_ADDRESS->vCompiler.identifier == BL_VERSION_ID_GCC) ? "GCC" : "<compiler unknown>", BL_CALLBACK_TABLE_ADDRESS->vCompiler.major, BL_CALLBACK_TABLE_ADDRESS->vCompiler.minor, BL_CALLBACK_TABLE_ADDRESS->vCompiler.patch); // TODO: support other compilers than GCC
  } else {
    const char* msg = "WARNING: AMiRo-BLT incompatible or not available.\n";
    stream ? chprintf(stream, "%s", msg) : aosprintf("%s", msg);
  }
#endif /* (AMIROOS_CFG_BOOTLOADER == X) */

#if defined(AMIROOS_CFG_SYSINFO_HOOK)
  // print module specific information
  _printSystemInfoSeparator(stream, '-', SYSTEM_INFO_WIDTH);
  AMIROOS_CFG_SYSINFO_HOOK();
#endif /* defined(AMIROOS_CFG_SYSINFO_HOOK) */

  // print dynamic information about the module
  _printSystemInfoSeparator(stream, '-', SYSTEM_INFO_WIDTH);
#if (AMIROOS_CFG_SSSP_ENABLE == true)
  if (aos.sssp.moduleId != 0) {
    _printSystemInfoLine(stream, "Module ID", SYSTEM_INFO_NAMEWIDTH, "%u", aos.sssp.moduleId);
  } else {
    _printSystemInfoLine(stream, "Module ID", SYSTEM_INFO_NAMEWIDTH, "not available");
  }
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */
#if (HAL_USE_RTC == TRUE)
  _printSystemInfoLine(stream, "Date", SYSTEM_INFO_NAMEWIDTH, "%04u-%02u-%02u (%s)",
                       dt.tm_year + 1900,
                       dt.tm_mon + 1,
                       dt.tm_mday,
                       (dt.tm_wday == 0) ? "Sunday" : (dt.tm_wday == 1) ? "Monday" : (dt.tm_wday == 2) ? "Tuesday" : (dt.tm_wday == 3) ? "Wednesday" : (dt.tm_wday == 4) ? "Thursday" : (dt.tm_wday == 5) ? "Friday" : "Saturday");
  _printSystemInfoLine(stream, "Time", SYSTEM_INFO_NAMEWIDTH, "%02u:%02u:%02u", dt.tm_hour, dt.tm_min, dt.tm_sec);
#endif /* (HAL_USE_RTC == TRUE) */

  _printSystemInfoSeparator(stream, '=', SYSTEM_INFO_WIDTH);

  return;
}

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
/**
 * @brief   Callback function for the system:config shell command.
 *
 * @param[in] stream    The I/O stream to use.
 * @param[in] argc      Number of arguments.
 * @param[in] argv      List of pointers to the arguments.
 *
 * @return              An exit status.
 * @retval  AOS_OK                  The command was executed successfuly.
 * @retval  AOS_INVALIDARGUMENTS    There was an issue with the arguemnts.
 */
static int _shellcmd_configcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  aosDbgCheck(stream != NULL);

  // local variables
  int retval = AOS_INVALIDARGUMENTS;

  // if there are additional arguments
  if (argc > 1) {
    // if the user wants to set or retrieve the shell configuration
    if (strcmp(argv[1], "--shell") == 0) {
      // if the user wants to modify the shell configuration
      if (argc > 2) {
        // if the user wants to modify the prompt
        if (strcmp(argv[2], "prompt") == 0) {
          // there must be a further argument
          if (argc > 3) {
            // handle the option
            if (strcmp(argv[3], "text") == 0) {
              aos.shell.config &= ~AOS_SHELL_CONFIG_PROMPT_MINIMAL;
              retval = AOS_OK;
            }
            else if (strcmp(argv[3], "minimal") == 0) {
              aos.shell.config |= AOS_SHELL_CONFIG_PROMPT_MINIMAL;
              retval = AOS_OK;
            }
            else if (strcmp(argv[3], "notime") == 0) {
              aos.shell.config &= ~(AOS_SHELL_CONFIG_PROMPT_UPTIME | AOS_SHELL_CONFIG_PROMPT_DATETIME);
              retval = AOS_OK;
            }
            else if (strcmp(argv[3], "uptime") == 0) {
              aos.shell.config &= ~AOS_SHELL_CONFIG_PROMPT_DATETIME;
              aos.shell.config |= AOS_SHELL_CONFIG_PROMPT_UPTIME;
              retval = AOS_OK;
            }
            else if (strcmp(argv[3], "date&time") == 0) {
              aos.shell.config &= ~AOS_SHELL_CONFIG_PROMPT_UPTIME;
              aos.shell.config |= AOS_SHELL_CONFIG_PROMPT_DATETIME;
              retval = AOS_OK;
            }
            else {
              chprintf(stream, "unknown option '%s'\n", argv[3]);
              return AOS_INVALIDARGUMENTS;
            }
          }
        }
        // if the user wants to modify the string matching
        else if (strcmp(argv[2], "match") == 0) {
          // there must be a further argument
          if (argc > 3) {
            if (strcmp(argv[3], "casesensitive") == 0) {
              aos.shell.config |= AOS_SHELL_CONFIG_MATCH_CASE;
              retval = AOS_OK;
            }
            else if (strcmp(argv[3], "caseinsensitive") == 0) {
              aos.shell.config &= ~AOS_SHELL_CONFIG_MATCH_CASE;
              retval = AOS_OK;
            }
          }
        }
      }
      // if the user wants to retrieve the shell configuration
      else {
        chprintf(stream, "current shell configuration:\n");
        chprintf(stream, "\tprompt text:   %s\n",
                 (aos.shell.prompt != NULL) ? aos.shell.prompt : "n/a");
        char time[10];
        switch (aos.shell.config & (AOS_SHELL_CONFIG_PROMPT_UPTIME | AOS_SHELL_CONFIG_PROMPT_DATETIME)) {
          case AOS_SHELL_CONFIG_PROMPT_UPTIME:
            strcpy(time, "uptime"); break;
          case AOS_SHELL_CONFIG_PROMPT_DATETIME:
            strcpy(time, "date&time"); break;
          default:
            strcpy(time, "no time"); break;
        }
        chprintf(stream, "\tprompt style:  %s, %s\n",
                 (aos.shell.config & AOS_SHELL_CONFIG_PROMPT_MINIMAL) ? "minimal" : "text",
                 time);
        chprintf(stream, "\tinput method:  %s\n",
                 (aos.shell.config & AOS_SHELL_CONFIG_INPUT_OVERWRITE) ? "replace" : "insert");
        chprintf(stream, "\ttext matching: %s\n",
                 (aos.shell.config & AOS_SHELL_CONFIG_MATCH_CASE) ? "case sensitive" : "case insensitive");
        retval = AOS_OK;
      }
    }
# if (HAL_USE_RTC == TRUE)
    // if the user wants to configure the date or time
    else if (strcmp(argv[1], "--date&time") == 0 && argc == 4) {
      struct tm dt;
      aosSysGetDateTime(&dt);
      int val = atoi(argv[3]);
      if (strcmp(argv[2], "year") == 0 && val >= 1900) {
        dt.tm_year = val - 1900;
      }
      else if (strcmp(argv[2], "month") == 0 && val > 0 && val <= 12) {
        dt.tm_mon = val - 1;
      }
      else if (strcmp(argv[2], "day") == 0 && val > 0 && val <= 31) {
        dt.tm_mday = val;
      }
      else if (strcmp(argv[2], "hour") == 0 && val >= 0 && val < 24) {
        dt.tm_hour = val;
      }
      else if (strcmp(argv[2], "minute") == 0 && val >= 0 && val < 60) {
        dt.tm_min = val;
      }
      else if (strcmp(argv[2], "second") == 0 && val >= 0 && val < 60) {
        dt.tm_sec = val;
      }
      else {
        chprintf(stream, "unknown option '%s' or invalid value '%s'\n", argv[2], argv[3]);
        return AOS_INVALIDARGUMENTS;
      }
      dt.tm_wday = aosTimeDayOfWeekFromDate((uint16_t)dt.tm_mday, (uint8_t)dt.tm_mon+1, (uint16_t)dt.tm_year+1900) % DAYS_PER_WEEK;
      aosSysSetDateTime(&dt);

      // read and print new date and time
      aosSysGetDateTime(&dt);
      chprintf(stream, "date/time set to %04u-%02u-%02u %02u:%02u:%02u\n",
               dt.tm_year+1900, dt.tm_mon+1, dt.tm_mday,
               dt.tm_hour, dt.tm_min, dt.tm_sec);

      retval = AOS_OK;
    }
#endif /* (HAL_USE_RTC == TRUE) */
  }

  // print help, if required
  if (retval == AOS_INVALIDARGUMENTS) {
    chprintf(stream, "Usage: %s OPTION\n", argv[0]);
    chprintf(stream, "Options:\n");
    chprintf(stream, "  --help\n");
    chprintf(stream, "    Print this help text.\n");
    chprintf(stream, "  --shell [OPT [VAL]]\n");
    chprintf(stream, "    Set or retrieve shell configuration.\n");
    chprintf(stream, "    Possible OPTs and VALs are:\n");
    chprintf(stream, "      prompt text|minimal|uptime|date&time|notime\n");
    chprintf(stream, "        Configures the prompt.\n");
    chprintf(stream, "      match casesensitive|caseinsenitive\n");
    chprintf(stream, "        Configures string matching.\n");
#if (HAL_USE_RTC == TRUE)
    chprintf(stream, "  --date&time OPT VAL\n");
    chprintf(stream, "    Set the date/time value of OPT to VAL.\n");
    chprintf(stream, "    Possible OPTs are:\n");
    chprintf(stream, "      year\n");
    chprintf(stream, "      month\n");
    chprintf(stream, "      day\n");
    chprintf(stream, "      hour\n");
    chprintf(stream, "      minute\n");
    chprintf(stream, "      second\n");
#endif /* (HAL_USE_RTC == TRUE) */
  }

  return (argc > 1 && strcmp(argv[1], "--help") == 0) ? AOS_OK : retval;
}

/**
 * @brief   Callback function for the system:info shell command.
 *
 * @param[in] stream    The I/O stream to use.
 * @param[in] argc      Number of arguments.
 * @param[in] argv      List of pointers to the arguments.
 *
 * @return            An exit status.
 * @retval  AOS_OK    The command was executed successfully.
 */
static int _shellcmd_infocb(BaseSequentialStream* stream, int argc, char* argv[])
{
  aosDbgCheck(stream != NULL);

  (void)argc;
  (void)argv;

  // print system information
  _printSystemInfo(stream);

  // print time measurement precision
  chprintf(stream, "module time resolution: %uus\n", AOS_SYSTEM_TIME_RESOLUTION);

  // print system uptime
  aos_timestamp_t uptime;
  aosSysGetUptime(&uptime);
  chprintf(stream, "The system is running for\n");
  chprintf(stream, "%10u days\n", (uint32_t)(uptime / MICROSECONDS_PER_DAY));
  chprintf(stream, "%10u hours\n", (uint8_t)(uptime % MICROSECONDS_PER_DAY / MICROSECONDS_PER_HOUR));
  chprintf(stream, "%10u minutes\n", (uint8_t)(uptime % MICROSECONDS_PER_HOUR / MICROSECONDS_PER_MINUTE));
  chprintf(stream, "%10u seconds\n", (uint8_t)(uptime % MICROSECONDS_PER_MINUTE / MICROSECONDS_PER_SECOND));
  chprintf(stream, "%10u milliseconds\n", (uint16_t)(uptime % MICROSECONDS_PER_SECOND / MICROSECONDS_PER_MILLISECOND));
  chprintf(stream, "%10u microseconds\n", (uint16_t)(uptime % MICROSECONDS_PER_MILLISECOND / MICROSECONDS_PER_MICROSECOND));
#if (AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_MASTER != true) && (AMIROOS_CFG_PROFILE == true)
  chprintf(stream, "SSSP synchronization offset: %.3fus per %uus\n", (double)aosSsspGetSyncSkew(), AMIROOS_CFG_SSSP_SYSSYNCPERIOD);
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_MASTER != true) && (AMIROOS_CFG_PROFILE == true) */
  _printSystemInfoSeparator(stream, '=', SYSTEM_INFO_WIDTH);

  // print shell info
  chprintf(stream, "System shell information:\n");
  chprintf(stream, "\tcommands available:     %u\n", aosShellCountCommands(&aos.shell));
  chprintf(stream, "\tinput width:            %u characters\n", aos.shell.input.linewidth);
  chprintf(stream, "\tmaximum arguments:      %u\n", aos.shell.input.nargs);
  chprintf(stream, "\thistory size:           %u\n", aos.shell.input.nentries - 1);
#if (AMIROOS_CFG_DBG == true)
  chprintf(stream, "\tthread stack size:      %u bytes\n", aosThdGetStacksize(aos.shell.thread));
#if (CH_DBG_FILL_THREADS == TRUE)
  {
    const size_t utilization = aosThdGetStackPeakUtilization(aos.shell.thread);
    chprintf(stream, "\tstack peak utilization: %u bytes (%.2f%%)\n", utilization, (double)((float)utilization / (float)(aosThdGetStacksize(aos.shell.thread)) * 100.0f));
  }
#endif /* (CH_DBG_FILL_THREADS == TRUE) */
#endif /* (AMIROOS_CFG_DBG == true) */
  _printSystemInfoSeparator(stream, '=', SYSTEM_INFO_WIDTH);

  return AOS_OK;
}

/**
 * @brief   Callback function for the sytem:shutdown or module:shutdown shell command.
 *
 * @param[in] stream    The I/O stream to use.
 * @param[in] argc      Number of arguments.
 * @param[in] argv      List of pointers to the arguments.
 *
 * @return              An exit status.
 * @retval  AOS_OK                  The command was executed successfully.
 * @retval  AOS_INVALIDARGUMENTS   There was an issue with the arguments.
 */
static int _shellcmd_shutdowncb(BaseSequentialStream* stream, int argc, char* argv[])
{
  aosDbgCheck(stream != NULL);

#if (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_NONE)

  (void)argv;

  if (argc != 1) {
    // error
    chprintf(stream, "ERROR: no arguments allowed.\n");
    return AOS_INVALIDARGUMENTS;
  } else {
    // broadcast shutdown event
    chEvtBroadcastFlags(&aos.events.os, AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_MASK);
    // set terminate flag so no further prompt will be printed
    chThdTerminate(chThdGetSelfX());
    return AOS_OK;
  }

#elif (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)

  // print help text
  if (argc != 2 || strcmp(argv[1], "--help") == 0) {
    chprintf(stream, "Usage: %s OPTION\n", argv[0]);
    chprintf(stream, "Options:\n");
    chprintf(stream, "  --help\n");
    chprintf(stream, "    Print this help text.\n");
    chprintf(stream, "  --hibernate, -h\n");
    chprintf(stream, "    Shutdown to hibernate mode.\n");
    chprintf(stream, "    Least energy saving, but allows charging via pins.\n");
    chprintf(stream, "  --deepsleep, -d\n");
    chprintf(stream, "    Shutdown to deepsleep mode.\n");
    chprintf(stream, "    Minimum energy consumption while allowing charging via plug.\n");
    chprintf(stream, "  --transportation, -t\n");
    chprintf(stream, "    Shutdown to transportation mode.\n");
    chprintf(stream, "    Minimum energy consumption with all interrupts disabled (no charging).\n");
    chprintf(stream, "  --restart, -r\n");
    chprintf(stream, "    Shutdown and restart system.\n");

    return (argc != 2) ? AOS_INVALIDARGUMENTS : AOS_OK;
  }
  // handle argument
  else {
    if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--hibernate") == 0) {
      // broadcast shutdown event
      chEvtBroadcastFlags(&aos.events.os, AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_HIBERNATE);
      // set terminate flag so no further prompt will be printed
      chThdTerminate(chThdGetSelfX());
      return AOS_OK;
    }
    else if (strcmp(argv[1], "-d") == 0 || strcmp(argv[1], "--deepsleep") == 0) {
      // broadcast shutdown event
      chEvtBroadcastFlags(&aos.events.os, AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_DEEPSLEEP);
      // set terminate flag so no further prompt will be printed
      chThdTerminate(chThdGetSelfX());
      return AOS_OK;
    }
    else if (strcmp(argv[1], "-t") == 0 || strcmp(argv[1], "--transportation") == 0) {
      // broadcast shutdown event
      chEvtBroadcastFlags(&aos.events.os, AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_TRANSPORTATION);
      // set terminate flag so no further prompt will be printed
      chThdTerminate(chThdGetSelfX());
      return AOS_OK;
    }
    else if (strcmp(argv[1], "-r") == 0 || strcmp(argv[1], "--restart") == 0) {
      // broadcast shutdown event
      chEvtBroadcastFlags(&aos.events.os, AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_RESTART);
      // set terminate flag so no further prompt will be printed
      chThdTerminate(chThdGetSelfX());
      return AOS_OK;
    }
    else {
      chprintf(stream, "unknown argument %s\n", argv[1]);
      return AOS_INVALIDARGUMENTS;
    }
  }

#endif /* (AMIROOS_CFG_BOOTLOADER == X) */
}

#if (((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE)) || defined(__DOXYGEN__)

/**
 * @brief   Callback function for the module:cpuload shell command.
 *
 * @param[in] stream    The I/O stream to use.
 * @param[in] argc      Number of arguments.
 * @param[in] argv      List of pointers to arguments.
 *
 * @return      An exit status.
 */
static int _shellcmd_cpuloadcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  aosDbgCheck(stream != NULL);

  (void)argc;
  (void)argv;

  // local variables
  rttime_t sum = 0;
  thread_t* thd = aosThreadGetFirst();

  // accumulate system load
  do {
    sum += thd->stats.cumulative;
    thd = aosThreadGetNext(thd);
  } while (thd);
  sum += ch.kernel_stats.m_crit_thd.cumulative;
  sum += ch.kernel_stats.m_crit_isr.cumulative;

  // retrieve, calculate and print performance measures
  chprintf(stream, "threads & critical zones:\n");
  thd = aosThreadGetFirst();
  do {
#if (CH_CFG_USE_REGISTRY == TRUE)
    chprintf(stream, "\t%22s: %6.2f%%\n",
             (thd->name != NULL) ? thd->name : "<unnamed thread>",
             (double)((float)thd->stats.cumulative / (float)sum * 100.f));
#else
    chprintf(stream, "\t     thread 0x%08X: %6.2f%%\n", (unsigned int)thd, (double)((float)thd->stats.cumulative / (float)sum * 100.f));
#endif
    thd = aosThreadGetNext(thd);
  } while (thd);
  chprintf(stream, "\t%22s: %6.2f%%\n",
           "thread critical zones",
           (double)((float)ch.kernel_stats.m_crit_thd.cumulative / (float)sum * 100.f));
  chprintf(stream, "\t%22s: %6.2f%%\n",
           "ISR critical zones",
           (double)((float)ch.kernel_stats.m_crit_isr.cumulative / (float)sum * 100.f));

  // retrieve further real-time statistics
  chprintf(stream, "\nworst critical zones:\n");
  chprintf(stream, "\tthreads: %uus (%u clock cycles)\n",
           RTC2US(SystemCoreClock, ch.kernel_stats.m_crit_thd.worst),
           ch.kernel_stats.m_crit_thd.worst);
  chprintf(stream, "\t   ISRs: %uus (%u clock cycles)\n",
           RTC2US(SystemCoreClock, ch.kernel_stats.m_crit_isr.worst),
           ch.kernel_stats.m_crit_isr.worst);

  return 0;
}

#endif /* ((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE) */

#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   Callback function for the kernel:test shell command.
 *
 * @param[in] stream    The I/O stream to use.
 * @param[in] argc      Number of arguments.
 * @param[in] argv      List of pointers to the arguments.
 *
 * @return      An exit status.
 */
static int _shellcmd_kerneltestcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  aosDbgCheck(stream != NULL);

  (void)argc;
  (void)argv;

  msg_t retval = test_execute(stream, &rt_test_suite);

  return retval;
}

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

// suppress warning in case no interrupt GPIOs are defined
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
/**
 * @brief   Generic callback function for GPIO interrupts.
 *
 * @param[in] args   Pointer to the GPIO line identifier.
 */
static void _gpioCallback(void* args)
{
  aosDbgCheck((args != NULL) && (*((ioline_t*)args) != PAL_NOLINE) && (PAL_PAD(*((ioline_t*)args)) < sizeof(eventflags_t) * 8));

  chSysLockFromISR();
  chEvtBroadcastFlagsI(&aos.events.gpio, AOS_GPIOEVENT_FLAG(PAL_PAD(*((ioline_t*)args))));
  chSysUnlockFromISR();

  return;
}
#pragma GCC diagnostic pop

/**
 * @brief   Callback function for the uptime accumulation timer.
 *
 * @param[in] par   Generic parameter.
 */
static void _uptimeCallback(void* par)
{
  (void)par;

  chSysLockFromISR();
  // read current time in system ticks
  register const systime_t st = chVTGetSystemTimeX();
  // update the uptime variables
  _uptime += chTimeI2US(chTimeDiffX(_synctime, st));
  _synctime = st;
  // enable the timer again
  chVTSetI(&_systimer, SYSTIMER_PERIOD, &_uptimeCallback, NULL);
  chSysUnlockFromISR();

  return;
}

/**
 * @brief   Retrieve the number of active threads.
 *
 * @return  Number of active threads.
 */
static size_t _numActiveThreads(void)
{
  size_t threads = 0;

  thread_t* tp = aosThreadGetFirst();
  while (tp) {
    threads += (tp->state != CH_STATE_FINAL) ? 1 : 0;
    tp = aosThreadGetNext(tp);
  }

  return threads;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   AMiRo-OS system initialization.
 * @note    Must be called from the system control thread (usually main thread).
 *
 * @param[in] shellPrompt   String to be printed as prompt of the system shell.
 */
void aosSysInit(const char* shellPrompt)
{
  aosDbgCheck(shellPrompt != NULL || !AMIROOS_CFG_SHELL_ENABLE);

  /* set control thread to maximum priority */
  chThdSetPriority(AOS_THD_CTRLPRIO);

#if (AMIROOS_CFG_SSSP_ENABLE == true)
  aosSsspInit(&_uptime);
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */

  /* set local variables */
  chVTObjectInit(&_systimer);
#if (AMIROOS_CFG_SSSP_ENABLE == true)
  // uptime counter is started when SSSP proceeds to operation phase
  _synctime = 0;
  _uptime = 0;
#else /* (AMIROOS_CFG_SSSP_ENABLE == true) */
  // start the uptime counter
  chSysLock();
  aosSysStartUptimeS();
  chSysUnlock();
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */

  /* initialize aos configuration */
  aosIOStreamInit(&aos.iostream);
  chEvtObjectInit(&aos.events.gpio);
  chEvtObjectInit(&aos.events.os);

#if (AMIROOS_CFG_SHELL_ENABLE == true)

  /* init shell */
  aosShellInit(&aos.shell,
               _shell_name,
               shellPrompt,
               _shell_buffer,
               SYSTEM_SHELL_BUFFERENTRIES,
               AMIROOS_CFG_SHELL_LINEWIDTH,
               AMIROOS_CFG_SHELL_MAXARGS);
  // add system commands
  aosShellAddCommand(&aos.shell, &_shellcmd_config);
  aosShellAddCommand(&aos.shell, &_shellcmd_info);
  aosShellAddCommand(&aos.shell, &_shellcmd_shutdown);
#if (((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE)) || defined(__DOXYGEN__)
  aosShellAddCommand(&aos.shell, &_shellcmd_cpuload);
#endif /* ((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) && (CH_DBG_STATISTICS == TRUE) */
#if (AMIROOS_CFG_TESTS_ENABLE == true)
  aosShellAddCommand(&aos.shell, &_shellcmd_kerneltest);
#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

#else /* (AMIROOS_CFG_SHELL_ENABLE == true) */

  // suppress unused variable warnings
  (void)shellPrompt;

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

  return;
}

/**
 * @brief   Starts the system and all system threads.
 */
void aosSysStart(void)
{
  // print system information;
  _printSystemInfo((BaseSequentialStream*)&aos.iostream);
  aosprintf("\n");

#if (AMIROOS_CFG_SHELL_ENABLE == true)
  // start system shell thread
#if (CH_CFG_USE_THREADHIERARCHY == TRUE)
  aos.shell.thread = chThdCreateStatic(_shell_wa, sizeof(_shell_wa), AMIROOS_CFG_SHELL_THREADPRIO, aosShellThread, &aos.shell, &ch.mainthread);
#else /* (CH_CFG_USE_THREADHIERARCHY == TRUE) */
  aos.shell.thread = chThdCreateStatic(_shell_wa, sizeof(_shell_wa), AMIROOS_CFG_SHELL_THREADPRIO, aosShellThread, &aos.shell);
#endif /* (CH_CFG_USE_THREADHIERARCHY == TRUE) */
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

  return;
}

/**
 * @brief   Start the system uptime measurement.
 * @note    Must be called from a locked context.
 */
void aosSysStartUptimeS(void)
{
  chDbgCheckClassS();

  // start the uptime aggregation counter
  _synctime = chVTGetSystemTimeX();
  _uptime = 0;
  chVTSetI(&_systimer, SYSTIMER_PERIOD, &_uptimeCallback, NULL);

  return;
}

/**
 * @brief   Retrieves the system uptime.
 *
 * @param[out] ut   The system uptime.
 */
void aosSysGetUptimeX(aos_timestamp_t* ut)
{
  aosDbgCheck(ut != NULL);

  *ut = _uptime + chTimeI2US(chTimeDiffX(_synctime, chVTGetSystemTimeX()));

  return;
}

#if (HAL_USE_RTC == TRUE) || defined(__DOXYGEN__)

/**
 * @brief   retrieves the date and time from the MCU clock.
 *
 * @param[out] td   The date and time.
 */
void aosSysGetDateTime(struct tm* dt)
{
  aosDbgCheck(dt != NULL);

  RTCDateTime rtc;
  rtcGetTime(&MODULE_HAL_RTC, &rtc);
  rtcConvertDateTimeToStructTm(&rtc, dt, NULL);

  return;
}

/**
 * @brief   set the date and time of the MCU clock.
 *
 * @param[in] dt    The date and time to set.
 */
void aosSysSetDateTime(struct tm* dt)
{
  aosDbgCheck(dt != NULL);

  RTCDateTime rtc;
  rtcConvertStructTmToDateTime(dt, 0, &rtc);
  rtcSetTime(&MODULE_HAL_RTC, &rtc);

  return;
}

#endif /* (HAL_USE_RTC == TRUE) */

/**
 * @brief   Initializes/Acknowledges a system shutdown/restart request.
 * @note    This functions should be called from the thread with highest priority.
 *
 * @param[in] shutdown    Type of shutdown.
 */
void aosSysShutdownInit(aos_shutdown_t shutdown)
{
  // check arguments
  aosDbgCheck(shutdown != AOS_SHUTDOWN_NONE);

#if (AMIROOS_CFG_SSSP_ENABLE == true)

  // activate the PD signal only if this module initiated the shutdown
  if (shutdown != AOS_SHUTDOWN_PASSIVE) {
    aosSsspShutdownInit(true);
  }

  switch (shutdown) {
    case AOS_SHUTDOWN_PASSIVE:
      chEvtBroadcastFlags(&aos.events.os, AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_PASSIVE);
      aosprintf("shutdown request received...\n");
      break;
#if (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_NONE)
    case AOS_SHUTDOWN_ACTIVE:
      aosprintf("shutdown initiated...\n");
      break;
#elif (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)
    case AOS_SHUTDOWN_HIBERNATE:
      aosprintf("shutdown to hibernate mode...\n");
      break;
    case AOS_SHUTDOWN_DEEPSLEEP:
      aosprintf("shutdown to deepsleep mode...\n");
      break;
    case AOS_SHUTDOWN_TRANSPORTATION:
      aosprintf("shutdown to transportation mode...\n");
      break;
    case AOS_SHUTDOWN_RESTART:
      aosprintf("restarting system...\n");
      break;
#endif /* (AMIROOS_CFG_BOOTLOADER == X) */
    case AOS_SHUTDOWN_NONE:
      // must never occur
      aosDbgAssert(false);
      break;
  }

#else /* (AMIROOS_CFG_SSSP_ENABLE == true) */

  aosprintf("shutdown initiated...\n");

#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */

  return;
}

/**
 * @brief   Stops the system and all related threads (not the thread this function is called from).
 */
void aosSysStop(void)
{
  // wait until the calling thread is the only remaining active thread
#if (CH_CFG_NO_IDLE_THREAD == TRUE)
  while (_numActiveThreads() > 1) {
#else /* (CH_CFG_NO_IDLE_THREAD == TRUE) */
  while (_numActiveThreads() > 2) {
#endif /* (CH_CFG_NO_IDLE_THREAD == TRUE) */
    aosDbgPrintf("waiting for all threads to terminate...\n");
    chThdYield();
  }

  return;
}

/**
 * @brief   Deinitialize all system variables.
 */
void aosSysDeinit(void)
{
  return;
}

#if ((AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_SHUTDOWN != true)) ||                 \
    ((AMIROOS_CFG_SSSP_ENABLE != true) && (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)) || \
    defined(__DOXYGEN__)

/**
 * @brief   Finally shuts down the system and calls the bootloader callback function.
 * @note    This function should be called from the thtead with highest priority.
 *
 * @param[in] shutdown    Type of shutdown.
 */
void aosSysShutdownToBootloader(aos_shutdown_t shutdown)
{
  // check arguments
  aosDbgCheck(shutdown != AOS_SHUTDOWN_NONE);

  // disable all interrupts
  irqDeinit();

#if (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)
  // validate AMiRo-BLT
  if ((BL_CALLBACK_TABLE_ADDRESS->magicNumber == BL_MAGIC_NUMBER) &&
      (BL_CALLBACK_TABLE_ADDRESS->vBootloader.major == BL_VERSION_MAJOR) &&
      (BL_CALLBACK_TABLE_ADDRESS->vBootloader.minor >= BL_VERSION_MINOR)) {
    // call bootloader callback depending on arguments
    switch (shutdown) {
#if (AMIROOS_CFG_SSSP_ENABLE == true)
      case AOS_SHUTDOWN_PASSIVE:
        BL_CALLBACK_TABLE_ADDRESS->cbHandleShutdownRequest();
        break;
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */
      case AOS_SHUTDOWN_HIBERNATE:
        BL_CALLBACK_TABLE_ADDRESS->cbShutdownHibernate();
        break;
      case AOS_SHUTDOWN_DEEPSLEEP:
        BL_CALLBACK_TABLE_ADDRESS->cbShutdownDeepsleep();
        break;
      case AOS_SHUTDOWN_TRANSPORTATION:
        BL_CALLBACK_TABLE_ADDRESS->cbShutdownTransportation();
        break;
      case AOS_SHUTDOWN_RESTART:
        BL_CALLBACK_TABLE_ADDRESS->cbShutdownRestart();
        break;
      // must never occur
      case AOS_SHUTDOWN_NONE:
      default:
        break;
    }
  } else {
    // fallback if AMiRo-BLT was found to be invalid
    aosprintf("ERROR: AMiRo-BLT incompatible or not available!\n");
    aosThdMSleep(10);
    chSysDisable();
  }
#endif /* (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT) */

  return;
}

#endif /* ((AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_SHUTDOWN != true)) || ((AMIROOS_CFG_SSSP_ENABLE != true) && (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)) */

/**
 * @brief   Generic callback function for GPIO interrupts.
 *
 * @return  Pointer to the callback function.
 */
palcallback_t aosSysGetStdGpioCallback(void)
{
  return _gpioCallback;
}

/** @} */
