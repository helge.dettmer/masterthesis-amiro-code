/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_thread.c
 * @brief   Thread code.
 *
 * @addtogroup aos_threads
 * @{
 */

#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Lets the calling thread sleep until the specifide system uptime.
 *
 * @param[in] t     Deadline until the thread will sleep.
 */
void aosThdSleepUntilS(const aos_timestamp_t t)
{
  aos_timestamp_t uptime;

  // get the current system uptime
  aosSysGetUptimeX(&uptime);

  // while the remaining time is too long, it must be split into multiple sleeps
  while ( (t > uptime) && ((t - uptime) > AOS_THD_MAX_SLEEP_US) ) {
    chThdSleepS(chTimeUS2I(AOS_THD_MAX_SLEEP_US));
    aosSysGetUptimeX(&uptime);
  }

  // sleep the remaining time
  if (t > uptime) {
    sysinterval_t rest = chTimeUS2I(t - uptime);
    if (rest > TIME_IMMEDIATE) {
      chThdSleepS(rest);
    }
  }

  return;
}

#if ((AMIROOS_CFG_DBG == true) && (CH_DBG_FILL_THREADS == TRUE)) || defined(__DOXYGEN__)
/**
 * @brief   Calculate the peak stack utilization for a specific thread so far in bytes.
 *
 * @param[in] thread    Thread to calculate the stack utilization for.
 *
 * @return  Absolute peak stack utilization in bytes.
 */
size_t aosThdGetStackPeakUtilization(thread_t* thread)
{
  aosDbgCheck(thread != NULL);

  // iterator through the stack
  // note: since the stack is filled from top to bottom, the loop searches for the first irregular byte from the bottom (stack end).
  for (uint8_t* ptr = (uint8_t*)thread->wabase; ptr < (uint8_t*)thread->wabase + aosThdGetStacksize(thread); ++ptr) {
    if (*ptr != CH_DBG_STACK_FILL_VALUE) {
      return aosThdGetStacksize(thread) - (size_t)(--ptr - (uint8_t*)thread->wabase);
    }
  }

  return 0;
}
#endif /* (AMIROOS_CFG_DBG == true) && (CH_DBG_FILL_THREADS == TRUE) */

#if ((CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE)) || defined(__DOXYGEN__)

/**
 * @brief   Retrieve the first/root thread in the system.
 *
 * @return  Pointer to the first/root thread.
 */
thread_t* aosThreadGetFirst(void)
{
#if (CH_CFG_USE_REGISTRY == TRUE)
  return chRegFirstThread();
#elif (CH_CFG_USE_THREADHIERARCHY == TRUE)
  thread_t* tp = chThdGetSelfX();
  while (tp->parent) {
    tp = tp->parent;
  }
  return tp;
#endif
}

/**
 * @brief   Retrieve the next thread in the system.
 *
 * @param[in] thread  Current thread to retrieve the 'successor' to.
 *
 * @return  Pointer to the next thread, or NULL if there are no further threads.
 */
thread_t* aosThreadGetNext(thread_t* thread)
{
  aosDbgCheck(thread != NULL);

#if (CH_CFG_USE_REGISTRY == TRUE)
  return chRegNextThread(thread);
#elif (CH_CFG_USE_THREADHIERARCHY == TRUE)
  if (thread->children != NULL) {
    return thread->children;
  } else {
    while (thread != NULL && thread->sibling == NULL) {
      thread = thread->parent;
    }
    return (thread != NULL) ? thread->sibling : NULL;
  }
#endif
}

#endif /* (CH_CFG_USE_REGISTRY == TRUE) || (CH_CFG_USE_THREADHIERARCHY == TRUE) */

/** @} */
