/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_timer.c
 * @brief   Timer code.
 * @details Implementation of aos_timer_t and aos_periodictimer_t functions.
 *
 * @addtogroup aos_timers
 * @{
 */

#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/*
 * forward declarations
 */
static void _intermediateCb(void* timer);
static void _triggerCb(void *timer);

/**
 * @brief   Setup a timer according to its configuration.
 *
 * @param[in] timer   Pointer to the timer to setup.
 */
static void _setupTimer(aos_timer_t* timer)
{
  // local variables
  aos_timestamp_t uptime;
  aos_timestamp_t timedelta;

  // get current system uptime
  aosSysGetUptimeX(&uptime);

  // if this is a periodic timer
  if (timer->interval > 0) {
    // if the periodic timer is already initialized
    if (timer->triggertime > 0) {
      timer->triggertime += timer->interval;
    }
    // if the periodic timer is not initialized yet
    else {
      timer->triggertime = uptime + timer->interval;
    }
  }

  // calculate the time delta (may be 'negative' at this point)
  timedelta = timer->triggertime - uptime;

  // if the trigger time is more than TIME_IMMEDIATE in the future
  if ((timer->triggertime > uptime) && (timedelta > chTimeI2US(TIME_IMMEDIATE))) {
    // split the time delta if necessary
    if (timedelta > AOS_TIMER_MAX_INTERVAL_US) {
      chVTSetI(&timer->vt, chTimeUS2I(AOS_TIMER_MAX_INTERVAL_US), _intermediateCb, timer);
    } else {
      chVTSetI(&timer->vt, chTimeUS2I(timedelta), _triggerCb, timer);
    }
  } else {
    // trigger immediately
    timer->callback(timer->cbparam);
  }

  return;
}

/**
 * @brief   Callback function for intermediate interrupts.
 * @details This is required if the desired time to trigger is too far in the future so that the interval must be split.
 *
 * @param[in] timer   Pointer to a aos_timer_t to reactivate.
 */
static void _intermediateCb(void* timer)
{
  chSysLockFromISR();
  _setupTimer((aos_timer_t*)timer);
  chSysUnlockFromISR();
}

/**
 * @brief   Callback function for the trigger event of the timer.
 *
 * @param[in] timer   Pointer to a aos_timer_t to call its callback.
 */
static void _triggerCb(void *timer)
{
  chSysLockFromISR();
  ((aos_timer_t*)timer)->callback(((aos_timer_t*)timer)->cbparam);
  // reenable periodic timers
  if (((aos_timer_t*)timer)->interval > 0) {
    _setupTimer((aos_timer_t*)timer);
  }
  chSysUnlockFromISR();
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initialize a aos_timer_t object.
 *
 * @param[in] timer   The timer to initialize.
 */
void aosTimerInit(aos_timer_t* timer)
{
  aosDbgAssert(timer != NULL);

  chVTObjectInit(&(timer->vt));
  timer->triggertime = 0;
  timer->interval = 0;
  timer->callback = NULL;
  timer->cbparam = NULL;

  return;
}

/**
 * @brief   Set timer to trigger at an absolute system time.
 *
 * @param[in] timer   Pointer to the timer to set.
 * @param[in] uptime  Absolute system time for the timer to trigger.
 * @param[in] cb      Pointer to a callback function to be called.
 *                    In contrast to ChibiOS callback functions, this gets called from an already ISR locked context.
 *                    Thus it may not contain any chSysLockX() or chSysUnlockX() calls and so forth.
 * @param[in] par     Pointer to a parameter for the callback function (may be NULL).
 */
void aosTimerSetAbsoluteI(aos_timer_t *timer, const aos_timestamp_t uptime, vtfunc_t cb, void *par)
{
  aosDbgCheck(timer != NULL);
  aosDbgCheck(cb != NULL);

  timer->triggertime = uptime;
  timer->interval = 0;
  timer->callback = cb;
  timer->cbparam = par;
  _setupTimer(timer);

  return;
}

/**
 * @brief   Set timer to trigger after a relative interval.
 *
 * @param[in] timer   Pointer to the timer to set.
 * @param[in] offset  Relative interval to set for the timer to trigger.
 * @param[in] cb      Pointer to a callback function to be called.
 *                    In contrast to ChibiOS callback functions, this gets called from an already ISR locked context.
 *                    Thus it may not contain any chSysLockX() or chSysUnlockX() calls and so forth.
 * @param[in] par     Pointer to a parameter for the callback function (may be NULL).
 */
void aosTimerSetIntervalI(aos_timer_t *timer, const aos_interval_t offset, vtfunc_t cb, void *par)
{
  aosDbgCheck(timer != NULL);
  aosDbgCheck(cb != NULL);

  aos_timestamp_t uptime;

  aosSysGetUptimeX(&uptime);
  timer->triggertime = uptime + offset;
  timer->interval = 0;
  timer->callback = cb;
  timer->cbparam = par;
  _setupTimer(timer);

  return;
}

/**
 * @brief   Set timer to trigger after a long relative interval.
 *
 * @param[in] timer   Pointer to the timer to set.
 * @param[in] offset  Long interval value to set for the timer to trigger.
 * @param[in] cb      Pointer to a callback function to be called.
 *                    In contrast to ChibiOS callback functions, this gets called from an already ISR locked context.
 *                    Thus it may not contain any chSysLockX() or chSysUnlockX() calls and so forth.
 * @param[in] par     Pointer to a parameter for the callback function (may be NULL).
 */
void aosTimerSetLongIntervalI(aos_timer_t *timer, const aos_longinterval_t offset, vtfunc_t cb, void *par)
{
  aosDbgCheck(timer != NULL);
  aosDbgCheck(cb != NULL);

  aos_timestamp_t uptime;

  aosSysGetUptimeX(&uptime);
  timer->triggertime = uptime + offset;
  timer->interval = 0;
  timer->callback = cb;
  timer->cbparam = par;
  _setupTimer(timer);

  return;
}

/**
 * @brief   Set timer to trigger periodically in the specified interval.
 *
 * @param[in] timer     Pointer to the periodic timer to set.
 * @param[in] interval  Interval for the periodic timer to trigger periodically.
 * @param[in] cb        Pointer to a callback function to be called.
 *                      In contrast to ChibiOS callback functions, this gets called from an already ISR locked context.
 *                      Thus it may not contain any chSysLockX() or chSysUnlockX() calls and so forth.
 * @param[in] par       Pointer to a parameter for the callback function (may be NULL).
 */
void aosTimerPeriodicIntervalI(aos_timer_t* timer, const aos_interval_t interval, vtfunc_t cb, void* par)
{
  aosDbgCheck(timer != NULL);
  aosDbgCheck(interval > TIME_IMMEDIATE);
  aosDbgCheck(cb != NULL);

  timer->triggertime = 0;
  timer->interval = interval;
  timer->callback = cb;
  timer->cbparam = par;
  _setupTimer(timer);

  return;
}

/**
 * @brief   Set timer to trigger periodically in the specified interval.
 *
 * @param[in] timer     Pointer to the periodic timer to set.
 * @param[in] interval  Long interval value for the periodic timer to trigger periodically.
 * @param[in] cb        Pointer to a callback function to be called.
 *                      In contrast to other callback functions, this get called from an already ISR locked context.
 *                      This it may not contain any chSysLockX() or chSysUnlockX() calls.
 * @param[in] par       Pointer to a parameter for the callback function (may be NULL).
 */
void aosTimerPeriodicLongIntervalI(aos_timer_t* timer, const aos_longinterval_t interval, vtfunc_t cb, void* par)
{
  aosDbgCheck(timer != NULL);
  aosDbgCheck(interval > TIME_IMMEDIATE);
  aosDbgCheck(cb != NULL);

  timer->triggertime = 0;
  timer->interval = interval;
  timer->callback = cb;
  timer->cbparam = par;
  _setupTimer(timer);

  return;
}

/** @} */
