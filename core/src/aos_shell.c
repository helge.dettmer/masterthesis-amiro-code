/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_shell.c
 * @brief   Shell code.
 * @details Shell code as well as shell related channels and streams.
 *
 * @addtogroup aos_shell
 * @{
 */

#include <amiroos.h>
#include <string.h>

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/**
 * @brief   Size of the escape sequence buffer.
 */
#if !defined(AOS_SHELL_ESCSEQUENCE_LENGTH) || defined(__DOXYGEN__)
#define AOS_SHELL_ESCSEQUENCE_LENGTH            8
#endif

/**
 * @brief   The character the input buffer is initialized with.
 */
#define INBUF_INIT_CHAR                         '\x07'

/**
 * @brief   Event mask to be set on OS related events.
 */
#define EVENTMASK_OS                            EVENT_MASK(0)

/**
 * @brief   Event mask to be set on a input event.
 */
#define EVENTMASK_INPUT                         EVENT_MASK(1)

/**
 * @brief   String that defines the INSERT key as specified by VT100.
 */
#define KEYSTRING_INSERT                        "\x1B\x5B\x32\x7E"

/**
 * @brief   String that defines the DEL key as specified by VT100.
 */
#define KEYSTRING_DELETE                        "\x1B\x5B\x33\x7E"

/**
 * @brief   String that defines the HOME key as specified by VT100.
 */
#define KEYSTRING_HOME                          "\x1B\x5B\x48"

/**
 * @brief   String that defines the END key as specified by VT100.
 */
#define KEYSTRING_END                           "\x1B\x5B\x46"

/**
 * @brief   String that defines the PGUP key as specified by VT100.
 */
#define KEYSTRING_PAGEUP                        "\x1B\x5B\x35\x7E"

/**
 * @brief   String that defines the PGUP key as specified by VT100.
 */
#define KEYSTRING_PAGEDOWN                      "\x1B\x5B\x36\x7E"

/**
 * @brief   String that defines the 'arrow down' key as specified by VT100.
 */
#define KEYSTRING_ARROWUP                       "\x1B\x5B\x41"

/**
 * @brief   String that defines the 'arrow up' key as specified by VT100.
 */
#define KEYSTRING_ARROWDOWN                     "\x1B\x5B\x42"

/**
 * @brief   String that defines the 'arrow left' key as specified by VT100.
 */
#define KEYSTRING_ARROWLEFT                     "\x1B\x5B\x44"

/**
 * @brief   String that defines the 'arrow right' key as specified by VT100.
 */
#define KEYSTRING_ARROWRIGHT                    "\x1B\x5B\x43"

/**
 * @brief   String that defines the CRTL + 'arrow up' key combination as specified by VT100.
 */
#define KEYSTRING_CTRL_ARROWUP                  "\x1B\x5B\x31\x3B\x35\x41"

/**
 * @brief   String that defines the CRTL + 'arrow down' key combination as specified by VT100.
 */
#define KEYSTRING_CTRL_ARROWDOWN                "\x1B\x5B\x31\x3B\x35\x42"

/**
 * @brief   String that defines the CRTL + 'arrow left' key combination as specified by VT100.
 */
#define KEYSTRING_CTRL_ARROWLEFT                "\x1B\x5B\x31\x3B\x35\x44"

/**
 * @brief   String that defines the CRTL + 'arrow right' key combination as specified by VT100.
 */
#define KEYSTRING_CTRL_ARROWRIGHT               "\x1B\x5B\x31\x3B\x35\x43"

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/*
 * forward declarations
 */
static size_t _channelwrite(void *instance, const uint8_t *bp, size_t n);
static size_t _channelread(void *instance, uint8_t *bp, size_t n);
static msg_t _channelput(void *instance, uint8_t b);
static msg_t _channelget(void *instance);
static msg_t _channelputt(void *instance, uint8_t b, sysinterval_t time);
static msg_t _channelgett(void *instance, sysinterval_t time);
static size_t _channelwritet(void *instance, const uint8_t *bp, size_t n, sysinterval_t time);
static size_t _channelreadt(void *instance, uint8_t *bp, size_t n, sysinterval_t time);
static msg_t _channelctl(void *instance, unsigned int operation, void *arg);
static size_t _streamwrite(void *instance, const uint8_t *bp, size_t n);
static size_t _stremread(void *instance, uint8_t *bp, size_t n);
static msg_t _streamput(void *instance, uint8_t b);
static msg_t _streamget(void *instance);

static const struct AosShellChannelVMT _channelvmt = {
  (size_t) 0,
  _channelwrite,
  _channelread,
  _channelput,
  _channelget,
  _channelputt,
  _channelgett,
  _channelwritet,
  _channelreadt,
  _channelctl,
};

static const struct AosShellStreamVMT _streamvmt = {
  (size_t) 0,
  _streamwrite,
  _stremread,
  _streamput,
  _streamget,
};

/**
 * @brief   Enumerator of special keyboard keys.
 */
typedef enum special_key {
  KEY_UNKNOWN,          /**< any/unknow key */
  KEY_AMBIGUOUS,        /**< key is ambiguous */
  KEY_TAB,              /**< tabulator key */
  KEY_BACKSPACE,        /**< backspace key */
  KEY_INSERT,           /**< insert key */
  KEY_DELETE,           /**< delete key */
  KEY_ESCAPE,           /**< escape key */
  KEY_HOME,             /**< home key */
  KEY_END,              /**< end key */
  KEY_PAGEUP,           /**< page up key */
  KEY_PAGEDOWN,         /**< page down key */
  KEY_ARROWUP,          /**< arrow up key */
  KEY_ARROWDOWN,        /**< arrow down key */
  KEY_ARROWLEFT,        /**< arrow left key */
  KEY_ARROWRIGHT,       /**< arrow right key */
  KEY_CTRL_ARROWUP,     /**< CTRL + arrow up key */
  KEY_CTRL_ARROWDOWN,   /**< CTRL + arrow down key */
  KEY_CTRL_ARROWLEFT,   /**< CTRL + arrow left key */
  KEY_CTRL_ARROWRIGHT,  /**< CTRL + arrow right key */
  KEY_CTRL_C,           /**< CTRL + C key */
} special_key_t;

/**
 * @brief   Enumerator for case (in)sensitive character matching.
 */
typedef enum charmatch {
  CHAR_MATCH_NOT    = 0,  /**< Characters do not match at all. */
  CHAR_MATCH_NCASE  = 1,  /**< Characters would match case insensitive. */
  CHAR_MATCH_CASE   = 2,  /**< Characters do match with case. */
} charmatch_t;

/**
 * @brief   Enumerator to encode shell actions.
 */
typedef enum aos_shellaction {
  ACTION_NONE,                  /**< No action at all. */
  ACTION_READCHAR,              /**< Read a printable character. */
  ACTION_AUTOCOMPLETE,          /**< Automatically comlete input by using available command. */
  ACTION_SUGGEST,               /**< Suggest matching available commands. */
  ACTION_EXECUTE,               /**< Execute input. */
  ACTION_DELETEBACKWARD,        /**< Delete a single character backwards. */
  ACTION_DELETEFORWARD,         /**< Delete a single character forwards. */
  ACTION_CLEAR,                 /**< Clear the input. */
  ACTION_RECALLPREVIOUS,        /**< Recall the previous (older) entry in the history. */
  ACTION_RECALLNEXT,            /**< Recall the next (more recent) entry in the history. */
  ACTION_RECALLOLDEST,          /**< Recall the oldest entry in the history. */
  ACTION_RECALLCURRENT,         /**< Recall the current input. */
  ACTION_CURSORLEFT,            /**< Move cursor one character to the left. */
  ACTION_CURSORRIGHT,           /**< Move cursor one character to the right. */
  ACTION_CURSORWORDLEFT,        /**< Move cursor one word to the left. */
  ACTION_CURSORWORDRIGHT,       /**< Move cursor one word to the right. */
  ACTION_CURSOR2END,            /**< Move cursor to the very right. */
  ACTION_CURSOR2START,          /**< Move cursor to the very left. */
  ACTION_RESET,                 /**< Reset the current input. */
  ACTION_INSERTTOGGLE,          /**< Toggle insertion mode. */
  ACTION_ESCSTART,              /**< Start an escape sequence (special keys). */
  ACTION_PRINTUNKNOWNSEQUENCE,  /**< Print an unknown escape sequence. */
} action_t;

/**
 * @brief   Struct that holds most important runtime data for the shell.
 * @details The structure is to be used by the shell thread main function as some kind of structured stack, which can be easily passed to other functions.
 */
typedef struct runtimedata {
  /**
   * @brief   Data related to the current input.
   */
  struct {
    /**
     * @brief   Length of the input.
     */
    size_t length;

    /**
     * @brief   Current position of the cursor in the input line.
     */
    size_t cursorpos;

    /**
     * @brief   Buffer to store escape sequences, which describe special characters.
     */
    char escseq[AOS_SHELL_ESCSEQUENCE_LENGTH];
  } input;

  /**
   * @brief   Data related to the entry or history buffer.
   */
  struct {
    /**
     * @brief   Current entry to be filled and executed.
     */
    size_t current;

    /**
     * @brief   Selected entry in the 'history' as preview.
     * @details A value of 0 indicates, that the line is cleared as a preview.
     *          A value of 1 indicates, that the current entry is selected.
     *          A value of t>1 indicates, that the entry t-1 in the past is selected.
     *          The value must never be greater than the number of entries available, of course.
     */
    size_t selected;

    /**
     * @brief   Selected entry in the 'history' that has been edited by the user.
     *          A value of 0 indicates, that there was no modification by the user yet (i.e. charcters, deletions or autofill).
     *          A value of 1 indicates, that the current entry was edited.
     *          A value of t>1 indicated, that a history entry was recalled and then edited.
     */
    size_t edited;
  } buffer;

  /**
   * @brief   The last action executed by the shell.
   */
  action_t lastaction;
} runtimedata_t;

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Implementation of the BaseAsynchronous write() method (inherited from BaseSequentialStream).
 */
static size_t _channelwrite(void *instance, const uint8_t *bp, size_t n)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_OUTPUT_ENABLED) {
    return streamWrite(((AosShellChannel*)instance)->asyncchannel, bp, n);
  } else {
    return 0;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous read() method (inherited from BaseSequentialStream).
 */
static size_t _channelread(void *instance, uint8_t *bp, size_t n)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_INPUT_ENABLED) {
    return streamRead(((AosShellChannel*)instance)->asyncchannel, bp, n);
  } else {
    return 0;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous put() method (inherited from BaseSequentialStream).
 */
static msg_t _channelput(void *instance, uint8_t b)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_OUTPUT_ENABLED) {
    return streamPut(((AosShellChannel*)instance)->asyncchannel, b);
  } else {
    return MSG_RESET;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous get() method (inherited from BaseSequentialStream).
 */
static msg_t _channelget(void *instance)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_INPUT_ENABLED) {
    return streamGet(((AosShellChannel*)instance)->asyncchannel);
  } else {
    return MSG_RESET;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous putt() method.
 */
static msg_t _channelputt(void *instance, uint8_t b, sysinterval_t time)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_OUTPUT_ENABLED) {
    return chnPutTimeout(((AosShellChannel*)instance)->asyncchannel, b, time);
  } else {
    return MSG_RESET;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous gett() method.
 */
static msg_t _channelgett(void *instance, sysinterval_t time)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_INPUT_ENABLED) {
    return chnGetTimeout(((AosShellChannel*)instance)->asyncchannel, time);
  } else {
    return MSG_RESET;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous writet() method.
 */
static size_t _channelwritet(void *instance, const uint8_t *bp, size_t n, sysinterval_t time)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_OUTPUT_ENABLED) {
    return chnWriteTimeout(((AosShellChannel*)instance)->asyncchannel, bp, n, time);
  } else {
    return 0;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronous readt() method.
 */
static size_t _channelreadt(void *instance, uint8_t *bp, size_t n, sysinterval_t time)
{
  if (((AosShellChannel*)instance)->flags & AOS_SHELLCHANNEL_INPUT_ENABLED) {
    return chnReadTimeout(((AosShellChannel*)instance)->asyncchannel, bp, n, time);
  } else {
    return 0;
  }
}

/**
 * @brief   Implementation of the BaseAsynchronousChannel ctl() method.
 */
static msg_t _channelctl(void *instance, unsigned int operation, void *arg)
{
  (void) instance;

  switch (operation) {
  case CHN_CTL_NOP:
    osalDbgCheck(arg == NULL);
    break;
  case CHN_CTL_INVALID:
    osalDbgAssert(false, "invalid CTL operation");
    break;
  default:
    break;
  }
  return MSG_OK;
}

/**
 * @brief   Implementation of the BaseSequentialStream write() method.
 */
static size_t _streamwrite(void *instance, const uint8_t *bp, size_t n)
{
  aosDbgCheck(instance != NULL);

  // local variables
  AosShellChannel* channel = ((AosShellStream*)instance)->channel;
  size_t bytes;
  size_t maxbytes = 0;

  // iterate through the list of channels
  while (channel != NULL) {
    bytes = streamWrite(channel, bp, n);
    maxbytes = (bytes > maxbytes) ? bytes : maxbytes;
    channel = channel->next;
  }

  return maxbytes;
}

/**
 * @brief   Implementation of the BaseSequentialStream read() method.
 */
static size_t _stremread(void *instance, uint8_t *bp, size_t n)
{
  (void)instance;
  (void)bp;
  (void)n;

  return 0;
}

/**
 * @brief   Implementation of the BaseSequentialStream put() method.
 */
static msg_t _streamput(void *instance, uint8_t b)
{
  aosDbgCheck(instance != NULL);

  // local variables
  AosShellChannel* channel = ((AosShellStream*)instance)->channel;
  msg_t ret = MSG_OK;

  // iterate through the list of channels
  while (channel != NULL) {
    msg_t ret_ = streamPut(channel, b);
    ret = (ret_ < ret) ? ret_ : ret;
    channel = channel->next;
  }

  return ret;
}

/**
 * @brief   Implementation of the BaseSequentialStream get() method.
 */
static msg_t _streamget(void *instance)
{
  (void)instance;

  return 0;
}

/**
 * @brief   Retrieve a pointer to the string buffer of a specified entry in the input buffer.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] entry   Entry to be retrieved.
 *
 * @return  Pointer to the entry in the input buffer.
 */
static inline char* _getAbsoluteEntry(const aos_shell_t* shell, size_t entry)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(entry < shell->input.nentries);

  return &(shell->input.buffer[entry * shell->input.linewidth * sizeof(char)]);
}

/**
 * @brief   Calculate absolute entry from history offset.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 * @param[in] offset  Relative offset of the entry to be retrieved.
 *
 * @return  Absolute index of the historic entry.
 */
static inline size_t _historyOffset2EntryIndex(const aos_shell_t* shell, const runtimedata_t* rdata, size_t offset)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);
  aosDbgCheck(offset < shell->input.nentries);

  return ((shell->input.nentries + rdata->buffer.current - offset) % shell->input.nentries);
}

/**
 * @brief   Retrieve a pointer to the string buffer of a historic entry in the input buffer.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 * @param[in] offset  Relative offset of the entry to be retrieved.
 *
 * @return  Pointer to the entry in the input buffer.
 */
static inline char* _getRelativeEntry(const aos_shell_t* shell, const runtimedata_t* rdata, size_t offset)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);
  aosDbgCheck(offset < shell->input.nentries);

  return _getAbsoluteEntry(shell, _historyOffset2EntryIndex(shell, rdata, offset));
}

/**
 * @brief   Retrieve a pointer to the current entry string in the input buffer.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 *
 * @return  Pointer to the string of the current entry in the input buffer.
 */
static inline char* _getCurrentEntry(const aos_shell_t* shell, const runtimedata_t* rdata)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);

  return _getAbsoluteEntry(shell, rdata->buffer.current);
}

/**
 * @brief   Retrieve a pointer to the currently selected entry.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 *
 * @return  Pointer to the currently selected entry or NULL if no entry is selected (cleared preview).
 */
static inline char* _getSelectedEntry(const aos_shell_t* shell, const runtimedata_t* rdata)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);

  if (rdata->buffer.selected > 0) {
    return _getRelativeEntry(shell, rdata, rdata->buffer.selected - 1);
  } else {
    return NULL;
  }
}

/**
 * @brief   Retrieve the currently visualized entry.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 *
 * @return  Pointer to the currently visualized entry or NULL if the input has been cleared (cleared preview).
 */
static inline char* _getVisualisedEntry(const aos_shell_t* shell, const runtimedata_t* rdata)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);

  if (rdata->buffer.selected == 0) {
    // cleared preview, nothing visualized
    return NULL;
  } else {
    if (rdata->buffer.selected == 1 || rdata->buffer.selected == rdata->buffer.edited) {
      // the current or a modified entry is selected
      return _getCurrentEntry(shell, rdata);
    } else {
      // a historic, unmodified entry is selected
      return _getRelativeEntry(shell, rdata, rdata->buffer.selected - 1);
    }
  }
}

/**
 * @brief   Print the shell prompt
 * @details Depending on the configuration flags, the system uptime is printed before the prompt string.
 *
 * @param[in] shell   Pointer to the shell object.
 */
static void _printPrompt(aos_shell_t* shell)
{
  aosDbgCheck(shell != NULL);

  // print some time informattion before prompt if configured
  if (shell->config & (AOS_SHELL_CONFIG_PROMPT_UPTIME | AOS_SHELL_CONFIG_PROMPT_DATETIME)) {
    // printf the system uptime
    if ((shell->config & (AOS_SHELL_CONFIG_PROMPT_UPTIME | AOS_SHELL_CONFIG_PROMPT_DATETIME)) == AOS_SHELL_CONFIG_PROMPT_UPTIME) {
      // get current system uptime
      aos_timestamp_t uptime;
      aosSysGetUptime(&uptime);

      chprintf((BaseSequentialStream*)&shell->stream, "[%01u:%02u:%02u:%02u:%03u:%03u] ",
               (uint32_t)(uptime / MICROSECONDS_PER_DAY),
               (uint8_t)(uptime % MICROSECONDS_PER_DAY / MICROSECONDS_PER_HOUR),
               (uint8_t)(uptime % MICROSECONDS_PER_HOUR / MICROSECONDS_PER_MINUTE),
               (uint8_t)(uptime % MICROSECONDS_PER_MINUTE / MICROSECONDS_PER_SECOND),
               (uint16_t)(uptime % MICROSECONDS_PER_SECOND / MICROSECONDS_PER_MILLISECOND),
               (uint16_t)(uptime % MICROSECONDS_PER_MILLISECOND / MICROSECONDS_PER_MICROSECOND));
    }
#if (HAL_USE_RTC == TRUE)
    else if ((shell->config & (AOS_SHELL_CONFIG_PROMPT_UPTIME | AOS_SHELL_CONFIG_PROMPT_DATETIME)) == AOS_SHELL_CONFIG_PROMPT_DATETIME) {
      // get current RTC time
      struct tm dt;
      aosSysGetDateTime(&dt);
      chprintf((BaseSequentialStream*)&shell->stream, "[%02u-%02u-%04u|%02u:%02u:%02u] ",
               dt.tm_mday,
               dt.tm_mon + 1,
               dt.tm_year + 1900,
               dt.tm_hour,
               dt.tm_min,
               dt.tm_sec);
    }
#endif /* (HAL_USE_RTC == TRUE) */
    else {
      aosDbgAssert(false);
    }
  }

  // print the actual prompt string
  if (shell->prompt && !(shell->config & AOS_SHELL_CONFIG_PROMPT_MINIMAL)) {
    chprintf((BaseSequentialStream*)&shell->stream, "%s$ ", shell->prompt);
  } else {
    chprintf((BaseSequentialStream*)&shell->stream, "%>$ ");
  }

  return;
}

/**
 * @brief   Interprete a escape sequence
 * @details This function interpretes escape sequences (starting with ASCII
 *          "Escape" character 0x1B) according to the VT100 / VT52 ANSI escape
 *          sequence definitions.
 * @note    Only the most important escape sequences are implemented yet.
 *
 * @param[in] seq   Character sequence to interprete.
 *                  Must be terminated by NUL byte.
 *
 * @return          A @p special_key value.
 */
static special_key_t _interpreteEscapeSequence(const char seq[])
{
  // local variables
  unsigned long strl = 0;
  const unsigned long seql = strlen(seq);
  bool ambiguous = false;

  // TAB
  /* not supported yet; use '\x09' instead */

  // BACKSPACE
  /* not supported yet; use '\x08' instead */

  // ESCAPE
  /* not supported yes; use '\x1B' instead */

  // CTRL + C
  /* not defined yet; use '\x03' instead */

  // INSERT
  if (strncmp(seq, KEYSTRING_INSERT, seql) == 0) {
    strl = strlen(KEYSTRING_INSERT);
    if (seql == strl) {
      return KEY_INSERT;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // DELETE
  if (strncmp(seq, KEYSTRING_DELETE, seql) == 0) {
    strl = strlen(KEYSTRING_DELETE);
    if (seql == strl) {
      return KEY_DELETE;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // HOME
  if (strncmp(seq, KEYSTRING_HOME, seql) == 0) {
    strl = strlen(KEYSTRING_HOME);
    if (seql == strl) {
      return KEY_HOME;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // END
  if (strncmp(seq, KEYSTRING_END, seql) == 0) {
    strl = strlen(KEYSTRING_END);
    if (seql == strl) {
      return KEY_END;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // PAGE UP
  if (strncmp(seq, KEYSTRING_PAGEUP, seql) == 0) {
    strl = strlen(KEYSTRING_PAGEUP);
    if (seql == strl) {
      return KEY_PAGEUP;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // PAGE DOWN
  if (strncmp(seq, KEYSTRING_PAGEDOWN, seql) == 0) {
    strl = strlen(KEYSTRING_PAGEDOWN);
    if (seql == strl) {
      return KEY_PAGEDOWN;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // ARROW UP
  if (strncmp(seq, KEYSTRING_ARROWUP, seql) == 0) {
    strl = strlen(KEYSTRING_ARROWUP);
    if (seql == strl) {
      return KEY_ARROWUP;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // ARROW DOWN
  if (strncmp(seq, KEYSTRING_ARROWDOWN, seql) == 0) {
    strl = strlen(KEYSTRING_ARROWDOWN);
    if (seql == strl) {
      return KEY_ARROWDOWN;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // ARROW LEFT
  if (strncmp(seq, KEYSTRING_ARROWLEFT, seql) == 0) {
    strl = strlen(KEYSTRING_ARROWLEFT);
    if (seql == strl) {
      return KEY_ARROWLEFT;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // ARROW RIGHT
  if (strncmp(seq, KEYSTRING_ARROWRIGHT, seql) == 0) {
    strl = strlen(KEYSTRING_ARROWRIGHT);
    if (seql == strl) {
      return KEY_ARROWRIGHT;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // CTRL + ARROW UP
  if (strncmp(seq, KEYSTRING_CTRL_ARROWUP, seql) == 0) {
    strl = strlen(KEYSTRING_CTRL_ARROWUP);
    if (seql == strl) {
      return KEY_CTRL_ARROWUP;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // CTRL + ARROW DOWN
  if (strncmp(seq, KEYSTRING_CTRL_ARROWDOWN, seql) == 0) {
    strl = strlen(KEYSTRING_CTRL_ARROWDOWN);
    if (seql == strl) {
      return KEY_CTRL_ARROWDOWN;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // CTRL + ARROW LEFT
  if (strncmp(seq, KEYSTRING_CTRL_ARROWLEFT, seql) == 0) {
    strl = strlen(KEYSTRING_CTRL_ARROWLEFT);
    if (seql == strl) {
      return KEY_CTRL_ARROWLEFT;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  // CTRL + ARROW RIGHT
  if (strncmp(seq, KEYSTRING_CTRL_ARROWRIGHT, seql) == 0) {
    strl = strlen(KEYSTRING_CTRL_ARROWRIGHT);
    if (seql == strl) {
      return KEY_CTRL_ARROWRIGHT;
    } else if (seql < strl) {
      ambiguous = true;
    }
  }

  return ambiguous ? KEY_AMBIGUOUS : KEY_UNKNOWN;
}

/**
 * @brief   Move the cursor in the terminal.
 *
 * @param[in] shell   Pointer to the shell object.
 * @param[in] line    Pointer to the current content of the line.
 * @param[in] from    Starting position of the cursor.
 * @param[in] to      Target position to move the cursor to.
 *
 * @return            The number of positions moved.
 */
static int _moveCursor(aos_shell_t* shell, const char* line, size_t from, size_t to)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(line !=  NULL || from >= to);
  aosDbgCheck(from <= shell->input.linewidth);
  aosDbgCheck(to <= shell->input.linewidth);

  // local variables
  size_t pos = from;

  // move cursor left by printing backspaces
  while (pos > to) {
    streamPut(&shell->stream, '\b');
    --pos;
  }

  // move cursor right by printing line content
  while (pos < to) {
    streamPut(&shell->stream, (uint8_t)line[pos]);
    ++pos;
  }

  return (int)pos - (int)from;
}

/**
 * @brief   Print content of a given string to the shell output stream.
 *
 * @param[in] shell   Pointer to the shell object.
 * @param[in] line    Pointer to the line to be printed.
 * @param[in] from    First position to start printing from.
 * @param[in] to      Position after the last character to print.
 *
 * @return            Number of characters printed.
 */
static size_t _printString(aos_shell_t* shell, const char* line, size_t from, size_t to)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(line != NULL || from >= to);
  aosDbgCheck(from < shell->input.linewidth);
  aosDbgCheck(to <= shell->input.linewidth);

  // local variables
  size_t cnt;

  for (cnt = 0; from + cnt < to; ++cnt) {
    streamPut(&shell->stream, (uint8_t)line[from + cnt]);
  }

  return cnt;
}

/**
 * @brief   Print a single character to the input buffer and to the output stream.
 *
 * @param[in] shell   Pointer to the shell object.
 * @param[in] rdata   Pointer to the runtim data object.
 * @param[in] c       Character to print.
 *
 * @return  Number of successfully handled characters.
 *          The return value can be interpreted as boolean (1 = sucess; 0 = error).
 */
static int _printChar(aos_shell_t* shell, runtimedata_t* rdata, char c)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);

  // check whether input line is already full
  if (rdata->input.length + 1 >= shell->input.linewidth) {
    return 0;
  }

  // retrieve entry in the input buffer
  char* line = _getCurrentEntry(shell, rdata);

  // overwrite content
  if (shell->config & AOS_SHELL_CONFIG_INPUT_OVERWRITE) {
    line[rdata->input.cursorpos] = c;
    ++rdata->input.cursorpos;
    rdata->input.length = (rdata->input.cursorpos > rdata->input.length) ? rdata->input.cursorpos : rdata->input.length;
    streamPut(&shell->stream, (uint8_t)c);
    return 1;
  }
  // insert character
  else {
    memmove(&line[rdata->input.cursorpos + 1], &line[rdata->input.cursorpos], rdata->input.length - rdata->input.cursorpos);
    line[rdata->input.cursorpos] = c;
    ++rdata->input.length;
    _printString(shell, line, rdata->input.cursorpos, rdata->input.length);
    ++rdata->input.cursorpos;
    _moveCursor(shell, line, rdata->input.length, rdata->input.cursorpos);
    return 1;
  }
}

/**
 * @brief   Overwrite the current output with a given line.
 * @details If the current output is longer than the string, the additional characters are cleared.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 * @param[in] line    The line to be printed.
 */
static void _overwriteOutput(aos_shell_t* shell, runtimedata_t* rdata, const char* line)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);
  aosDbgCheck(line != NULL);

  // local variables
  const size_t oldlength = rdata->input.length;

  // print line (overwrite current output)
  _moveCursor(shell, line, rdata->input.cursorpos, 0);
  rdata->input.length = strlen(line);
  _printString(shell, line, 0, rdata->input.length);

  // clear any remaining symbols
  if (oldlength > rdata->input.length) {
    for (rdata->input.cursorpos = rdata->input.length; rdata->input.cursorpos < oldlength; ++rdata->input.cursorpos) {
      streamPut(&shell->stream, ' ');
    }
    _moveCursor(shell, line, oldlength, rdata->input.length);
  }

  rdata->input.cursorpos = rdata->input.length;

  return;
}

/**
 * @brief   Compare two characters.
 *
 * @param[in] lhs       First character to compare.
 * @param[in] rhs       Second character to compare.
 *
 * @return              How well the characters match.
 */
static inline charmatch_t _charcmp(char lhs, char rhs)
{
  // if lhs is a upper case letter and rhs is a lower case letter
  if (lhs >= 'A' && lhs <= 'Z' && rhs >= 'a' && rhs <= 'z') {
    return (lhs == (rhs - 'a' + 'A')) ? CHAR_MATCH_NCASE : CHAR_MATCH_NOT;
  }
  // if lhs is a lower case letter and rhs is a upper case letter
  else if (lhs >= 'a' && lhs <= 'z' && rhs >= 'A' && rhs <= 'Z') {
    return ((lhs - 'a' + 'A') == rhs) ? CHAR_MATCH_NCASE : CHAR_MATCH_NOT;
  }
  // default
  else {
    return (lhs == rhs) ? CHAR_MATCH_CASE : CHAR_MATCH_NOT;
  }
}

/**
 * @brief   Maps an character from ASCII to a modified custom encoding.
 * @details The custom character encoding is very similar to ASCII and has the following structure:
 *          0x00=NULL ... 0x40='@' (identically to ASCII)
 *          0x4A='a'; 0x4B='A'; 0x4C='b'; 0x4D='B' ... 0x73='z'; 0x74='Z' (custom letter order)
 *          0x75='[' ... 0x7A='`' (0x5B..0x60 is ASCII)
 *          0x7B='{' ... 0x7F=DEL (identically to ASCII)
 *
 * @param[in] c   Character to map to the custom encoding.
 *
 * @return    The customly encoded character.
 */
static inline char _mapAscii2Custom(char c)
{
  if (c >= 'A' && c <= 'Z') {
    return ((c - 'A') * 2) + 'A' + 1;
  } else if (c > 'Z' && c < 'a') {
    return c + ('z' - 'a') + 1;
  } else if (c >= 'a' && c <= 'z') {
    return ((c - 'a') * 2) + 'A';
  } else {
    return c;
  }
}

/**
 * @brief   Compares two strings wrt letter case.
 * @details Comparisson uses a custom character encoding or mapping.
 *          See @p _mapAscii2Custom for details.
 *
 * @param[in] str1    First string to compare.
 * @param[in] str2    Second string to compare.
 * @param[in] cs      Flag indicating whether comparison shall be case sensitive.
 * @param[in,out] n   Maximum number of character to compare (in) and number of matching characters (out).
 *                    If a null pointer is specified, this parameter is ignored.
 *                    If the value pointed to is zero, comarison will not be limited.
 * @param[out] m      Optional indicator whether there was at least one case mismatch.
 *
 * @return      Integer value indicating the relationship between the strings.
 * @retval <0   The first character that does not match has a lower value in str1 than in str2.
 * @retval  0   The contents of both strings are equal.
 * @retval >0   The first character that does not match has a greater value in str1 than in str2.
 */
static int _strccmp(const char *str1, const char *str2, bool cs, size_t* n, charmatch_t* m)
{
  aosDbgCheck(str1 != NULL);
  aosDbgCheck(str2 != NULL);

  // initialize variables
  if (m) {
    *m = CHAR_MATCH_NOT;
  }
  size_t i = 0;

  // iterate through the strings
  while ((n == NULL) || (*n == 0) || (*n > 0 && i < *n)) {
    // break on NUL
    if (str1[i] == '\0' || str2[i] == '\0') {
      if (n) {
        *n = i;
      }
      break;
    }
    // compare character
    const charmatch_t match = _charcmp(str1[i], str2[i]);
    if ((match == CHAR_MATCH_CASE) || (!cs && match == CHAR_MATCH_NCASE)) {
      if (m != NULL && *m != CHAR_MATCH_NCASE) {
        *m = match;
      }
      ++i;
    } else {
      if (n) {
        *n = i;
      }
      break;
    }
  }

  return _mapAscii2Custom(str1[i]) - _mapAscii2Custom(str2[i]);
}

/**
 * @brief   Alters all intermediate NUL bytes in a string to spaces.
 *
 * @param[in] string  The string to be handled.
 * @param[in] length  Length of the string.
 *
 * @return  Detected Length of the actual content of the string.
 */
static size_t _restoreWhitespace(char* string, size_t length)
{
  aosDbgCheck(string != NULL || length == 0);

  // local variables
  size_t c = length;

  // seach for first non-NUL byte from the back
  while (c > 0) {
    --c;
    if (string[c] != '\0') {
      // store the detected length of the content
      length = ++c;
      break;
    }
  }

  // iterate further and replace all encountered NUL bytes by spaces
  while (c > 0) {
    --c;
    if (string[c] == '\0') {
      string[c] = ' ';
    }
  }

  return length;
}

/**
 * @brief   Performs required actions before an imminent modiifcation (character input, deletion or autofill).
 * @details This functions checks the current status and clears or copies entries in the input buffer as required.
 *          Status information (runtime data) is altered accordingly as well.
 *
 * @param[in] shell   Pointer to a shell object.
 * @param[in] rdata   Pointer to a runtime data object.
 *
 * @return    Pointer to the current entry in the input buffer.
 */
static char* _prepare4Modification(aos_shell_t* shell, runtimedata_t* rdata)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);

  char* line = _getCurrentEntry(shell, rdata);

  // cleared preview
  if (rdata->buffer.selected == 0) {
    // reset the current entry if required
    if (rdata->buffer.edited != 0) {
      memset(line, '\0', shell->input.linewidth * sizeof(char));
    }
    // set the current entry as the selected one and mark it as edited
    rdata->buffer.selected = 1;
    rdata->buffer.edited = 1;
  }
  // current entry
  else if (rdata->buffer.selected == 1) {
    // mark current entry as edited
    rdata->buffer.edited = 1;
  }
  // preview of historic entry
  else if (rdata->buffer.selected > 1) {
    // copy the selected entry before modification if required
    if (rdata->buffer.selected!= rdata->buffer.edited) {
      memcpy(line, _getSelectedEntry(shell, rdata), shell->input.linewidth * sizeof(char));
    }
    // mark the selected entry as edited
    rdata->buffer.edited = rdata->buffer.selected;
  }

  return line;
}

/**
 * @brief   Read input from a channel as long as there is data available.
 *
 * @param[in]     shell     Pointer to the shell object.
 * @param[in,out] rdata     Pointer to a runtime data object.
 * @param[in]     channel   The channel to read from.
 *
 * @return  Number of characters read.
 */
static size_t _readChannel(aos_shell_t* shell, runtimedata_t* rdata, AosShellChannel* channel)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(rdata != NULL);
  aosDbgCheck(channel != NULL);

  // local variables
  size_t bytes = 0;
  char c;
  special_key_t key;
  action_t action;

  // read character by character from the channel
  while (chnReadTimeout(channel, (uint8_t*)&c, 1, TIME_IMMEDIATE)) {
    // increment byte counter
    ++bytes;

    // drop any further input after an execution request was detected
    if (rdata->lastaction == ACTION_EXECUTE && bytes > 1) {
      continue;
    }

    // try to interprete escape sequence
    {
      // set default
      key = KEY_UNKNOWN;
      // only interprete, if there is an escape sequence at all
      const size_t escl = strlen(rdata->input.escseq);
      if (escl > 0) {
        // append and 'consume' character
        rdata->input.escseq[escl] = c;
        c = '\0';
        // try to interprete sequence
        key = _interpreteEscapeSequence(rdata->input.escseq);
        switch (key) {
          // ambiguous key due to incomplete sequence
          case KEY_AMBIGUOUS:
            // read next byte to resolve ambiguity
            continue;
          // an unknown sequence has been encountered
          case KEY_UNKNOWN:
            // increment number of inputs but handle this unknown sequence below
            break;
          // a key was identified successfully
          default:
            // reset the sequence buffer
            memset(rdata->input.escseq, '\0', AOS_SHELL_ESCSEQUENCE_LENGTH * sizeof(char));
            break;
        }
      }
    }

    /*
     * Derive action to be executed from keypress.
     * This step handles all sanity checks, so any required prerequisites for the selected action are fulfilled.
     */
    // set default
    action = ACTION_NONE;
    // if there is no escape sequence pending
    if (rdata->input.escseq[0] == '\0') {

      // printable character
      if (c >= '\x20' && c <= '\x7E') {
        action = ACTION_READCHAR;
      }

      // tab key or character
      else if (c == '\x09' || key == KEY_TAB) {
        /*
         * pressing tab once applies auto fill
         * pressing tab a second time (or more) prints suggestions
         */
        if (rdata->lastaction == ACTION_AUTOCOMPLETE || rdata->lastaction == ACTION_SUGGEST) {
          action = ACTION_SUGGEST;
        } else {
          action = ACTION_AUTOCOMPLETE;
        }
      }

      // carriage return ('\r') or line feed ('\n') character
      else if (c == '\x0D' || c == '\x0A') {
        action = ACTION_EXECUTE;
      }

      // backspace key or character
      else if (c == '\x08' || key == KEY_BACKSPACE) {
        // ignore if cursor is at very left
        if (rdata->input.cursorpos > 0) {
          action = ACTION_DELETEBACKWARD;
        }
      }

      // DEL key or character
      else if (c == '\x7F' || key == KEY_DELETE) {
        // ignore if cursor is at very right
        if (rdata->input.cursorpos < rdata->input.length) {
          action = ACTION_DELETEFORWARD;
        }
      }

      // 'arrow up' key
      else if (key == KEY_ARROWUP) {
        // recall previous input from history only if
        // not the oldest entry is already selected and
        // the previous entry has been set.
        if (rdata->buffer.selected < shell->input.nentries &&
            (_getRelativeEntry(shell, rdata, rdata->buffer.selected))[0] != INBUF_INIT_CHAR) {
          action = ACTION_RECALLPREVIOUS;
        }
      }

      // 'arrow down' key
      else if (key == KEY_ARROWDOWN) {
        // clear the line if
        // no historic entry is selected or
        // the most recent entry is selected, but the current one is occupied by a moodfied version of a historic entry
        if ((rdata->buffer.selected == 1) ||
            (rdata->buffer.selected == 2 && rdata->buffer.edited > 1)) {
          action = ACTION_CLEAR;
        }
        // if a historic entry is selected, recall the next input from history
        else if (rdata->buffer.selected > 1) {
          action = ACTION_RECALLNEXT;
        }
      }

      // 'arrow left' key
      else if (key == KEY_ARROWLEFT) {
        // ignore if cursor is very left
        if (rdata->input.cursorpos > 0) {
          action = ACTION_CURSORLEFT;
        }
      }

      // 'arrow right' key
      else if (key == KEY_ARROWRIGHT) {
        // ignore if cursor is very right
        if (rdata->input.cursorpos < rdata->input.length) {
          action = ACTION_CURSORRIGHT;
        }
      }

      // CTRL + 'arrow up' key combination or 'page up' key
      else if (key == KEY_CTRL_ARROWUP || key == KEY_PAGEUP) {
        // recall oldest input from history only if
        // not the oldest entry is already selected and
        // there is at least one history entry set
        if (rdata->buffer.selected < shell->input.nentries &&
            (_getRelativeEntry(shell, rdata, (rdata->buffer.selected > 0) ? 1 : 0))[0] != INBUF_INIT_CHAR) {
          action = ACTION_RECALLOLDEST;
        }
      }

      // CTRL + 'arrow down' key combination or 'page down' key
      else if (key == KEY_CTRL_ARROWDOWN || key == KEY_PAGEDOWN) {
        // clear the line if
        // no historic entry is selected or
        // the most recent entry is selected, but the current one is occupied by a moodfied version of a historic entry
        if ((rdata->buffer.selected == 1) ||
            (rdata->buffer.selected > 1 && rdata->buffer.edited > 1)) {
          action = ACTION_CLEAR;
        }
        // if a historic entry is selected, reset to the current input
        else if (rdata->buffer.selected > 1) {
          action = ACTION_RECALLCURRENT;
        }
      }

      // CTRL + 'arrow left' key combination
      else if (key == KEY_CTRL_ARROWLEFT) {
        // ignore if cursor is very left
        if (rdata->input.cursorpos > 0) {
          action = ACTION_CURSORWORDLEFT;
        }
      }

      // CTRL + 'arrow right' key combination
      else if (key == KEY_CTRL_ARROWRIGHT) {
        // ignore if cursor is very right
        if (rdata->input.cursorpos < rdata->input.length) {
          action = ACTION_CURSORWORDRIGHT;
        }
      }

      // 'end' key
      else if (key == KEY_END) {
        // ignore if cursos is very right
        if (rdata->input.cursorpos < rdata->input.length) {
          action = ACTION_CURSOR2END;
        }
      }

      // 'home' key
      else if (key == KEY_HOME) {
        // ignore if cursor is very left
        if (rdata->input.cursorpos > 0) {
          action = ACTION_CURSOR2START;
        }
      }

      // CTRL + C key combination
      else if (c == '\x03' || key == KEY_CTRL_C) {
        action = ACTION_RESET;
      }

      // INS key
      else if (key == KEY_INSERT) {
        action = ACTION_INSERTTOGGLE;
      }

      // ESC key or [ESCAPE] character
      else if (c == '\x1B' || key == KEY_ESCAPE) {
        action = ACTION_ESCSTART;
      }
    }
    // ongoing escape sequence or interpretation failed
    else /* if (rdata->input.escseq[0] != '\0') */ {
      // unknown escape sequence (interpretation failed)
      if (key == KEY_UNKNOWN) {
        action = ACTION_PRINTUNKNOWNSEQUENCE;
      }
    } /* end of action selection */

    /*
     * execute action
     */
    switch (action) {
      case ACTION_NONE:
      {
        // do nothing (ignore input) and read next byte
        break;
      }

      case ACTION_READCHAR:
      {
        char* line = _prepare4Modification(shell, rdata);
        if (_printChar(shell, rdata, c) == 0) {
          // line is full
          _moveCursor(shell, line, rdata->input.cursorpos, rdata->input.length);
          chprintf((BaseSequentialStream*)&shell->stream, "\n\tmaximum line width reached\n");
          _printPrompt(shell);
          _printString(shell, line, 0, rdata->input.length);
          _moveCursor(shell, line, rdata->input.length, rdata->input.cursorpos);
        }
        break;
      }

      case ACTION_AUTOCOMPLETE:
      {
        // local variables
        char* line = _getVisualisedEntry(shell, rdata);
        const char* fill = line;
        size_t cmatch = rdata->input.cursorpos;
        charmatch_t matchlevel = CHAR_MATCH_NOT;
        size_t n;

        // only execute autofill if the line is valid
        if (line) {
          // iterate through command list
          for (aos_shellcommand_t* cmd = shell->commands; cmd != NULL; cmd = cmd->next) {
            // compare current match with command
            n = cmatch;
            charmatch_t mlvl = CHAR_MATCH_NOT;
            _strccmp(fill, cmd->name, shell->config & AOS_SHELL_CONFIG_MATCH_CASE, (n == 0) ? NULL : &n, &mlvl);
            const int cmp = (n < cmatch) ?
                  ((int)n - (int)cmatch) :
                  (cmd->name[n] != '\0') ?
                    (int)strlen(cmd->name) - (int)n :
                    0;
            // if an exact match was found
            if ((size_t)((int)cmatch + cmp) == rdata->input.cursorpos) {
              cmatch = rdata->input.cursorpos;
              fill = cmd->name;
              // break the loop only if there are no case mismatches with the input
              n = rdata->input.cursorpos;
              _strccmp(fill, line, false, &n, &mlvl);
              if (mlvl == CHAR_MATCH_CASE) {
                break;
              }
            }
            // if a not exact match was found
            else if ((size_t)((int)cmatch + cmp) > rdata->input.cursorpos) {
              // if this is the first one
              if (fill == line) {
                cmatch = (size_t)((int)cmatch + cmp);
                fill = cmd->name;
              }
              // if this is a worse one
              else if ((cmp < 0) || (cmp == 0 && mlvl == CHAR_MATCH_CASE)) {
                cmatch = (size_t)((int)cmatch + cmp);
              }
            }
            // non matching commands are ignored
            else {}
          }

          // evaluate if there are case mismatches
          n = cmatch;
          _strccmp(line, fill, shell->config & AOS_SHELL_CONFIG_MATCH_CASE, &n, &matchlevel);
          // print the auto fill if any
          if ((cmatch > rdata->input.cursorpos) ||
              (cmatch == rdata->input.cursorpos && matchlevel == CHAR_MATCH_NCASE && strlen(fill) == rdata->input.cursorpos)) {
            line = _prepare4Modification(shell, rdata);
            // limit auto fill so it will not overflow the line width
            if (rdata->input.length + (cmatch - rdata->input.cursorpos) > shell->input.linewidth) {
              cmatch = shell->input.linewidth - rdata->input.length + rdata->input.cursorpos;
            }
            // move trailing memory further in the line
            memmove(&line[cmatch], &line[rdata->input.cursorpos], (rdata->input.length - rdata->input.cursorpos) * sizeof(char));
            rdata->input.length += cmatch - rdata->input.cursorpos;
            // if there was no incorrect case when matching
            if (matchlevel == CHAR_MATCH_CASE) {
              // insert fill command name to line
              memcpy(&line[rdata->input.cursorpos], &fill[rdata->input.cursorpos], (cmatch - rdata->input.cursorpos) * sizeof(char));
              // print the output
              _printString(shell, line, rdata->input.cursorpos, rdata->input.length);
            } else {
              // overwrite line with fill command name
              memcpy(line, fill, cmatch * sizeof(char));
              // reprint the whole line
              _moveCursor(shell, line, rdata->input.cursorpos, 0);
              _printString(shell, line, 0, rdata->input.length);
            }
            // move cursor to the end of the matching sequence
            rdata->input.cursorpos = cmatch;
            _moveCursor(shell, line, rdata->input.length, rdata->input.cursorpos);
          }
        }
        break;
      }

      case ACTION_SUGGEST:
      {
        // local variables
        const char* line = _getVisualisedEntry(shell, rdata);
        unsigned int matches = 0;

        // iterate through command list
        for (aos_shellcommand_t* cmd = shell->commands; cmd != NULL; cmd = cmd->next) {
          // compare line content with command, except if cursorpos is 0
          size_t i = rdata->input.cursorpos;
          if (rdata->input.cursorpos > 0) {
            _strccmp(line, cmd->name, shell->config & AOS_SHELL_CONFIG_MATCH_CASE, &i, NULL);
          }
          const int cmp = (i < rdata->input.cursorpos) ?
                ((int)i - (int)rdata->input.cursorpos) :
                (cmd->name[i] != '\0') ?
                  (int)strlen(cmd->name) - (int)i :
                  0;
          // if a match was found
          if (cmp > 0) {
            // if this is the first one
            if (matches == 0) {
              _moveCursor(shell, line, rdata->input.cursorpos, rdata->input.length);
              streamPut(&shell->stream, '\n');
            }
            // print the command
            chprintf((BaseSequentialStream*)&shell->stream, "\t%s\n", cmd->name);
            ++matches;
          }
        }
        // reprint the prompt and line if any matches have been found
        if (matches > 0) {
          _printPrompt(shell);
          _printString(shell, line, 0, rdata->input.length);
          _moveCursor(shell, line, rdata->input.length, rdata->input.cursorpos);
        }
        break;
      }

      case ACTION_EXECUTE:
      {
        // if the input buffer can hold historic entries
        if (shell->input.nentries > 1) {
          _prepare4Modification(shell, rdata);
        }
        break;
      }

      case ACTION_DELETEBACKWARD:
      {
        char* line = _prepare4Modification(shell, rdata);
        --rdata->input.cursorpos;
        memmove(&line[rdata->input.cursorpos], &line[rdata->input.cursorpos + 1], (rdata->input.length - rdata->input.cursorpos) * sizeof(char));
        --rdata->input.length;
        line[rdata->input.length] = '\0';
        _moveCursor(shell, line, rdata->input.cursorpos + 1, rdata->input.cursorpos);
        _printString(shell, line, rdata->input.cursorpos, rdata->input.length);
        streamPut(&shell->stream, ' ');
        _moveCursor(shell, line, rdata->input.length + 1, rdata->input.cursorpos);
        break;
      }

      case ACTION_DELETEFORWARD:
      {
        char* line = _prepare4Modification(shell, rdata);
        --rdata->input.length;
        memmove(&line[rdata->input.cursorpos], &line[rdata->input.cursorpos + 1], (rdata->input.length - rdata->input.cursorpos) * sizeof(char));
        _printString(shell, line, rdata->input.cursorpos, rdata->input.length);
        streamPut(&shell->stream, ' ');
        _moveCursor(shell, line, rdata->input.length + 1, rdata->input.cursorpos);
        break;
      }

      case ACTION_CLEAR:
      {
        // clear visualization
        _moveCursor(shell, NULL, rdata->input.cursorpos, 0);
        for (size_t cpos = 0; cpos < rdata->input.length; ++cpos) {
          streamPut(&shell->stream, ' ');
        }
        _moveCursor(shell, NULL, rdata->input.length, 0);

        // update metadata
        rdata->input.cursorpos = 0;
        rdata->input.length = 0;
        rdata->buffer.selected = 0;

        break;
      }

      case ACTION_RECALLPREVIOUS:
      {
        // if the input was cleared but the current entry is occupied by a modified copy of a history entry, skip the current entry
        if (shell->input.nentries > 1 && rdata->buffer.selected == 0 && rdata->buffer.edited != 1) {
          rdata->buffer.selected += 2;
        } else {
          ++rdata->buffer.selected;
        }
        _overwriteOutput(shell, rdata, _getVisualisedEntry(shell, rdata));
        break;
      }

      case ACTION_RECALLNEXT:
      {
        --rdata->buffer.selected;
        _overwriteOutput(shell, rdata, _getVisualisedEntry(shell, rdata));
        break;
      }

      case ACTION_RECALLOLDEST:
      {
        // find oldest valid entry
        rdata->buffer.selected = shell->input.nentries;
        while (_getSelectedEntry(shell, rdata)[0] == INBUF_INIT_CHAR) {
          --rdata->buffer.selected;
        }

        _overwriteOutput(shell, rdata, _getVisualisedEntry(shell, rdata));
        break;
      }

      case ACTION_RECALLCURRENT:
      {
        rdata->buffer.selected = 1;
        _overwriteOutput(shell, rdata, _getVisualisedEntry(shell, rdata));
        break;
      }

      case ACTION_CURSORLEFT:
      {
        _moveCursor(shell, NULL, rdata->input.cursorpos, rdata->input.cursorpos - 1);
        --rdata->input.cursorpos;
        break;
      }

      case ACTION_CURSORRIGHT:
      {
        _moveCursor(shell, _getVisualisedEntry(shell, rdata), rdata->input.cursorpos, rdata->input.cursorpos + 1);
        ++rdata->input.cursorpos;
        break;
      }

      case ACTION_CURSORWORDLEFT:
      {
        // local variables
        const char* line = _getVisualisedEntry(shell, rdata);
        size_t cpos = rdata->input.cursorpos;

        // skip spaces
        while (cpos > 0) {
          --cpos;
          if (line[cpos] != ' ') {
            break;
          }
        }

        // search for beginning of the word
        while (cpos > 0) {
          --cpos;
          if (line[cpos] == ' ') {
            // go back to first character of the word
            ++cpos;
            break;
          }
        }

        // move the cursor and set metadata
        _moveCursor(shell, line, rdata->input.cursorpos, cpos);
        rdata->input.cursorpos = cpos;

        break;
      }

      case ACTION_CURSORWORDRIGHT:
      {
        // local variables
        const char* line = _getVisualisedEntry(shell, rdata);
        size_t cpos = rdata->input.cursorpos;

        // skip spaces
        while (cpos < rdata->input.length && line[cpos] == ' ') {
          ++cpos;
        }

        // search for end of the word
        while (cpos < rdata->input.length && line[cpos] != ' ') {
          ++cpos;
        }

        // move cursor and set metadata
        _moveCursor(shell, line, rdata->input.cursorpos, cpos);
        rdata->input.cursorpos = cpos;

        break;
      }

      case ACTION_CURSOR2END:
      {
        _moveCursor(shell, _getVisualisedEntry(shell, rdata), rdata->input.cursorpos, rdata->input.length);
        rdata->input.cursorpos = rdata->input.length;
        break;
      }

      case ACTION_CURSOR2START:
      {
        _moveCursor(shell, _getVisualisedEntry(shell, rdata), rdata->input.cursorpos, 0);
        rdata->input.cursorpos = 0;
        break;
      }

      case ACTION_RESET:
      {
        // print reset indicator
        chprintf((BaseSequentialStream*)&shell->stream, "^C");
        if (!(shell->config & AOS_SHELL_CONFIG_INPUT_OVERWRITE)) {
          _printString(shell, _getVisualisedEntry(shell, rdata), rdata->input.cursorpos, rdata->input.length);
        }
        chprintf((BaseSequentialStream*)&shell->stream, "\n");

        // reset buffers and metadata
        if (rdata->buffer.edited != 0) {
          memset(_getCurrentEntry(shell, rdata), '\0', shell->input.linewidth * sizeof(char));
        }
        rdata->input.length = 0;
        rdata->input.cursorpos = 0;
        rdata->buffer.selected = (shell->input.nentries > 1) ? 1 : 0;
        rdata->buffer.edited = 0;

        // print a new prompt
        _printPrompt(shell);

        break;
      }

      case ACTION_INSERTTOGGLE:
      {
        if (shell->config & AOS_SHELL_CONFIG_INPUT_OVERWRITE) {
          shell->config &= ~AOS_SHELL_CONFIG_INPUT_OVERWRITE;
        } else {
          shell->config |= AOS_SHELL_CONFIG_INPUT_OVERWRITE;
        }
        break;
      }

      case ACTION_ESCSTART:
      {
        rdata->input.escseq[0] = c;
        break;
      }

      case ACTION_PRINTUNKNOWNSEQUENCE:
      {
        _prepare4Modification(shell, rdata);
        for (size_t seqc = 1; rdata->input.escseq[seqc] != '\0'; ++seqc) {
          // element 0 would be unprintable ESC character
          _printChar(shell, rdata, rdata->input.escseq[seqc]);
        }
        memset(rdata->input.escseq, '\0', AOS_SHELL_ESCSEQUENCE_LENGTH * sizeof(char));
        break;
      }
    }

    // update runtime data
    rdata->lastaction = (action != ACTION_NONE) ? action : rdata->lastaction;
  } /* end of while */

  return bytes;
}

/**
 * @brief   Parses the content of the given string to separate arguments.
 *
 * @param[in]   shell   Pointer to the shell object.
 * @param[in]   str     String to be parsed.
 * @param[out]  argbuf  Buffer to store argument pointers to.
 *
 * @return            Number of arguments found.
 */
static size_t _parseArguments(aos_shell_t* shell, char* str, char** argbuf)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(str != NULL);
  aosDbgCheck(argbuf != NULL);

  /*
   * States for a very small FSM.
   */
  typedef enum {
    START,
    SPACE,
    TEXT,
    END,
  } state_t;

  // local variables
  state_t state = START;
  size_t nargs = 0;

  // iterate through the line
  for (size_t c = 0; c < shell->input.linewidth; ++c) {
    // terminate at first NUL byte
    if (str[c] == '\0') {
      state = END;
      break;
    }
    // spaces become NUL bytes
    else if (str[c] == ' ') {
      str[c] = '\0';
      state = SPACE;
    }
    // handle non-NUL bytes
    else {
      switch (state) {
        case START:
        case SPACE:
          // ignore too many arguments
          if (nargs < shell->input.nargs) {
            argbuf[nargs] = &str[c];
          }
          ++nargs;
          break;
        case TEXT:
        case END:
          break;
      }
      state = TEXT;
    }
  }

  // set all remaining argument pointers to NULL
  for (size_t a = nargs; a < shell->input.nargs; ++a) {
    argbuf[a] = NULL;
  }

  return nargs;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes a shell object with the specified parameters.
 *
 * @param[in,out] shell           Pointer to the shell object to be initialized.
 * @param[in]     name            Name of the shell thread (may be NULL).
 * @param[in]     prompt          Prompt line to print (NULL = use default prompt).
 * @param[in]     inbuf           Two dimensional input buffer.
 * @param[in]     entries         Number of entries in the input buffer (1st dimension).
 * @param[in]     linewidth       Length of each entry in the input buffer (2nd dimension).
 * @param[in]     numargs         Maximum number of arguments (defines size of internal buffer).
 */
void aosShellInit(aos_shell_t* shell, const char* name, const char* prompt, char inbuf[], size_t entries, size_t linewidth, size_t numargs)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(inbuf != NULL);
  aosDbgCheck(entries > 0);
  aosDbgCheck(linewidth > 0);
  aosDbgCheck(numargs > 0);

  // set parameters
  shell->thread = NULL;
#if (CH_CFG_USE_REGISTRY == TRUE)
  shell->name = name;
#else /* (CH_CFG_USE_REGISTRY == TRUE) */
  (void)name;
#endif /* (CH_CFG_USE_REGISTRY == TRUE) */
  chEvtObjectInit(&shell->eventSource);
  aosShellStreamInit(&shell->stream);
  shell->prompt = prompt;
  shell->commands = NULL;
  shell->execstatus.command = NULL;
  shell->execstatus.retval = 0;
  shell->input.buffer = inbuf;
  shell->input.nentries = entries;
  shell->input.linewidth = linewidth;
  shell->input.nargs = numargs;
  shell->config = 0x00;

  // initialize buffers
  memset(shell->input.buffer, INBUF_INIT_CHAR, shell->input.nentries * shell->input.linewidth * sizeof(char));

  return;
}

/**
 * @brief   Initialize an AosShellStream object.
 *
 * @param[in] stream  The AosShellStrem to initialize.
 */
void aosShellStreamInit(AosShellStream* stream)
{
  aosDbgCheck(stream != NULL);

  stream->vmt = &_streamvmt;
  stream->channel = NULL;

  return;
}

/**
 * @brief   Initialize an AosShellChannel object with the specified parameters.
 *
 * @param[in] channel       The AosShellChannel to initialize.
 * @param[in] asyncchannel  An BaseAsynchronousChannel this AosShellChannel is associated with.
 */
void aosShellChannelInit(AosShellChannel* channel, BaseAsynchronousChannel* asyncchannel)
{
  aosDbgCheck(channel != NULL);
  aosDbgCheck(asyncchannel != NULL);

  channel->vmt = &_channelvmt;
  channel->asyncchannel = asyncchannel;
  channel->listener.wflags = 0;
  channel->next = NULL;
  channel->flags = 0;

  return;
}

/**
 * @brief   Inserts a command to the shells list of commands.
 *
 * @param[in] shell   Pointer to the shell object.
 * @param[in] cmd     Pointer to the command to add.
 *
 * @return            A status value.
 * @retval AOS_SUCCESS  The command was added successfully.
 * @retval AOS_ERROR    Another command with identical name already exists.
 */
aos_status_t aosShellAddCommand(aos_shell_t *shell, aos_shellcommand_t *cmd)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(cmd != NULL);
  aosDbgCheck(cmd->name != NULL && strlen(cmd->name) > 0 && strchr(cmd->name, ' ') == NULL && strchr(cmd->name, '\t') == NULL);
  aosDbgCheck(cmd->callback != NULL);
  aosDbgCheck(cmd->next == NULL);

  aos_shellcommand_t* prev = NULL;
  aos_shellcommand_t** curr = &(shell->commands);

  // insert the command to the list wrt lexographical order (exception: lower case characters preceed upper their uppercase counterparts)
  while (*curr != NULL) {
    // iterate through the list as long as the command names are 'smaller'
    const int cmp = _strccmp((*curr)->name, cmd->name, true, NULL, NULL);
    if (cmp < 0) {
      prev = *curr;
      curr = &((*curr)->next);
      continue;
    }
    // error if the command already exists
    else if (cmp == 0) {
      return AOS_ERROR;
    }
    // insert the command as soon as a 'larger' name was found
    else /* if (cmpval > 0) */ {
      cmd->next = *curr;
      // special case: the first command is larger
      if (prev == NULL) {
        shell->commands = cmd;
      } else {
        prev->next = cmd;
      }
      return AOS_SUCCESS;
    }
  }
  // the end of the list has been reached

  // append the command
  *curr = cmd;
  return AOS_SUCCESS;
}

/**
 * @brief   Removes a command from the shells list of commands.
 *
 * @param[in] shell     Pointer to the shell object.
 * @param[in] cmd       Name of the command to removde.
 * @param[out] removed  Optional pointer to the command that was removed.
 *
 * @return              A status value.
 * @retval AOS_SUCCESS  The command was removed successfully.
 * @retval AOS_ERROR    The command name was not found.
 */
aos_status_t aosShellRemoveCommand(aos_shell_t *shell, char *cmd, aos_shellcommand_t **removed)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(cmd != NULL && strlen(cmd) > 0);

  aos_shellcommand_t* prev = NULL;
  aos_shellcommand_t** curr = &(shell->commands);

  // iterate through the list and seach for the specified command name
  while (curr != NULL) {
    const int cmpval = strcmp((*curr)->name, cmd);
    // iterate through the list as long as the command names are 'smaller'
    if (cmpval < 0) {
      prev = *curr;
      curr = &((*curr)->next);
      continue;
    }
    // remove the command when found
    else if (cmpval == 0) {
      // special case: the first command matches
      if (prev == NULL) {
        shell->commands = (*curr)->next;
      } else {
        prev->next = (*curr)->next;
      }
      (*curr)->next = NULL;
      // set the optional output argument
      if (removed != NULL) {
        *removed = *curr;
      }
      return AOS_SUCCESS;
    }
    // break the loop if the command names are 'larger'
    else /* if (cmpval > 0) */ {
      break;
    }
  }

  // if the command was not found, return an error
  return AOS_ERROR;
}

/**
 * @brief   Count the number of commands assigned to the shell.
 *
 * @param[in] shell   The shell to count the commands for.
 *
 * @return  The number of commands associated to the shell.
 */
unsigned int aosShellCountCommands(aos_shell_t* shell)
{
  aosDbgCheck(shell != NULL);

  unsigned int count = 0;
  aos_shellcommand_t* cmd = shell->commands;
  while (cmd != NULL) {
    ++count;
    cmd = cmd->next;
  }

  return count;
}

/**
 * @brief   Add a channel to a AosShellStream.
 *
 * @param[in] stream    The AosShellStream to extend.
 * @param[in] channel   The channel to be added to the stream.
 */
void aosShellStreamAddChannel(AosShellStream* stream, AosShellChannel* channel)
{
  aosDbgCheck(stream != NULL);
  aosDbgCheck(channel != NULL && channel->asyncchannel != NULL && channel->next == NULL && (channel->flags & AOS_SHELLCHANNEL_ATTACHED) == 0);

  // prepend the new channel
  chSysLock();
  channel->flags |= AOS_SHELLCHANNEL_ATTACHED;
  channel->next = stream->channel;
  stream->channel = channel;
  chSysUnlock();

  return;
}

/**
 * @brief   Remove a channel from an AosShellStream.
 *
 * @param[in] stream    The AosShellStream to modify.
 * @param[in] channel   The channel to remove.
 *
 * @return              A status value.
 * @retval AOS_SUCCESS  The channel was removed successfully.
 * @retval AOS_ERROR    The specified channel was not found to be associated with the shell.
 */
aos_status_t aosShellStreamRemoveChannel(AosShellStream* stream, AosShellChannel* channel)
{
  aosDbgCheck(stream != NULL);
  aosDbgCheck(channel != NULL && channel->asyncchannel != NULL && channel->flags & AOS_SHELLCHANNEL_ATTACHED);

  // local varibales
  AosShellChannel* prev = NULL;
  AosShellChannel* curr = stream->channel;

  // iterate through the list and search for the specified channel
  while (curr != NULL) {
    // if the channel was found
    if (curr == channel) {
      chSysLock();
      // special case: the first channel matches (prev is NULL)
      if (prev == NULL) {
        stream->channel = curr->next;
      } else {
        prev->next = channel->next;
      }
      curr->next = NULL;
      curr->flags &= ~AOS_SHELLCHANNEL_ATTACHED;
      chSysUnlock();
      return AOS_SUCCESS;
    }
  }

  // if the channel was not found, return an error
  return AOS_ERROR;
}

/**
 * @brief   Enable a AosShellChannel as input.
 *
 * @param[in] channel   The channel to enable as input.
 */
void aosShellChannelInputEnable(AosShellChannel* channel)
{
  aosDbgCheck(channel != NULL && channel->asyncchannel != NULL);

  chSysLock();
  channel->listener.wflags |= CHN_INPUT_AVAILABLE;
  channel->flags |= AOS_SHELLCHANNEL_INPUT_ENABLED;
  chSysUnlock();

  return;
}

/**
 * @brief   Disable a AosShellChannel as input.
 *
 * @param[in] channel   The channel to disable as input.
 */
void aosShellChannelInputDisable( AosShellChannel* channel)
{
  aosDbgCheck(channel != NULL && channel->asyncchannel != NULL);

  chSysLock();
  channel->listener.wflags &= ~CHN_INPUT_AVAILABLE;
  channel->flags &= ~AOS_SHELLCHANNEL_INPUT_ENABLED;
  chSysUnlock();

  return;
}

/**
 * @brief   Enable a AosShellChannel as output.
 *
 * @param[in] channel   The channel to enable as output.
 */
void aosShellChannelOutputEnable(AosShellChannel* channel)
{
  aosDbgCheck(channel != NULL && channel->asyncchannel != NULL);

  channel->flags |= AOS_SHELLCHANNEL_OUTPUT_ENABLED;

  return;
}

/**
 * @brief   Disable a AosShellChannel as output.
 *
 * @param[in] channel   The channel to disable as output.
 */
void aosShellChannelOutputDisable(AosShellChannel* channel)
{
  aosDbgCheck(channel != NULL && channel->asyncchannel != NULL);

  channel->flags &= ~AOS_SHELLCHANNEL_OUTPUT_ENABLED;

  return;
}

/**
 * @brief   Thread main function.
 *
 * @param[in] aosShellThread    Name of the function;
 * @param[in] shell             Pointer to the shell object.
 */
void aosShellThread(void* shell)
{
  aosDbgCheck(shell != NULL);
  aosDbgCheck(((aos_shell_t*)shell)->input.nentries > 0);

  // local variables
  eventmask_t eventmask;
  eventflags_t eventflags;
  AosShellChannel* channel;
  runtimedata_t rdata;
  char* args[((aos_shell_t*)shell)->input.nargs];
  size_t nargs = 0;
  aos_shellcommand_t* cmd;

  // initialize variables and buffers
#if (CH_CFG_USE_REGISTRY == TRUE)
  currp->name = ((aos_shell_t*)shell)->name;
#endif /* (CH_CFG_USE_REGISTRY == TRUE) */
  rdata.input.length = 0;
  rdata.input.cursorpos = 0;
  memset(rdata.input.escseq, '\0', AOS_SHELL_ESCSEQUENCE_LENGTH * sizeof(char));
  rdata.buffer.current = 0;
  rdata.buffer.selected = (((aos_shell_t*)shell)->input.nentries > 1) ? 1 : 0;
  rdata.buffer.edited = 0;
  rdata.lastaction = ACTION_NONE;
  for (size_t arg = 0; arg < ((aos_shell_t*)shell)->input.nargs; ++arg) {
    args[arg] = NULL;
  }
  memset(_getCurrentEntry((aos_shell_t*)shell, &rdata), '\0', ((aos_shell_t*)shell)->input.linewidth * sizeof(char));

  // register OS related events
  chEvtRegisterMask(&aos.events.os, &(((aos_shell_t*)shell)->osEventListener), EVENTMASK_OS);
  // register events to all input channels
  for (channel = ((aos_shell_t*)shell)->stream.channel; channel != NULL; channel = channel->next) {
    chEvtRegisterMaskWithFlags(&(channel->asyncchannel->event), &(channel->listener), EVENTMASK_INPUT, channel->listener.wflags);
  }

  // fire start event
  chEvtBroadcastFlags(&(((aos_shell_t*)shell)->eventSource), AOS_SHELL_EVTFLAG_START);

  // print the prompt for the first time
  _printPrompt((aos_shell_t*)shell);

  // enter thread loop
  while (!chThdShouldTerminateX()) {
    // wait for event and handle it accordingly
    eventmask = chEvtWaitOne(ALL_EVENTS);

    // handle event
    switch (eventmask) {

      // OS related events
      case EVENTMASK_OS:
      {
        eventflags = chEvtGetAndClearFlags(&((aos_shell_t*)shell)->osEventListener);
        // handle shutdown/restart events
        if (eventflags & AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_MASK) {
          chThdTerminate(((aos_shell_t*)shell)->thread);
        } else {
          // print an error message
          chprintf((BaseSequentialStream*)&((aos_shell_t*)shell)->stream, "\nERROR: unknown OS event received (0x%08X)\n", eventflags);
        }
        break;
      }

      // input events
      case EVENTMASK_INPUT:
      {
        // check and handle all channels
        channel = ((aos_shell_t*)shell)->stream.channel;
        while (channel != NULL) {
          eventflags = chEvtGetAndClearFlags(&channel->listener);
          // if there is new input and a command shall be executed
          if (eventflags & CHN_INPUT_AVAILABLE) {
            _readChannel(shell, &rdata, channel);

            // if an execution request was detected
            if (rdata.lastaction == ACTION_EXECUTE) {
              streamPut(&((aos_shell_t*)shell)->stream, '\n');
              char* line = _getVisualisedEntry((aos_shell_t*)shell, &rdata);

              // skip, if there is nothing to be executed
              if (line) {
                // parse arguments
                nargs = _parseArguments(shell, line, args);
                // check number of arguments found
                if (nargs > ((aos_shell_t*)shell)->input.nargs) {
                  chprintf((BaseSequentialStream*)&((aos_shell_t*)shell)->stream, "\ttoo many arguments\n");
                } else if (nargs > 0) {
                  // search command list for arg[0] and execute callback
                  cmd = ((aos_shell_t*)shell)->commands;
                  while (cmd != NULL) {
                    // if the requested command has been found
                    if (strcmp(args[0], cmd->name) == 0) {
                      ((aos_shell_t*)shell)->execstatus.command = cmd;
                      chEvtBroadcastFlags(&((aos_shell_t*)shell)->eventSource, AOS_SHELL_EVTFLAG_EXECUTE);
                      ((aos_shell_t*)shell)->execstatus.retval = cmd->callback((BaseSequentialStream*)&((aos_shell_t*)shell)->stream, (int)nargs, args);
                      chEvtBroadcastFlags(&((aos_shell_t*)shell)->eventSource, AOS_SHELL_EVTFLAG_DONE);
                      // notify user if execution of the command was not successful
                      if (((aos_shell_t*)shell)->execstatus.retval != 0) {
                        chprintf((BaseSequentialStream*)&((aos_shell_t*)shell)->stream, "command returned exit status %d\n", ((aos_shell_t*)shell)->execstatus.retval);
                      }
                      break;
                    }
                    // keep searching
                    else {
                      cmd = cmd->next;
                    }
                  } /* end of while */

                  // if no matching command was found, print a message
                  if (cmd == NULL) {
                    chprintf((BaseSequentialStream*)&((aos_shell_t*)shell)->stream, "\tcommand '%s' not found\n", args[0]);
                  }
                }

                // restore spaces in the current entry, which have been modified to NUL bytes due to argument parsing
                _restoreWhitespace(line, rdata.input.length);

                // update internat variables
                {
                  rdata.input.length = 0;
                  rdata.input.cursorpos = 0;
                  rdata.buffer.edited = 0;

                  // if the input buffer can hold historic entries
                  if (((aos_shell_t*)shell)->input.nentries > 1) {
                    // iterate in the history only if
                    // there was some valid input and
                    // the user did not execute the exact previous command again
                    if (nargs > 0 &&
                        strcmp(line, _getRelativeEntry((aos_shell_t*)shell, &rdata, 1)) != 0) {
                      rdata.buffer.current = (rdata.buffer.current + 1) % ((aos_shell_t*)shell)->input.nentries;
                    }
                    // clear and select next (now current) entry
                    memset(_getCurrentEntry((aos_shell_t*)shell, &rdata), '\0', ((aos_shell_t*)shell)->input.linewidth * sizeof(char));
                    rdata.buffer.selected = 1;
                  }
                  // if there is only a single entry in the input buffer
                  else {
                    // do not clear/reset the entry, but start off with a cleared preview
                    rdata.buffer.selected = 0;
                  }
                }
              }

              // print a new prompt
              if (!chThdShouldTerminateX()) {
                _printPrompt((aos_shell_t*)shell);
              }
            }
          }

          // iterate to next channel
          channel = channel->next;
        }
        break;
      }

      // other events
      default:
      {
        // print an error message
        chprintf((BaseSequentialStream*)&((aos_shell_t*)shell)->stream, "\nSHELL: ERROR: unknown event received (0x%08X)\n", eventmask);
        break;
      }

    } /* end of switch */

  } /* end of while */

  // fire event and exit the thread
  chSysLock();
  chEvtBroadcastFlagsI(&(((aos_shell_t*)shell)->eventSource), AOS_SHELL_EVTFLAG_EXIT);
  chThdExitS(MSG_OK);
  // no chSysUnlock() required since the thread has been terminated an all waiting threads have been woken up
}

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/** @} */
