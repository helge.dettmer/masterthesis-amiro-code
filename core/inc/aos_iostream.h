/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_iostream.h
 * @brief   Stream and channel macros and structures.
 *
 * @addtogroup aos_stream
 * @{
 */

#ifndef AMIROOS_IOSTREAM_H
#define AMIROOS_IOSTREAM_H

#include <amiroos.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/**
 * @brief   Channel flag to indicate whether the channel is attached to a stream.
 */
#define AOS_IOCHANNEL_ATTACHED                  ((aos_iochannelflag_t)(1 << 0))

/**
 * @brief   Channel flag to indicate whether the channel is set as input.
 */
#define AOS_IOCHANNEL_INPUT_ENABLE              ((aos_iochannelflag_t)(1 << 1))

/**
 * @brief   Channel flag to indicate whether the channel is set as output.
 */
#define AOS_IOCHANNEL_OUTPUT_ENABLE             ((aos_iochannelflag_t)(1 << 2))

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

typedef uint8_t aos_iochannelflag_t;

/**
 * @brief   AosIOChannel specific methods.
 */
#define _aos_iochannel_methods                                              \
  _base_asynchronous_channel_methods

/**
 * @brief   AosIOChannel specific data.
 */
#define _aos_iochannel_data                                                 \
  /* pointer to a BaseAsynchronousChannel object */                         \
  BaseAsynchronousChannel* asyncchannel;                                    \
  /* pointer to the next channel in a AosIOStream */                        \
  struct aos_iochannel* next;                                               \
  /* flags related to the channel */                                        \
  aos_iochannelflag_t flags;

/**
 * @extends BaseAsynchronousChannelVMT
 *
 * @brief   AosIOChannel virtual methods table.
 */
struct AosIOChannelVMT {
  _aos_iochannel_methods
};

/**
 * @extends BaseAsynchronousChannel
 *
 * @brief   I/O Channel class.
 * @details This class implements an asynchronous I/O channel.
 */
typedef struct aos_iochannel {
  /** @brief Virtual Methods Table. */
  const struct AosIOChannelVMT* vmt;
  _aos_iochannel_data
} AosIOChannel;

/**
 * @brief   AosIOStream methods.
 */
#define _aos_iostream_methods                                               \
  _base_sequential_stream_methods

/**
 * @brief   AosIOStream data.
 */
#define _aos_iostream_data                                                  \
  /* Pointer to the first channel in a list. */                             \
  AosIOChannel* channel;

/**
 * @extends BaseSequentialStream
 *
 * @brief   AosIOStream virtual methods table.
 */
struct AosIOStreamVMT {
  _aos_iostream_methods
};

/**
 * @extends BaseSequentialStream
 *
 * @brief   I/O Stream class.
 * @details This class implements an base sequential stream.
 * @todo    So far only output but no input is supported.
 */
typedef struct aos_ostream {
  const struct AosIOStreamVMT* vmt;
  _aos_iostream_data
} AosIOStream;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void aosIOStreamInit(AosIOStream* stream);
  void aosIOChannelInit(AosIOChannel* channel, BaseAsynchronousChannel* asyncchannel);
  void aosIOStreamAddChannel(AosIOStream* stream, AosIOChannel* channel);
  aos_status_t aosIOStreamRemoveChannel(AosIOStream* stream, AosIOChannel* channel);
  void aosIOChannelInputEnable(AosIOChannel* channel);
  void aosIOChannelInputDisable(AosIOChannel* channel);
  void aosIOChannelOutputEnable(AosIOChannel* channel);
  void aosIOChannelOutputDisable(AosIOChannel* channel);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

#endif /* AMIROOS_IOSTREAM_H */

/** @} */
