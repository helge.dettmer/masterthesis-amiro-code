/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_system.h
 * @brief   System macros and structures.
 *
 * @addtogroup aos_system
 * @{
 */

#ifndef AMIROOS_SYSTEM_H
#define AMIROOS_SYSTEM_H

#include <amiroos.h>
#include <chprintf.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/**
 * @brief   Resolution of the system time measurement.
 */
#define AOS_SYSTEM_TIME_RESOLUTION              ((MICROSECONDS_PER_SECOND + CH_CFG_ST_FREQUENCY - 1) / CH_CFG_ST_FREQUENCY)

/**
 * @brief   System event flag mask for shutdown related events.
 */
#define AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_MASK     ((eventflags_t)0x1F)

/**
 * @brief   System event flag which is emitted when a passive shutdown was initiated.
 */
#define AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_PASSIVE  ((eventflags_t)(1 << 0))

/**
 * @brief   System event flag which is emitted when a shutdown to transportation mode was initiated.
 */
#define AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_TRANSPORTATION ((eventflags_t)(1 << 1))

/**
 * @brief   System event flag which is emitted when a shutdown to deepsleep mode was initiated.
 */
#define AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_DEEPSLEEP ((eventflags_t)(1 << 2))

/**
 * @brief   System event flag which is emitted when a shutdown to hibernate mode was initiated.
 */
#define AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_HIBERNATE ((eventflags_t)(1 << 3))

/**
 * @brief   System event flag which is emitted when a system restart was initiated.
 */
#define AOS_SYSTEM_EVENTFLAGS_SHUTDOWN_RESTART ((eventflags_t)(1 << 4))

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Enumerator to identify shutdown types.
 */
typedef enum aos_shutdown {
  AOS_SHUTDOWN_NONE,            /**< Default value if no shutdown action was initiated */
#if (AMIROOS_CFG_SSSP_ENABLE == true) || defined(__DOXYGEN__)
  AOS_SHUTDOWN_PASSIVE,         /**< Passive shutdown (initiated by another module). */
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */
#if (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_NONE) || defined (__DOXYGEN__)
  AOS_SHUTDOWN_ACTIVE,          /**< Active shutdown. */
#endif /* (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_NONE) */
#if (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT) || defined (__DOXYGEN__)
  AOS_SHUTDOWN_HIBERNATE,       /**< Active shutdown to hibernate mode. */
  AOS_SHUTDOWN_DEEPSLEEP,       /**< Active shutdown to deepsleep mode. */
  AOS_SHUTDOWN_TRANSPORTATION,  /**< Active shutdown to transportation mode. */
  AOS_SHUTDOWN_RESTART,         /**< Active saystem restart request. */
#endif /* (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT) */
} aos_shutdown_t;

/**
 * @brief   AMiRo-OS base system structure.
 */
typedef struct aos_system {

  /**
   * @brief   System I/O stream.
   */
  AosIOStream iostream;

  /**
   * @brief   Event structure.
   */
  struct {

    /**
     * @brief   GPIO event source.
     */
    event_source_t gpio;

    /**
     * @brief   OS event source.
     */
    event_source_t os;
  } events;

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

  /**
   * @brief   Pointer to the shell object.
   */
  aos_shell_t shell;

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

#if (AMIROOS_CFG_SSSP_ENABLE == true) || defined(__DOXYGEN__)

  /**
   * @brief   SSSP relevant data.
   */
  aos_ssspdata_t sssp;

#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */

} aos_system_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/**
 * @brief   Printf function that uses the default system I/O stream.
 *
 * @param[in] fmt   Formatted string to print.
 */
#define aosprintf(fmt, ...)                     chprintf((BaseSequentialStream*)&aos.iostream, fmt, ##__VA_ARGS__)

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/**
 * @brief   Global system object.
 */
extern aos_system_t aos;

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void aosSysInit(const char* shellPrompt);
  void aosSysStart(void);
  void aosSysStartUptimeS(void);
  void aosSysGetUptimeX(aos_timestamp_t* ut);
#if (HAL_USE_RTC == TRUE) || defined(__DOXYGEN__)
  void aosSysGetDateTime(struct tm* dt);
  void aosSysSetDateTime(struct tm* dt);
#endif /* (HAL_USE_RTC == TRUE) */
  void aosSysShutdownInit(aos_shutdown_t shutdown);
  void aosSysStop(void);
  void aosSysDeinit(void);
#if ((AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_SHUTDOWN != true)) ||               \
    ((AMIROOS_CFG_SSSP_ENABLE != true) && (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)) || \
    defined(__DOXYGEN__)
  void aosSysShutdownToBootloader(aos_shutdown_t shutdown);
#endif /* ((AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_SHUTDOWN != true)) || ((AMIROOS_CFG_SSSP_ENABLE != true) && (AMIROOS_CFG_BOOTLOADER == AOS_BOOTLOADER_AMiRoBLT)) */
  palcallback_t aosSysGetStdGpioCallback(void);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/**
 * @brief   Retrieves the system uptime.
 *
 * @param[out] ut   Pointer to the system uptime.
 */
static inline void aosSysGetUptime(aos_timestamp_t* ut)
{
  chSysLock();
  aosSysGetUptimeX(ut);
  chSysUnlock();
}

#endif /* AMIROOS_SYSTEM_H */

/** @} */
