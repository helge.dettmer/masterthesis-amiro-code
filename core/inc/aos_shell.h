/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    aos_shell.h
 * @brief   Shell macros and structures.
 *
 * @addtogroup aos_shell
 * @{
 */

#ifndef AMIROOS_SHELL_H
#define AMIROOS_SHELL_H

#include <amiroos.h>

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/**
 * @brief   Shell event flag that is emitted when the thread starts.
 */
#define AOS_SHELL_EVTFLAG_START                 ((eventflags_t)(1 << 0))

/**
 * @brief   Shell event flag that is emitted when a command is executed.
 */
#define AOS_SHELL_EVTFLAG_EXECUTE               ((eventflags_t)(1 << 1))

/**
 * @brief   Shell event flag that is emitted when a command execution finished.
 */
#define AOS_SHELL_EVTFLAG_DONE                  ((eventflags_t)(1 << 2))

/**
 * @brief   Shell event flag that is emitted when the shread stops.
 */
#define AOS_SHELL_EVTFLAG_EXIT                  ((eventflags_t)(1 << 3))

/**
 * @brief   Shell event flag that is emitted when an I/O error occurred.
 */
#define AOS_SHELL_EVTFLAG_IOERROR               ((eventflags_t)(1 << 4))

/**
 * @brief   Shell input configuration for replacing content by user input.
 */
#define AOS_SHELL_CONFIG_INPUT_OVERWRITE        (1 << 0)

/**
 * @brief   Shell prompt configuration print a minimalistic prompt.
 */
#define AOS_SHELL_CONFIG_PROMPT_MINIMAL         (1 << 1)

/**
 * @brief   Shell prompt configuration to additionally print the system uptime with the prompt.
 */
#define AOS_SHELL_CONFIG_PROMPT_UPTIME          (1 << 2)

/**
 * @brief   Shell prompt configuration to additionally print the date and time with the prompt.
 */
#define AOS_SHELL_CONFIG_PROMPT_DATETIME        (2 << 2)

/**
 * @brief   Shell prompt configuration to additionally print the system uptime with the prompt.
 */
#define AOS_SHELL_CONFIG_MATCH_CASE             (1 << 4)

/**
 * @brief   Shell I/O channel flag whether the channel is attached to a list.
 */
#define AOS_SHELLCHANNEL_ATTACHED               (1 << 0)

/**
 * @brief   Shell I/O channel flag whether the channel is enabled as input.
 */
#define AOS_SHELLCHANNEL_INPUT_ENABLED          (1 << 1)

/**
 * @brief   Shell I/O channel flag whether the channel is enabled as output.
 */
#define AOS_SHELLCHANNEL_OUTPUT_ENABLED         (1 << 2)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   AosShellChannel specific methods.
 */
#define _aos_shell_channel_methods                                          \
  _base_asynchronous_channel_methods

/**
 * @brief   AosShellChannel specific data.
 */
#define _aos_shell_channel_data                                             \
  /* pointer to a BaseAsynchronousChannel object */                         \
  BaseAsynchronousChannel* asyncchannel;                                    \
  /* event listener for the associated BaseAsynchronousChannel */           \
  event_listener_t listener;                                                \
  /* pointer to the next chennal in a AosShellStream */                     \
  struct aos_shellchannel* next;                                            \
  /* flags related to the channel */                                        \
  uint8_t flags;

/**
 * @extends BaseAsynchronousChannelVMT
 *
 * @brief   AosShellChannel virtual methods table.
 */
struct AosShellChannelVMT {
  _aos_shell_channel_methods
};

/**
 * @extends BaseAsynchronousChannel
 *
 * @brief   Shell channel class.
 * @details This class implements an asynchronous I/O channel.
 */
typedef struct aos_shellchannel {
  /** @brief Virtual Methods Table. */
  const struct AosShellChannelVMT* vmt;
  _aos_shell_channel_data
} AosShellChannel;

/**
 * @brief   AosShellStream methods.
 */
#define _aos_shellstream_methods                                            \
  _base_sequential_stream_methods

/**
 * @brief   AosShellStream data.
 */
#define _aos_shellstream_data                                               \
  /* Pointer to the first channel in a list. */                             \
  AosShellChannel* channel;

/**
 * @extends BaseSequentialStream
 *
 * @brief   AosShellStream virtual methods table.
 */
struct AosShellStreamVMT {
  _aos_shellstream_methods
};

/**
 * @extends BaseSequentialStream
 *
 * @brief   Shell Stream class.
 * @details This class implements an base sequential stream.
 * @todo    So far only output but no input is supported.
 */
typedef struct aos_shellstream {
  const struct AosShellStreamVMT* vmt;
  _aos_shellstream_data
} AosShellStream;

/**
 * @brief   Shell command calback type.
 *
 * @param[in] stream  Stream to print to.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    List of arguments.
 */
typedef int (*aos_shellcmdcb_t)(BaseSequentialStream* stream, int argc, char* argv[]);

/**
 * @brief   Shell command structure.
 */
typedef struct aos_shellcommand {
  /**
   * @brief   Command name.
   */
  const char* name;

  /**
   * @brief   Callback function.
   */
  aos_shellcmdcb_t callback;

  /**
   * @brief   Pointer to next command in a singly linked list.
   */
  struct aos_shellcommand* next;

} aos_shellcommand_t;

/**
 * @brief   Execution status of a shell command.
 */
typedef struct aos_shellexecstatus {
  /**
   * @brief   Pointer to the command that was executed.
   */
  aos_shellcommand_t* command;

  /**
   * @brief   Return value of the executed command.
   */
  int retval;
} aos_shellexecstatus_t;

/**
 * @brief   Shell structure.
 */
typedef struct aos_shell {
  /**
   * @brief   Pointer to the thread object.
   */
  thread_t* thread;

#if (CH_CFG_USE_REGISTRY == TRUE) || defined(__DOXYGEN__)

  /**
   * @brief   Name of the shell and the associated thread.
   */
  const char* name;

#endif /* (CH_CFG_USE_REGISTRY == TRUE) */

  /**
   * @brief   Event source.
   */
  event_source_t eventSource;

  /**
   * @brief   Listener for OS related events.
   */
  event_listener_t osEventListener;

  /**
     * @brief   Pointer to the first I/O channel.
     */
  AosShellStream stream;

  /**
   * @brief   String to printed as prompt.
   */
  const char* prompt;

  /**
   * @brief   Pointer to the first element of the singly linked list of commands.
   * @details Commands are ordered alphabetically in the list.
   */
  aos_shellcommand_t* commands;

  /**
   * @brief   Execution status of the most recent command.
   */
  aos_shellexecstatus_t execstatus;

  /**
   * @brief   Structure containing all input data.
   */
  struct {
    /**
     * @brief   Input buffer.
     * @details This buffer is interpreted as two dimensional array.
     *          It contains @p nentries elements of @p linewidth size each.
     */
    char* buffer;

    /**
     * @brief   Number of entries in the buffer.
     * @note    Must be >0.
     */
    size_t nentries;

    /**
     * @brief   Width of each input line.
     * @brief   Must be >0.
     */
    size_t linewidth;

    /**
     * @brief   Size of the argument buffer.
     * @note    Must be >0.
     */
    size_t nargs;
  } input;

  /**
   * @brief   Configuration flags.
   */
  uint8_t config;
} aos_shell_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/**
 * @brief   Initializes a shell command object.
 *
 * @param[in] var       Name of the object variable to be initialized.
 * @param[in] name      Shell command name.
 * @param[in] callback  Pointer to the callback function.
 */
#define AOS_SHELL_COMMAND(var, name, callback) aos_shellcommand_t var = {     \
  /* name     */ name,                                                        \
  /* callback */ callback,                                                    \
  /* next     */ NULL,                                                        \
}

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void aosShellInit(aos_shell_t* shell, const char* name, const char* prompt, char inbuf[], size_t entries, size_t linewidth, size_t numargs);
  void aosShellStreamInit(AosShellStream* stream);
  void aosShellChannelInit(AosShellChannel* channel, BaseAsynchronousChannel* asyncchannel);
  aos_status_t aosShellAddCommand(aos_shell_t* shell, aos_shellcommand_t* cmd);
  aos_status_t aosShellRemoveCommand(aos_shell_t* shell, char* cmd, aos_shellcommand_t** removed);
  unsigned int aosShellCountCommands(aos_shell_t* shell);
  void aosShellStreamAddChannel(AosShellStream* stream, AosShellChannel* channel);
  aos_status_t aosShellStreamRemoveChannel(AosShellStream* stream, AosShellChannel* channel);
  void aosShellChannelInputEnable(AosShellChannel* channel);
  void aosShellChannelInputDisable( AosShellChannel* channel);
  void aosShellChannelOutputEnable(AosShellChannel* channel);
  void aosShellChannelOutputDisable(AosShellChannel* channel);
  THD_FUNCTION(aosShellThread, shell);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

#endif /* AMIROOS_SHELL_H */

/** @} */
