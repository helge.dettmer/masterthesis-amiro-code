/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <amiroos.h>

#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

#include <module_test_A3906.h>
#include <aos_test_A3906.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

static aos_test_a3906data_t _data = {
  /* driver           */ &moduleLldMotors,
  /* PWM information  */ {
    /* driver   */ &MODULE_HAL_PWM_DRIVE,
    /* channels */ {
      /* left wheel forward   */ MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD,
      /* left wheel backward  */ MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD,
      /* right wheel forward  */ MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD,
      /* right wheel backward */ MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD,
    },
  },
  /* QEI information  */ {
    /* left wheel               */ &MODULE_HAL_QEI_LEFT_WHEEL,
    /* right wheel              */ &MODULE_HAL_QEI_RIGHT_WHEEL,
    /* increment per revolution */ MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
  },
  /* Wheel diameter   */ {
    /* left wheel   */ 0.05571f,
    /* right wheel  */ 0.05571f,
  },
  /* timeout          */ 10 * MICROSECONDS_PER_SECOND,
};

static AOS_TEST(_test, "A3906", "motor driver", moduleTestA3906ShellCb, aosTestA3906Func, &_data);

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

int moduleTestA3906ShellCb(BaseSequentialStream* stream, int argc, char* argv[], aos_testresult_t* result)
{
  (void)argc;
  (void)argv;

  if (result != NULL) {
    *result = aosTestRun(stream, &_test, NULL);
  } else {
    aosTestRun(stream, &_test, NULL);
  }

  return AOS_OK;
}

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */
