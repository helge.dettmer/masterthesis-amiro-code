/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the DiWheelDrive module.
 *
 * @addtogroup diwheeldrive_module
 * @{
 */

#include <amiroos.h>

#include <string.h>

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

CANConfig moduleHalCanConfig = {
  /* mcr  */ CAN_MCR_ABOM | CAN_MCR_AWUM | CAN_MCR_TXFP,
  /* btr  */ CAN_BTR_SJW(1) | CAN_BTR_TS2(2) | CAN_BTR_TS1(13) | CAN_BTR_BRP(1),
};

I2CConfig moduleHalI2cCompassConfig = {
  /* I²C mode   */ OPMODE_I2C,
  /* frequency  */ 400000,
  /* duty cycle */ FAST_DUTY_CYCLE_2,
};

I2CConfig moduleHalI2cProxEepromPwrmtrConfig = {
  /* I²C mode   */ OPMODE_I2C,
  /* frequency  */ 400000,
  /* duty cycle */ FAST_DUTY_CYCLE_2,
};

PWMConfig moduleHalPwmDriveConfig = {
  /* frequency              */ 7200000,
  /* period                 */ 360,
  /* callback               */ NULL,
  /* channel configurations */ {
    /* channel 0              */ {
      /* mode                   */ PWM_OUTPUT_ACTIVE_HIGH,
      /* callback               */ NULL
    },
    /* channel 1              */ {
      /* mode                   */ PWM_OUTPUT_ACTIVE_HIGH,
      /* callback               */ NULL
    },
    /* channel 2              */ {
      /* mode                   */ PWM_OUTPUT_ACTIVE_HIGH,
      /* callback               */ NULL
    },
    /* channel 3              */ {
      /* mode                   */ PWM_OUTPUT_ACTIVE_HIGH,
      /* callback               */ NULL
    },
  },
  /* TIM CR2 register       */ 0,
#if (STM32_PWM_USE_ADVANCED == TRUE)
  /* TIM BDTR register      */ 0,
#endif /* (STM32_PWM_USE_ADVANCED == TRUE) */
  /* TIM DIER register      */ 0
};

QEIConfig moduleHalQeiConfig = {
  /* mode           */ QEI_COUNT_BOTH,
  /* channel config */ {
    /* channel 0 */ {
      /* input mode */ QEI_INPUT_NONINVERTED,
    },
    /* channel 1 */ {
      /* input mode */ QEI_INPUT_NONINVERTED,
    },
  },
  /* encoder range  */  0x10000u,
};

SerialConfig moduleHalProgIfConfig = {
  /* bit rate */ 115200,
  /* CR1      */ 0,
  /* CR1      */ 0,
  /* CR1      */ 0,
};

SPIConfig moduleHalSpiAccelerometerConfig = {
  /* circular buffer mode         */ false,
  /* callback function pointer    */ NULL,
  /* chip select line port        */ PAL_PORT(LINE_ACCEL_SS_N),
  /* chip select line pad number  */ PAL_PAD(LINE_ACCEL_SS_N),
  /* CR1                          */ SPI_CR1_BR_0,
  /* CR2                          */ SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN,
};

SPIConfig moduleHalSpiGyroscopeConfig = {
  /* circular buffer mode         */ false,
  /* callback function pointer    */ NULL,
  /* chip select line port        */ PAL_PORT(LINE_GYRO_SS_N),
  /* chip select line pad number  */ PAL_PAD(LINE_GYRO_SS_N),
  /* CR1                          */ SPI_CR1_BR_0,
  /* CR2                          */ SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN,
};

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LED output signal GPIO.
 */
static apalGpio_t _gpioLed = {
  /* line */ LINE_LED,
};
ROMCONST apalControlGpio_t moduleGpioLed = {
  /* GPIO */ &_gpioLed,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_OUTPUT,
    /* active state   */ APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ APAL_GPIO_EDGE_NONE,
  },
};

/**
 * @brief   POWER_EN output signal GPIO.
 */
static apalGpio_t _gpioPowerEn = {
  /* line */ LINE_POWER_EN,
};
ROMCONST apalControlGpio_t moduleGpioPowerEn = {
  /* GPIO */ &_gpioPowerEn,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_OUTPUT,
    /* active state   */ APAL_GPIO_ACTIVE_HIGH,
    /* interrupt edge */ APAL_GPIO_EDGE_NONE,
  },
};

/**
 * @brief   COMPASS_DRDY output signal GPIO.
 */
static apalGpio_t _gpioCompassDrdy = {
  /* line */ LINE_COMPASS_DRDY,
};
ROMCONST apalControlGpio_t moduleGpioCompassDrdy = {
  /* GPIO */ &_gpioCompassDrdy,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ (L3G4200D_LLD_INT_EDGE == APAL_GPIO_EDGE_RISING) ? APAL_GPIO_ACTIVE_HIGH : APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ L3G4200D_LLD_INT_EDGE,
  },
};

/**
 * @brief   IR_INT input signal GPIO.
 */
static apalGpio_t _gpioIrInt = {
  /* line */ LINE_IR_INT,
};
ROMCONST apalControlGpio_t moduleGpioIrInt = {
  /* GPIO */ &_gpioIrInt,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ (VCNL4020_LLD_INT_EDGE == APAL_GPIO_EDGE_RISING) ? APAL_GPIO_ACTIVE_HIGH : APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ VCNL4020_LLD_INT_EDGE,
  },
};

/**
 * @brief   GYRO_DRDY input signal GPIO.
 */
static apalGpio_t _gpioGyroDrdy = {
  /* line */ LINE_GYRO_DRDY,
};
ROMCONST apalControlGpio_t moduleGpioGyroDrdy = {
  /* GPIO */ &_gpioGyroDrdy,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ (L3G4200D_LLD_INT_EDGE == APAL_GPIO_EDGE_RISING) ? APAL_GPIO_ACTIVE_HIGH : APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ L3G4200D_LLD_INT_EDGE,
  },
};

/**
 * @brief   SYS_UART_UP bidirectional signal GPIO.
 */
static apalGpio_t _gpioSysUartUp = {
  /* line */ LINE_SYS_UART_UP,
};
ROMCONST apalControlGpio_t moduleGpioSysUartUp = {
  /* GPIO */ &_gpioSysUartUp,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_BIDIRECTIONAL,
    /* active state   */ APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/**
 * @brief   ACCEL_INT input signal GPIO.
 */
static apalGpio_t _gpioAccelInt = {
  /* line */ LINE_ACCEL_INT_N,
};
ROMCONST apalControlGpio_t moduleGpioAccelInt = {
  /* GPIO */ &_gpioAccelInt,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ (LIS331DLH_LLD_INT_EDGE == APAL_GPIO_EDGE_RISING) ? APAL_GPIO_ACTIVE_HIGH : APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ LIS331DLH_LLD_INT_EDGE,
  },
};

/**
 * @brief   SYS_SNYC bidirectional signal GPIO.
 */
static apalGpio_t _gpioSysSync = {
  /* line */ LINE_SYS_INT_N,
};
ROMCONST apalControlGpio_t  moduleGpioSysSync = {
  /* GPIO */ &_gpioSysSync,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_BIDIRECTIONAL,
    /* active state   */ APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/**
 * @brief   PATH_DCSTAT input signal GPIO.
 */
static apalGpio_t _gpioPathDcStat = {
  /* line */ LINE_PATH_DCSTAT,
};
ROMCONST apalControlGpio_t moduleGpioPathDcStat = {
  /* GPIO */ &_gpioPathDcStat,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ LTC4412_LLD_STAT_ACTIVE_STATE,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/**
 * @brief   PATH_DCEN output signal GPIO.
 */
static apalGpio_t _gpioPathDcEn = {
  /* line */ LINE_PATH_DCEN,
};
ROMCONST apalControlGpio_t moduleGpioPathDcEn = {
  /* GPIO */ &_gpioPathDcEn,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_OUTPUT,
    /* active state   */ LTC4412_LLD_CTRL_ACTIVE_STATE,
    /* interrupt edge */ APAL_GPIO_EDGE_NONE,
  },
};

/**
 * @brief   SYS_PD bidirectional signal GPIO.
 */
static apalGpio_t _gpioSysPd = {
  /* line */ LINE_SYS_PD_N,
};
ROMCONST apalControlGpio_t moduleGpioSysPd = {
  /* GPIO */ &_gpioSysPd,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_BIDIRECTIONAL,
    /* active state   */ APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/**
 * @brief   SYS_REG_EN input signal GPIO.
 */
static apalGpio_t _gpioSysRegEn = {
  /* line */ LINE_SYS_REG_EN,
};
ROMCONST apalControlGpio_t moduleGpioSysRegEn = {
  /* GPIO */ &_gpioSysRegEn,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ APAL_GPIO_ACTIVE_HIGH,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/**
 * @brief   SYS_WARMRST bidirectional signal GPIO.
 */
static apalGpio_t _gpioSysWarmrst = {
  /* line */ LINE_SYS_WARMRST_N,
};
ROMCONST apalControlGpio_t moduleGpioSysWarmrst = {
  /* GPIO */ &_gpioSysWarmrst,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_BIDIRECTIONAL,
    /* active state   */ APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
ROMCONST char* moduleShellPrompt = "DiWheelDrive";
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

#if ((AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_MSI == true)) || defined(__DOXYGEN__)

/* some local definitions */
// maximum number of bytes per CAN frame
#define CAN_BYTES_PER_FRAME                     8
// identifier (as dominant as possible)
#define MSI_BCBMSG_CANID                        0

aos_ssspbcbstatus_t moduleSsspBcbTransmit(const uint8_t* buffer, size_t length)
{
  aosDbgCheck(buffer != NULL);
  aosDbgCheck(length > 0 && length <= CAN_BYTES_PER_FRAME);

  // local variables
  CANTxFrame frame;

  // setup the common parts of the message frame
  frame.DLC = (uint8_t)length;
  frame.RTR = CAN_RTR_DATA;
  frame.IDE = CAN_IDE_STD;
  frame.SID = MSI_BCBMSG_CANID;
  memcpy(frame.data8, buffer, length);

  // sent the frame and return
  return (canTransmitTimeout(&MODULE_HAL_CAN, CAN_ANY_MAILBOX, &frame, TIME_IMMEDIATE) == MSG_OK) ? AOS_SSSP_BCB_SUCCESS : AOS_SSSP_BCB_ERROR;
}

aos_ssspbcbstatus_t moduleSsspBcbReceive(uint8_t* buffer, size_t length)
{
  aosDbgCheck(buffer != NULL);
  aosDbgCheck(length > 0 && length <= CAN_BYTES_PER_FRAME);

  // local variables
  CANRxFrame frame;

  // receive a frame and check for errors
  if (canReceiveTimeout(&MODULE_HAL_CAN, CAN_ANY_MAILBOX, &frame, TIME_IMMEDIATE) == MSG_OK) {
    // a correct frame was received
    if (frame.DLC == length &&
        frame.RTR == CAN_RTR_DATA &&
        frame.IDE == CAN_IDE_STD &&
        frame.SID == MSI_BCBMSG_CANID) {
      // success: fetch the data and return
      memcpy(buffer, frame.data8, length);
      return AOS_SSSP_BCB_SUCCESS;
    }
    // an unexpected frame was received
    else {
      return AOS_SSSP_BCB_INVALIDMSG;
    }
  } else {
    // failure: return with error
    return AOS_SSSP_BCB_ERROR;
  }
}

#undef MSI_BCBMSG_CANID
#undef CAN_BYTES_PER_FRAME

#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) && (AMIROOS_CFG_SSSP_MSI == true) */

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/

A3906Driver moduleLldMotors = {
  /* power enable GPIO  */ &moduleGpioPowerEn,
};

AT24C01BDriver moduleLldEeprom = {
  /* I2C driver   */ &MODULE_HAL_I2C_PROX_EEPROM_PWRMTR,
  /* I²C address  */ AT24C01B_LLD_I2C_ADDR_FIXED,
};

HMC5883LDriver moduleLldCompass = {
  /* I²C Driver */ &MODULE_HAL_I2C_COMPASS,
};

INA219Driver moduleLldPowerMonitorVdd = {
  /* I2C Driver       */ &MODULE_HAL_I2C_PROX_EEPROM_PWRMTR,
  /* I²C address      */ INA219_LLD_I2C_ADDR_FIXED,
  /* current LSB (uA) */ 0x00u,
  /* configuration    */ NULL,
};

L3G4200DDriver moduleLldGyroscope = {
  /* SPI Driver */ &MODULE_HAL_SPI_MOTION,
};

LEDDriver moduleLldStatusLed = {
  /* LED enable Gpio */ &moduleGpioLed,
};

LIS331DLHDriver moduleLldAccelerometer = {
  /* SPI Driver */ &MODULE_HAL_SPI_MOTION,
};

LTC4412Driver moduleLldPowerPathController = {
  /* Control GPIO */ &moduleGpioPathDcEn,
  /* Status GPIO  */ &moduleGpioPathDcStat,
};

PCA9544ADriver moduleLldI2cMultiplexer = {
  /* I²C driver   */ &MODULE_HAL_I2C_PROX_EEPROM_PWRMTR,
  /* I²C address  */ PCA9544A_LLD_I2C_ADDR_FIXED | PCA9544A_LLD_I2C_ADDR_A0 | PCA9544A_LLD_I2C_ADDR_A1 | PCA9544A_LLD_I2C_ADDR_A2,
};

TPS6211xDriver moduleLldStepDownConverterVdrive = {
  /* Power enable Gpio */ &moduleGpioPowerEn,
};

VCNL4020Driver moduleLldProximity = {
  /* I²C Driver */ &MODULE_HAL_I2C_PROX_EEPROM_PWRMTR,
};

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/*
 * A3906 (motor driver)
 */
#include <module_test_A3906.h>
static int _testA3906ShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestA3906ShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestA3906ShellCmd, "test:MotorDriver", _testA3906ShellCmdCb);

/*
 * AT24C01BN-SH-B (EEPROM)
 */
#include <module_test_AT24C01B.h>
static int _testAt24co1bShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestAt24c01bShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestAt24c01bShellCmd, "test:EEPROM", _testAt24co1bShellCmdCb);

/*
 * HMC5883L (compass)
 */
#include <module_test_HMC5883L.h>
static int _testHmc5883lShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestHmc5883lShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestHmc5883lShellCmd, "test:Compass", _testHmc5883lShellCmdCb);

/*
 * INA219 (power monitor)
 */
#include <module_test_INA219.h>
static int _testIna219ShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestIna219ShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestIna219ShellCmd, "test:PowerMonitor", _testIna219ShellCmdCb);

/*
 * L3G4200D (gyroscope)
 */
#include <module_test_L3G4200D.h>
static int _testL3g4200dShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestL3g4200dShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestL3g4200dShellCmd, "test:Gyroscope", _testL3g4200dShellCmdCb);

/*
 * Status LED
 */
#include <module_test_LED.h>
static int _testLedShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestLedShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestLedShellCmd, "test:StatusLED", _testLedShellCmdCb);

/*
 * LIS331DLH (accelerometer)
 */
#include <module_test_LIS331DLH.h>
static int _testLis331dlhShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestLis331dlhShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestLis331dlhShellCmd, "test:Accelerometer", _testLis331dlhShellCmdCb);

/*
 * LTC4412 (power path controller)
 */
#include <module_test_LTC4412.h>
static int _testLtc4412ShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestLtc4412ShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestLtc4412ShellCmd, "test:PowerPathController", _testLtc4412ShellCmdCb);

/*
 * PCA9544A (I2C multiplexer)
 */
#include <module_test_PCA9544A.h>
static int _testPca9544aShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestPca9544aShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestPca9544aShellCmd, "test:I2CMultiplexer", _testPca9544aShellCmdCb);

/*
 * TPS62113 (step-down converter)
 */
#include <module_test_TPS6211x.h>
static int _testTps6211xShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestTps6211xShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestTps62113ShellCmd, "test:StepDownConverter", _testTps6211xShellCmdCb);

/*
 * VCNL4020 (proximity sensor)
 */
#include <module_test_VCNL4020.h>
static int _testVcnl4020ShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestVcnl4020ShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestVcnl4020ShellCmd, "test:ProximitySensor", _testVcnl4020ShellCmdCb);

/*
 * entire module
 */
static int _testAllShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  int status = AOS_OK;
  char* targv[AMIROOS_CFG_SHELL_MAXARGS] = {NULL};
  aos_testresult_t result_test = {0, 0};
  aos_testresult_t result_total = {0, 0};

  /* A3906 (motor driver) */
  status |= moduleTestA3906ShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* AT24C01B (EEPROM) */
  status |= moduleTestAt24c01bShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* HMC5883L (compass) */
  status |= moduleTestHmc5883lShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* INA219 (power monitor) */
  status |= moduleTestIna219ShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* L3G4200D (gyroscope) */
  status |= moduleTestL3g4200dShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* Status LED */
  status |= moduleTestLedShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* LIS331DLH (accelerometer) */
  status |= moduleTestLis331dlhShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* LTC4412 (power path controller) */
  status |= moduleTestLtc4412ShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* PCA9544A (I2C multiplexer) */
  status |= moduleTestPca9544aShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* TPS62113 (step-down converter) */
  status |= moduleTestTps6211xShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* VCNL4020 (proximity sensor) */
  // wheel left
  targv[1] = "-wl";
  status |= moduleTestVcnl4020ShellCb(stream, 2, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);
  // front left
  targv[1] = "-fl";
  status |= moduleTestVcnl4020ShellCb(stream, 2, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);
  // front right
  targv[1] = "-fr";
  status |= moduleTestVcnl4020ShellCb(stream, 2, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);
  // wheel right
  targv[1] = "-wr";
  status |= moduleTestVcnl4020ShellCb(stream, 2, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);
  targv[1] = "";

  // print total result
  chprintf(stream, "\n");
  aosTestResultPrintSummary(stream, &result_total, "entire module");

  return status;
}
AOS_SHELL_COMMAND(moduleTestAllShellCmd, "test:all", _testAllShellCmdCb);

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */
/** @} */
