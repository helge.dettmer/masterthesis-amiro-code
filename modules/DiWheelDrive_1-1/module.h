/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the DiWheelDrive module.
 *
 * @addtogroup diwheeldrive_module
 * @{
 */

#ifndef AMIROOS_MODULE_H
#define AMIROOS_MODULE_H

#include <amiroos.h>

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

/**
 * @brief   CAN driver to use.
 */
#define MODULE_HAL_CAN                          CAND1

/**
 * @brief   Configuration for the CAN driver.
 */
extern CANConfig moduleHalCanConfig;

/**
 * @brief   I2C driver to access the compass.
 */
#define MODULE_HAL_I2C_COMPASS                  I2CD1

/**
 * @brief   Configuration for the compass I2C driver.
 */
extern I2CConfig moduleHalI2cCompassConfig;

/**
 * @brief   I2C driver to access multiplexer, proximity sensors, EEPROM and power monitor.
 */
#define MODULE_HAL_I2C_PROX_EEPROM_PWRMTR       I2CD2

/**
 * @brief   Configuration for the multiplexer, proximity, EEPROM and power monitor I2C driver.
 */
extern I2CConfig moduleHalI2cProxEepromPwrmtrConfig;

/**
 * @brief   PWM driver to use.
 */
#define MODULE_HAL_PWM_DRIVE                    PWMD2

/**
 * @brief   Configuration for the PWM driver.
 */
extern PWMConfig moduleHalPwmDriveConfig;

/**
 * @brief   Drive PWM channel for the left wheel forward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD     ((apalPWMchannel_t)0)

/**
 * @brief   Drive PWM channel for the left wheel backward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD    ((apalPWMchannel_t)1)

/**
 * @brief   Drive PWM channel for the right wheel forward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD    ((apalPWMchannel_t)2)

/**
 * @brief   Drive PWM channel for the right wheel backward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD   ((apalPWMchannel_t)3)

/**
 * @brief   Quadrature encooder for the left wheel.
 */
#define MODULE_HAL_QEI_LEFT_WHEEL               QEID3

/**
 * @brief   Quadrature encooder for the right wheel.
 */
#define MODULE_HAL_QEI_RIGHT_WHEEL              QEID4

/**
 * @brief   Configuration for both quadrature encoders.
 */
extern QEIConfig moduleHalQeiConfig;

/**
 * @brief   QEI increments per wheel revolution.
 * @details 2 signal edges per pulse * 2 signals * 16 pulses per motor revolution * 22:1 gearbox
 */
#define MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION  (apalQEICount_t)(2 * 2 * 16 * 22)

/**
 * @brief   Serial driver of the programmer interface.
 */
#define MODULE_HAL_PROGIF                       SD1

/**
 * @brief   Configuration for the programmer serial interface driver.
 */
extern SerialConfig moduleHalProgIfConfig;

/**
 * @brief   SPI interface driver for the motion sensors (gyroscope and accelerometer).
 */
#define MODULE_HAL_SPI_MOTION                   SPID1

/**
 * @brief   Configuration for the motion sensor SPI interface  driver to communicate with the accelerometer.
 */
extern SPIConfig moduleHalSpiAccelerometerConfig;

/**
 * @brief   Configuration for the motion sensor SPI interface  driver to communicate with the gyroscope.
 */
extern SPIConfig moduleHalSpiGyroscopeConfig;

/**
 * @brief   Real-Time Clock driver.
 */
#define MODULE_HAL_RTC                          RTCD1

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LED output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLed;

/**
 * @brief   POWER_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioPowerEn;

/**
 * @brief   COMPASS_DRDY input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioCompassDrdy;

/**
 * @brief   IR_INT input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioIrInt;

/**
 * @brief   GYRO_DRDY input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioGyroDrdy;

/**
 * @brief   SYS_UART_UP bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysUartUp;

/**
 * @brief   ACCEL_INT input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioAccelInt;

/**
 * @brief   SYS_SNYC bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysSync;

/**
 * @brief   PATH_DCSTAT input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioPathDcStat;

/**
 * @brief   PATH_DCEN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioPathDcEn;

/**
 * @brief   SYS_PD bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysPd;

/**
 * @brief   SYS_REG_EN input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysRegEn;

/**
 * @brief   SYS_WARMRST bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysWarmrst;

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Event flag to be set on a SYS_SYNC interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSSYNC         AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_INT_N))

/**
 * @brief   Event flag to be set on a SYS_WARMRST interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSWARMRST      AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_WARMRST_N))

/**
 * @brief   Event flag to be set on a PATH_DCSTAT interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_PATHDCSTAT      AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_PATH_DCEN))

/**
 * @brief   Event flag to be set on a COMPASS_DRDY interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_COMPASSDRDY     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_COMPASS_DRDY))

/**
 * @brief   Event flag to be set on a SYS_PD interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSPD           AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_PD_N))

/**
 * @brief   Event flag to be set on a SYS_REG_EN interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSREGEN        AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_REG_EN))

/**
 * @brief   Event flag to be set on a IR_INT interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_IRINT           AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IR_INT))

/**
 * @brief   Event flag to be set on a GYRO_DRDY interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_GYRODRDY        AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_GYRO_DRDY))

/**
 * @brief   Event flag to be set on a SYS_UART_UP interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSUARTUP       AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_UART_UP))

/**
 * @brief   Event flag to be set on a ACCEL_INT interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_ACCELINT        AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_ACCEL_INT_N))

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
/**
 * @brief   Shell prompt text.
 */
extern ROMCONST char* moduleShellPrompt;
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/**
 * @brief   Interrupt initialization macro.
 */
#define MODULE_INIT_INTERRUPTS() {                                            \
  /* COMPASS_DRDY */                                                          \
  palSetLineCallback(moduleGpioCompassDrdy.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioCompassDrdy.gpio->line);  \
  palEnableLineEvent(moduleGpioCompassDrdy.gpio->line, APAL2CH_EDGE(moduleGpioCompassDrdy.meta.edge));                  \
  /* IR_INT */                                                                \
  palSetLineCallback(moduleGpioIrInt.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioIrInt.gpio->line);  \
  palEnableLineEvent(moduleGpioIrInt.gpio->line, APAL2CH_EDGE(moduleGpioIrInt.meta.edge));                  \
  /* GYRO_DRDY */                                                             \
  palSetLineCallback(moduleGpioGyroDrdy.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioGyroDrdy.gpio->line);  \
  palEnableLineEvent(moduleGpioGyroDrdy.gpio->line, APAL2CH_EDGE(moduleGpioGyroDrdy.meta.edge));                  \
  /* ACCEL_INT */                                                             \
  palSetLineCallback(moduleGpioAccelInt.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioAccelInt.gpio->line);  \
  palEnableLineEvent(moduleGpioAccelInt.gpio->line, APAL2CH_EDGE(moduleGpioAccelInt.meta.edge));                  \
  /* PATH_DCSTAT */                                                           \
  palSetLineCallback(moduleGpioPathDcStat.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioPathDcStat.gpio->line);  \
  palEnableLineEvent(moduleGpioPathDcStat.gpio->line, APAL2CH_EDGE(moduleGpioPathDcStat.meta.edge));                  \
  /* SYS_REG_EN */                                                            \
  palSetLineCallback(moduleGpioSysRegEn.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioSysRegEn.gpio->line);  \
  palEnableLineEvent(moduleGpioSysRegEn.gpio->line, APAL2CH_EDGE(moduleGpioSysRegEn.meta.edge));                  \
  /* SYS_WARMRST */                                                           \
  palSetLineCallback(moduleGpioSysWarmrst.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioSysWarmrst.gpio->line);  \
  palEnableLineEvent(moduleGpioSysWarmrst.gpio->line, APAL2CH_EDGE(moduleGpioSysWarmrst.meta.edge));                  \
}

/**
 * @brief   Test initialization hook.
 */
#define MODULE_INIT_TESTS() {                                                 \
  /* add test commands to shell */                                            \
  aosShellAddCommand(&aos.shell, &moduleTestA3906ShellCmd);                   \
  aosShellAddCommand(&aos.shell, &moduleTestAt24c01bShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestHmc5883lShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestIna219ShellCmd);                  \
  aosShellAddCommand(&aos.shell, &moduleTestL3g4200dShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestLedShellCmd);                     \
  aosShellAddCommand(&aos.shell, &moduleTestLis331dlhShellCmd);               \
  aosShellAddCommand(&aos.shell, &moduleTestLtc4412ShellCmd);                 \
  aosShellAddCommand(&aos.shell, &moduleTestPca9544aShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestTps62113ShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestVcnl4020ShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestAllShellCmd);                     \
}

/**
 * @brief   Periphery communication interfaces initialization hook.
 */
#define MODULE_INIT_PERIPHERY_IF() {                                          \
  /* serial driver */                                                         \
  sdStart(&MODULE_HAL_PROGIF, &moduleHalProgIfConfig);                        \
  /* I2C */                                                                   \
  moduleHalI2cCompassConfig.clock_speed = (HMC5883L_LLD_I2C_MAXFREQUENCY < moduleHalI2cCompassConfig.clock_speed) ? HMC5883L_LLD_I2C_MAXFREQUENCY : moduleHalI2cCompassConfig.clock_speed;  \
  moduleHalI2cCompassConfig.duty_cycle = (moduleHalI2cCompassConfig.clock_speed <= 100000) ? STD_DUTY_CYCLE : FAST_DUTY_CYCLE_2;  \
  i2cStart(&MODULE_HAL_I2C_COMPASS, &moduleHalI2cCompassConfig);              \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (PCA9544A_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? PCA9544A_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (VCNL4020_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? VCNL4020_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (AT24C01B_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? AT24C01B_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (INA219_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? INA219_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.duty_cycle = (moduleHalI2cProxEepromPwrmtrConfig.clock_speed <= 100000) ? STD_DUTY_CYCLE : FAST_DUTY_CYCLE_2;  \
  i2cStart(&MODULE_HAL_I2C_PROX_EEPROM_PWRMTR, &moduleHalI2cProxEepromPwrmtrConfig);  \
  /* SPI is shared between accelerometer and gyroscope and needs to be restarted for each transmission */ \
  /* PWM */                                                                   \
  pwmStart(&MODULE_HAL_PWM_DRIVE, &moduleHalPwmDriveConfig);                  \
  /* QEI */                                                                   \
  qeiStart(&MODULE_HAL_QEI_LEFT_WHEEL, &moduleHalQeiConfig);                  \
  qeiStart(&MODULE_HAL_QEI_RIGHT_WHEEL, &moduleHalQeiConfig);                 \
  qeiEnable(&MODULE_HAL_QEI_LEFT_WHEEL);                                      \
  qeiEnable(&MODULE_HAL_QEI_RIGHT_WHEEL);                                     \
  /* CAN */                                                                   \
  canStart(&MODULE_HAL_CAN, &moduleHalCanConfig);                             \
}

/**
 * @brief   Periphery communication interface deinitialization hook.
 */
#define MODULE_SHUTDOWN_PERIPHERY_IF() {                                      \
  /* CAN */                                                                   \
  canStop(&MODULE_HAL_CAN);                                                   \
  /* PWM */                                                                   \
  pwmStop(&MODULE_HAL_PWM_DRIVE);                                             \
  /* QEI */                                                                   \
  qeiDisable(&MODULE_HAL_QEI_LEFT_WHEEL);                                     \
  qeiDisable(&MODULE_HAL_QEI_RIGHT_WHEEL);                                    \
  qeiStop(&MODULE_HAL_QEI_LEFT_WHEEL);                                        \
  qeiStop(&MODULE_HAL_QEI_RIGHT_WHEEL);                                       \
  /* I2C */                                                                   \
  i2cStop(&MODULE_HAL_I2C_COMPASS);                                           \
  i2cStop(&MODULE_HAL_I2C_PROX_EEPROM_PWRMTR);                                \
  /* don't stop the serial driver so messages can still be printed */         \
}

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

#define moduleSsspSignalPD()                    (&moduleGpioSysPd)
#define moduleSsspEventflagPD()                 MODULE_OS_GPIOEVENTFLAG_SYSPD

#define moduleSsspSignalS()                     (&moduleGpioSysSync)
#define moduleSsspEventflagS()                  MODULE_OS_GPIOEVENTFLAG_SYSSYNC

#define moduleSsspSignalUP()                    (&moduleGpioSysUartUp)
#define moduleSsspEventflagUP()                 MODULE_OS_GPIOEVENTFLAG_SYSUARTUP

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/
#include <alld_A3906.h>
#include <alld_AT24C01B.h>
#include <alld_HMC5883L.h>
#include <alld_INA219.h>
#include <alld_L3G4200D.h>
#include <alld_LED.h>
#include <alld_LIS331DLH.h>
#include <alld_LTC4412.h>
#include <alld_PCA9544A.h>
#include <alld_TPS6211x.h>
#include <alld_VCNL4020.h>

/**
 * @brief   Motor driver.
 */
extern A3906Driver moduleLldMotors;

/**
 * @brief   EEPROM driver.
 */
extern AT24C01BDriver moduleLldEeprom;

/**
 * @brief   Compass driver.
 */
extern HMC5883LDriver moduleLldCompass;

/**
 * @brief   Power monitor (VDD) driver.
 */
extern INA219Driver moduleLldPowerMonitorVdd;

/**
 * @brief   Gyroscope driver.
 */
extern L3G4200DDriver moduleLldGyroscope;

/**
 * @brief   Status LED driver.
 */
extern LEDDriver moduleLldStatusLed;

/**
 * @brief   Accelerometer driver.
 */
extern LIS331DLHDriver moduleLldAccelerometer;

/**
 * @brief   Power path controler (charging pins) driver.
 */
extern LTC4412Driver moduleLldPowerPathController;

/**
 * @brief   I2C multiplexer driver.
 */
extern PCA9544ADriver moduleLldI2cMultiplexer;

/**
 * @brief   Step down converter (VDRIVE) driver.
 */
extern TPS6211xDriver moduleLldStepDownConverterVdrive;

/**
 * @brief   Proximity sensor driver.
 */
extern VCNL4020Driver moduleLldProximity;

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   A3906 (motor driver) test command.
 */
extern aos_shellcommand_t moduleTestA3906ShellCmd;

/**
 * @brief   AT24C01BN-SH-B (EEPROM) test command.
 */
extern aos_shellcommand_t moduleTestAt24c01bShellCmd;

/**
 * @brief   HMC5883L (compass) test command.
 */
extern aos_shellcommand_t moduleTestHmc5883lShellCmd;

/**
 * @brief   INA219 (power monitor) test command.
 */
extern aos_shellcommand_t moduleTestIna219ShellCmd;

/**
 * @brief   L3G4200D (gyroscope) test command.
 */
extern aos_shellcommand_t moduleTestL3g4200dShellCmd;

/**
 * @brief   Status LED test command.
 */
extern aos_shellcommand_t moduleTestLedShellCmd;

/**
 * @brief   LIS331DLH (accelerometer) test command.
 */
extern aos_shellcommand_t moduleTestLis331dlhShellCmd;

/**
 * @brief   LTC4412 (power path controller) test command.
 */
extern aos_shellcommand_t moduleTestLtc4412ShellCmd;

/**
 * @brief   PCA9544A (I2C multiplexer) test command.
 */
extern aos_shellcommand_t moduleTestPca9544aShellCmd;

/**
 * @brief   TPS62113 (step-down converter) test command.
 */
extern aos_shellcommand_t moduleTestTps62113ShellCmd;

/**
 * @brief   VCNL4020 (proximity sensor) test command.
 */
extern aos_shellcommand_t moduleTestVcnl4020ShellCmd;

/**
 * @brief   Entire module test command.
 */
extern aos_shellcommand_t moduleTestAllShellCmd;

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */

#endif /* AMIROOS_MODULE_H */

/** @} */
