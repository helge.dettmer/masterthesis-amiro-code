/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the LightRing module.
 *
 * @addtogroup lightring_module
 * @{
 */

#ifndef AMIROOS_MODULE_H
#define AMIROOS_MODULE_H

#include <amiroos.h>

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

/**
 * @brief   CAN driver to use.
 */
#define MODULE_HAL_CAN                          CAND1

/**
 * @brief   Configuration for the CAN driver.
 */
extern CANConfig moduleHalCanConfig;

/**
 * @brief   I2C driver to access the EEPROM, power monitor and the breakout header.
 */
#define MODULE_HAL_I2C_EEPROM_PWRMTR_BREAKOUT   I2CD2

/**
 * @brief   Configuration for the EEPROM, power monitor and breakout I2C driver.
 */
extern I2CConfig moduleHalI2cEepromPwrmtrBreakoutConfig;

/**
 * @brief   Serial driver of the programmer interface.
 */
#define MODULE_HAL_PROGIF                       SD1

/**
 * @brief   Configuration for the programmer serial interface driver.
 */
extern SerialConfig moduleHalProgIfConfig;

/**
 * @brief   SPI interface driver for the motion sensors (gyroscope and accelerometer).
 */
#define MODULE_HAL_SPI_LIGHT                    SPID1

/**
 * @brief   Configuration for the SPI interface driver to communicate with the LED driver.
 */
extern SPIConfig moduleHalSpiLightConfig;

/**
 * @brief   SPI interface driver for the breakout header.
 */
#define MODULE_HAL_SPI_BREAKOUT                 SPID2

/**
 * @brief   UART interface driver for the breakout header (alternative to serial).
 */
#define MODULE_HAL_UART_BREAKOUT                UARTD2

/**
 * @brief   Real-Time Clock driver.
 */
#define MODULE_HAL_RTC                          RTCD1

#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) || defined(__DOXYGEN__)

/**
 * @brief   SPI interface driver for UWB DW1000 module.
 */
#define MODULE_HAL_SPI_UWB                      MODULE_HAL_SPI_BREAKOUT

/**
 * @brief   Configuration for the high-speed SPI interface driver of DW1000 module.
 */
extern SPIConfig moduleHalSpiUwbHsConfig;

/**
 * @brief   Configuration for the low-speed SPI interface driver of DW1000 module.
 */
extern SPIConfig moduleHalSpiUwbLsConfig;

#endif /* (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) */

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LIGHT_BANK output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLightBlank;

/**
 * @brief   RS232_R_EN_N output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioRs232En;

/**
 * @brief   SW_V33_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSwV33En;

// The 4.2V switch is disabled due to a hardware bug.
///**
// * @brief   SW_V42_EN output signal GPIO.
// */
//extern ROMCONST apalControlGpio_t moduleGpioSwV42En;

/**
 * @brief   SW_V50_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSwV50En;

/**
 * @brief   IO_3 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo3;

/**
 * @brief   IO_5 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo5;

/**
 * @brief   IO_6 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo6;

/**
 * @brief   SYS_UART_DN bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysUartDn;

/**
 * @brief   IO_7 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo7;

/**
 * @brief   IO_8 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo8;

/**
 * @brief   IO_4 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo4;

/**
 * @brief   IO_1 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo1;

/**
 * @brief   IO_2 breakout signal GPIO.
 */
extern apalControlGpio_t moduleGpioBreakoutIo2;

/**
 * @brief   LED output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLed;

/**
 * @brief   LIGHT_XLAT output signal.
 */
extern ROMCONST apalControlGpio_t moduleGpioLightXlat;

/**
 * @brief   SW_V18_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSwV18En;

/**
 * @brief   SW_VSYS_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSwVsysEn;

/**
 * @brief   SYS_UART_UP bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysUartUp;

/**
 * @brief   SYS_PD bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysPd;

/**
 * @brief   SYS_SYNC bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysSync;

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Event flag to be set on a IO_4 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO4     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_4))

/**
 * @brief   Event flag to be set on a IO_1 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO1     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_1))

/**
 * @brief   Event flag to be set on a SYS_SYNC interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSSYNC         AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_INT_N))

/**
 * @brief   Event flag to be set on a IO_3 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO3     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_3))

/**
 * @brief   Event flag to be set on a IO_5 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO5     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_5))

/**
 * @brief   Event flag to be set on a IO_6 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO6     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_6))

/**
 * @brief   Event flag to be set on a SYS_UART_DN interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSUARTDN       AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_UART_DN))

/**
 * @brief   Event flag to be set on a SYS_UART_UP interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSUARTUP       AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_UART_UP))

/**
 * @brief   Event flag to be set on a IO_7 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO7     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_7))

/**
 * @brief   Event flag to be set on a IO_8 (breakout) interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_BREAKOUTIO8     AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IO_8))

/**
 * @brief   Event flag to be set on a SYS_PD interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSPD           AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_PD_N))

#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) || defined(__DOXYGEN__)

/**
 * @brief   Event flag to be set on a DW1000 interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_DW1000          MODULE_OS_GPIOEVENTFLAGS_BREAKOUTIO8

#endif /* (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) */

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
/**
 * @brief   Shell prompt text.
 */
extern ROMCONST char* moduleShellPrompt;
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/**
 * @brief   Interrupt initialization macro.
 */
#define MODULE_INIT_INTERRUPTS() {                                            \
  /* breakout interrupts must be enabled explicitely */                       \
  MODULE_INIT_INTERRUPTS_BREAKOUT();                                          \
}
#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10)
  #define MODULE_INIT_INTERRUPTS_BREAKOUT() {                                 \
    palSetLineCallback(moduleLldDw1000.gpio_exti->gpio->line, aosSysGetStdGpioCallback(), &moduleLldDw1000.gpio_exti->gpio->line);  \
    palEnableLineEvent(moduleLldDw1000.gpio_exti->gpio->line, APAL2CH_EDGE(moduleLldDw1000.gpio_exti->meta.edge));                  \
  }
#elif (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_NONE)
  #define MODULE_INIT_INTERRUPTS_BREAKOUT() {                                 \
  }
#endif

/**
 * @brief   Test initialization hook.
 */
#define MODULE_INIT_TESTS() {                                                 \
  /* add test commands to shell */                                            \
  aosShellAddCommand(&aos.shell, &moduleTestAt24c01bShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestIna219ShellCmd);                  \
  aosShellAddCommand(&aos.shell, &moduleTestLedShellCmd);                     \
  aosShellAddCommand(&aos.shell, &moduleTestMic9404xShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestTlc5947ShellCmd);                 \
  aosShellAddCommand(&aos.shell, &moduleTestAllShellCmd);                     \
  MODULE_INIT_TESTS_BREAKOUT();                                               \
}
#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10)
  #define MODULE_INIT_TESTS_BREAKOUT() {                                      \
    aosShellAddCommand(&aos.shell, &moduleTestDw1000ShellCmd);                \
  }
#elif (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_NONE)
  #define MODULE_INIT_TESTS_BREAKOUT() {                                      \
  }
#endif

/**
 * @brief   Periphery communication interfaces initialization hook.
 */
#define MODULE_INIT_PERIPHERY_IF() {                                          \
  /* serial driver */                                                         \
  sdStart(&MODULE_HAL_PROGIF, &moduleHalProgIfConfig);                        \
  /* I2C */                                                                   \
  moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed = (AT24C01B_LLD_I2C_MAXFREQUENCY < moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed) ? AT24C01B_LLD_I2C_MAXFREQUENCY : moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed; \
  moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed = (INA219_LLD_I2C_MAXFREQUENCY < moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed) ? INA219_LLD_I2C_MAXFREQUENCY : moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed; \
  moduleHalI2cEepromPwrmtrBreakoutConfig.duty_cycle = (moduleHalI2cEepromPwrmtrBreakoutConfig.clock_speed <= 100000) ? STD_DUTY_CYCLE : FAST_DUTY_CYCLE_2;  \
  i2cStart(&MODULE_HAL_I2C_EEPROM_PWRMTR_BREAKOUT, &moduleHalI2cEepromPwrmtrBreakoutConfig);  \
  /* SPI */                                                                   \
  spiStart(&MODULE_HAL_SPI_LIGHT, &moduleHalSpiLightConfig);                  \
  /* CAN */                                                                   \
  canStart(&MODULE_HAL_CAN, &moduleHalCanConfig);                             \
  /* breakout module */                                                       \
  MODULE_INIT_PERIPHERY_COMM_BREAKOUT();                                      \
}
#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10)
  #define MODULE_INIT_PERIPHERY_COMM_BREAKOUT() {                             \
    spiStart(&MODULE_HAL_SPI_UWB, &moduleHalSpiUwbLsConfig);                  \
  }
#elif (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_NONE)
  #define MODULE_INIT_PERIPHERY_COMM_BREAKOUT() {                             \
  }
#endif

/**
 * @brief   Periphery communication interface deinitialization hook.
 */
#define MODULE_SHUTDOWN_PERIPHERY_IF() {                                      \
  /* breakout module */                                                       \
  MODULE_SHUTDOWN_PERIPHERY_COMM_BREAKOUT();                                  \
  /* CAN */                                                                   \
  canStop(&MODULE_HAL_CAN);                                                   \
  /* SPI */                                                                   \
  spiStop(&MODULE_HAL_SPI_LIGHT);                                             \
  /* I2C */                                                                   \
  i2cStop(&MODULE_HAL_I2C_EEPROM_PWRMTR_BREAKOUT);                            \
  /* don't stop the serial driver so messages can still be printed */         \
}
#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10)
  #define MODULE_SHUTDOWN_PERIPHERY_COMM_BREAKOUT() {                         \
    /* SPI */                                                                 \
    spiStop(&MODULE_HAL_SPI_UWB);                                             \
  }
#elif (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_NONE)
  #define MODULE_SHUTDOWN_PERIPHERY_COMM_BREAKOUT() {                         \
  }
#endif

#define AMIROOS_CFG_SYSINFO_HOOK() {                                          \
  _printSystemInfoLine(stream, "Built for Breakout Module", SYSTEM_INFO_NAMEWIDTH, "%s",  \
                       (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_NONE) ? "none" :  \
                       (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) ? "UWB v1.0 (DW1000)" : \
                       "unknown");                                            \
}

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

#define moduleSsspSignalPD()                    (&moduleGpioSysPd)
#define moduleSsspEventflagPD()                 MODULE_OS_GPIOEVENTFLAG_SYSPD

#define moduleSsspSignalS()                     (&moduleGpioSysSync)
#define moduleSsspEventflagS()                  MODULE_OS_GPIOEVENTFLAG_SYSSYNC

#define moduleSsspSignalDN()                    (&moduleGpioSysUartDn)
#define moduleSsspEventflagDN()                 MODULE_OS_GPIOEVENTFLAG_SYSUARTDN

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/
#include <alld_AT24C01B.h>
#include <alld_INA219.h>
#include <alld_LED.h>
#include <alld_MIC9404x.h>
// TODO: add SNx5C3221E
#include <alld_TLC5947.h>

/**
 * @brief   EEPROM driver.
 */
extern AT24C01BDriver moduleLldEeprom;

/**
 * @brief   Power monitor (VLED 4.2) driver.
 */
extern INA219Driver moduleLldPowerMonitorVled;

/**
 * @brief   Status LED driver.
 */
extern LEDDriver moduleLldStatusLed;

/**
 * @brief   Power switch driver (1.8V).
 */
extern MIC9404xDriver moduleLldPowerSwitchV18;

/**
 * @brief   Power switch driver (3.3V).
 */
extern MIC9404xDriver moduleLldPowerSwitchV33;

/**
 * @brief   Power switch driver (4.2V).
 */
extern MIC9404xDriver moduleLldPowerSwitchV42;

/**
 * @brief   Power switch driver (5.0V).
 */
extern MIC9404xDriver moduleLldPowerSwitchV50;

/**
 * @brief   Pseudo power switch driver (VSYS).
 * @details There is no actual MIC9040x device, but the swicthable circuit behaves analogous.
 */
extern MIC9404xDriver moduleLldPowerSwitchVsys;

// TODO: add SNx5C3221E

/**
 * @brief   24 channel PWM LED driver.
 */
extern TLC5947Driver moduleLldLedPwm;

#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) || defined(__DOXYGEN__)

#include <alld_DW1000.h>

/**
 * @brief   Alias for the DW1000 driver object.
 * @note    The dw1000 struct is defined as external variable (singleton) by the
 *          driver, since the Decawave software stacks assumes no more than a
 *          single device in a system.
 */
#define moduleLldDw1000                         dw1000

#endif /* (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) */

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   AT24C01BN-SH-B (EEPROM) test command.
 */
extern aos_shellcommand_t moduleTestAt24c01bShellCmd;

/**
 * @brief   INA219 (power monitor) test command.
 */
extern aos_shellcommand_t moduleTestIna219ShellCmd;

/**
 * @brief   Status LED test command.
 */
extern aos_shellcommand_t moduleTestLedShellCmd;

/**
 * @brief   MIC9404x (power switch) test command.
 */
extern aos_shellcommand_t moduleTestMic9404xShellCmd;

// TODO: add SNx5C3221E

/**
 * @brief   TLC5947 (24 channel PWM LED driver) test command.
 */
extern aos_shellcommand_t moduleTestTlc5947ShellCmd;

/**
 * @brief   Entire module test command.
 */
extern aos_shellcommand_t moduleTestAllShellCmd;


#if (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) || defined(__DOXYGEN__)

/**
 * @brief   DW1000 (UWB transmitter) test command.
 */
extern aos_shellcommand_t moduleTestDw1000ShellCmd;

#endif /* (BOARD_BREAKOUT_MODULE == BOARD_BREAKOUT_UWBv10) */

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */

#endif /* AMIROOS_MODULE_H */

/** @} */
