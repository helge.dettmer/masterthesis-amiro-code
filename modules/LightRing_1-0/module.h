/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the LightRing module.
 *
 * @addtogroup lightring_module
 * @{
 */

#ifndef AMIROOS_MODULE_H
#define AMIROOS_MODULE_H

#include <amiroos.h>

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

/**
 * @brief   CAN driver to use.
 */
#define MODULE_HAL_CAN                          CAND1

/**
 * @brief   Configuration for the CAN driver.
 */
extern CANConfig moduleHalCanConfig;

/**
 * @brief   I2C driver to access the EEPROM.
 */
#define MODULE_HAL_I2C_EEPROM                   I2CD2

/**
 * @brief   Configuration for the EEPROM I2C driver.
 */
extern I2CConfig moduleHalI2cEepromConfig;

/**
 * @brief   Serial driver of the programmer interface.
 */
#define MODULE_HAL_PROGIF                       SD1

/**
 * @brief   Configuration for the programmer serial interface driver.
 */
extern SerialConfig moduleHalProgIfConfig;

/**
 * @brief   SPI interface driver for the motion sensors (gyroscope and accelerometer).
 */
#define MODULE_HAL_SPI_LIGHT                    SPID1

/**
 * @brief   SPI interface driver for the wireless transceiver.
 */
#define MODULE_HAL_SPI_WL                       SPID2

/**
 * @brief   Configuration for the SPI interface driver to communicate with the LED driver.
 */
extern SPIConfig moduleHalSpiLightConfig;

/**
 * @brief   Configuration for the SPI interface driver to communicate with the wireless transceiver.
 */
extern SPIConfig moduleHalSpiWlConfig;

/**
 * @brief   Real-Time Clock driver.
 */
#define MODULE_HAL_RTC                          RTCD1

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LIGHT_BANK output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLightBlank;

/**
 * @brief   LASER_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLaserEn;

/**
 * @brief   LASER_OC input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLaserOc;

/**
 * @brief   SYS_UART_DN bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysUartDn;

/**
 * @brief   WL_GDO2 input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioWlGdo2;

/**
 * @brief   WL_GDO0 input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioWlGdo0;

/**
 * @brief   LIGHT_XLAT output signal.
 */
extern ROMCONST apalControlGpio_t moduleGpioLightXlat;

/**
 * @brief   SYS_PD bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysPd;

/**
 * @brief   SYS_SYNC bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysSync;

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Event flag to be set on a LASER_OC interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_LASEROC         AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_LASER_OC_N))

/**
 * @brief   Event flag to be set on a SYS_UART_DN interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSUARTDN       AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_UART_DN))

/**
 * @brief   Event flag to be set on a WL_GDO2 interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_WLGDO2          AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_WL_GDO2))

/**
 * @brief   Event flag to be set on a WL_GDO0 interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_WLGDO0          AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_WL_GDO0))

/**
 * @brief   Event flag to be set on a SYS_PD interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSPD           AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_PD_N))

/**
 * @brief   Event flag to be set on a SYS_SYNC interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSSYNC         AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_INT_N))

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
/**
 * @brief   Shell prompt text.
 */
extern ROMCONST char* moduleShellPrompt;
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/**
 * @brief   Interrupt initialization macro.
 */
#define MODULE_INIT_INTERRUPTS() {                                            \
  /* LASER_OC */                                                              \
  palSetLineCallback(moduleGpioLaserOc.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioLaserOc.gpio->line);  \
  palEnableLineEvent(moduleGpioLaserOc.gpio->line, APAL2CH_EDGE(moduleGpioLaserOc.meta.edge));                  \
  /* WL_GDO2 */                                                               \
  palSetLineCallback(moduleGpioWlGdo2.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioWlGdo2.gpio->line);  \
  palEnableLineEvent(moduleGpioWlGdo2.gpio->line, APAL2CH_EDGE(moduleGpioWlGdo2.meta.edge));                  \
  /* WL_GDO0 */                                                               \
  palSetLineCallback(moduleGpioWlGdo0.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioWlGdo0.gpio->line);  \
  /*palEnableLineEvent(moduleGpioWlGdo0.gpio->line, APAL2CH_EDGE(moduleGpioWlGdo0.meta.edge)); // this is broken for some reason*/  \
}

/**
 * @brief   Test initialization hook.
 */
#define MODULE_INIT_TESTS() {                                                 \
  /* add test commands to shell */                                            \
  aosShellAddCommand(&aos.shell, &moduleTestAt24c01bShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestTlc5947ShellCmd);                 \
  aosShellAddCommand(&aos.shell, &moduleTestTps2051bdbvShellCmd);             \
  aosShellAddCommand(&aos.shell, &moduleTestAllShellCmd);                     \
}

/**
 * @brief   Periphery communication interfaces initialization hook.
 */
#define MODULE_INIT_PERIPHERY_IF() {                                          \
  /* serial driver */                                                         \
  sdStart(&MODULE_HAL_PROGIF, &moduleHalProgIfConfig);                        \
  /* I2C */                                                                   \
  moduleHalI2cEepromConfig.clock_speed = (AT24C01B_LLD_I2C_MAXFREQUENCY < moduleHalI2cEepromConfig.clock_speed) ? AT24C01B_LLD_I2C_MAXFREQUENCY : moduleHalI2cEepromConfig.clock_speed; \
  moduleHalI2cEepromConfig.duty_cycle = (moduleHalI2cEepromConfig.clock_speed <= 100000) ? STD_DUTY_CYCLE : FAST_DUTY_CYCLE_2;  \
  i2cStart(&MODULE_HAL_I2C_EEPROM, &moduleHalI2cEepromConfig);                \
  /* SPI */                                                                   \
  spiStart(&MODULE_HAL_SPI_LIGHT, &moduleHalSpiLightConfig);                  \
  spiStart(&MODULE_HAL_SPI_WL, &moduleHalSpiWlConfig);                        \
  /* CAN */                                                                   \
  canStart(&MODULE_HAL_CAN, &moduleHalCanConfig);                             \
}

/**
 * @brief   Periphery communication interface deinitialization hook.
 */
#define MODULE_SHUTDOWN_PERIPHERY_IF() {                                      \
  /* CAN */                                                                   \
  canStop(&MODULE_HAL_CAN);                                                   \
  /* SPI */                                                                   \
  spiStop(&MODULE_HAL_SPI_LIGHT);                                             \
  spiStop(&MODULE_HAL_SPI_WL);                                                \
  /* I2C */                                                                   \
  i2cStop(&MODULE_HAL_I2C_EEPROM);                                            \
  /* don't stop the serial driver so messages can still be printed */         \
}

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

#define moduleSsspSignalPD()                    (&moduleGpioSysPd)
#define moduleSsspEventflagPD()                 MODULE_OS_GPIOEVENTFLAG_SYSPD

#define moduleSsspSignalS()                     (&moduleGpioSysSync)
#define moduleSsspEventflagS()                  MODULE_OS_GPIOEVENTFLAG_SYSSYNC

#define moduleSsspSignalDN()                    (&moduleGpioSysUartDn)
#define moduleSsspEventflagDN()                 MODULE_OS_GPIOEVENTFLAG_SYSUARTDN

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/
#include <alld_AT24C01B.h>
#include <alld_TLC5947.h>
#include <alld_TPS20xxB.h>

/**
 * @brief   EEPROM driver.
 */
extern AT24C01BDriver moduleLldEeprom;

/**
 * @brief   24 channel PWM LED driver.
 */
extern TLC5947Driver moduleLldLedPwm;

/**
 * @brief   Power switch driver for the laser supply power.
 */
extern TPS20xxBDriver moduleLldPowerSwitchLaser;

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   AT24C01BN-SH-B (EEPROM) test command.
 */
extern aos_shellcommand_t moduleTestAt24c01bShellCmd;

/**
 * @brief   TLC5947 (24 channel PWM LED driver) test command
 */
extern aos_shellcommand_t moduleTestTlc5947ShellCmd;

/**
 * @brief   TPS2051BDBV (Current-limited power switch) test command
 */
extern aos_shellcommand_t moduleTestTps2051bdbvShellCmd;

/**
 * @brief   Entire module test command.
 */
extern aos_shellcommand_t moduleTestAllShellCmd;

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */

#endif /* AMIROOS_MODULE_H */

/** @} */
