/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the NUCLEO-L476RG module.
 *
 * @addtogroup NUCLEO-L476RG_module
 * @{
 */

#include <amiroos.h>

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

SerialConfig moduleHalProgIfConfig = {
  /* bit rate */ 115200,
  /* CR1      */ 0,
  /* CR1      */ 0,
  /* CR1      */ 0,
};

#if (BOARD_MPU6050_CONNECTED == true)

I2CConfig moduleHalI2c3Config = {
  /* timing reg */ 0, // configured later in MODULE_INIT_PERIPHERY_IF_MPU6050() hook
  /* CR1        */ 0,
  /* CR2        */ 0,
};

#endif /* (BOARD_MPU6050_CONNECTED == true) */

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LED output signal GPIO.
 */
static apalGpio_t _gpioLed = {
  /* line */ LINE_LED_GREEN,
};
ROMCONST apalControlGpio_t moduleGpioLed = {
  /* GPIO */ &_gpioLed,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_OUTPUT,
    /* active state   */ APAL_GPIO_ACTIVE_HIGH,
    /* interrupt edge */ APAL_GPIO_EDGE_NONE,
  },
};

/**
 * @brief   User button input signal GPIO.
 */
static apalGpio_t _gpioUserButton = {
  /* line */ LINE_BUTTON,
};
ROMCONST apalControlGpio_t moduleGpioUserButton = {
  /* GPIO */ &_gpioUserButton,
  /* meta */ {
    /* direction      */ APAL_GPIO_DIRECTION_INPUT,
    /* active state   */ APAL_GPIO_ACTIVE_LOW,
    /* interrupt edge */ APAL_GPIO_EDGE_BOTH,
  },
};

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
ROMCONST char* moduleShellPrompt = "NUCLEO-L476RG";
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/

LEDDriver moduleLldLed = {
  /* LED enable Gpio */ &moduleGpioLed,
};

ButtonDriver moduleLldUserButton = {
  /* Button Gpio  */ &moduleGpioUserButton,
};

#if (BOARD_MPU6050_CONNECTED == true)

MPU6050Driver moduleLldMpu6050 = {
  /* I2C Driver       */ &MODULE_HAL_I2C3,
  /* I²C address      */ MPU6050_LLD_I2C_ADDR_FIXED,
};

#endif /* (BOARD_MPU6050_CONNECTED == true) */

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/*
 * LED
 */
#include <module_test_LED.h>
static int _testLedShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestLedShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestLedShellCmd, "test:LED", _testLedShellCmdCb);

/*
 * User button
 */
#include <module_test_button.h>
static int _testButtonShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestButtonShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestButtonShellCmd, "test:button", _testButtonShellCmdCb);

#if (BOARD_MPU6050_CONNECTED == true) || defined(__DOXYGEN__)

/*
 * MPU6050 (accelerometer & gyroscope)
 */
#include <module_test_MPU6050.h>
static int _testMpu6050ShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return moduleTestMpu6050ShellCb(stream, argc, argv, NULL);
}
AOS_SHELL_COMMAND(moduleTestMpu6050ShellCmd, "test:IMU", _testMpu6050ShellCmdCb);

#endif /* (BOARD_MPU6050_CONNECTED == true) */

/*
 * entire module
 */
static int _testAllShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;

  int status = AOS_OK;
  char* targv[AMIROOS_CFG_SHELL_MAXARGS] = {NULL};
  aos_testresult_t result_test = {0, 0};
  aos_testresult_t result_total = {0, 0};

  /* LED */
  status |= moduleTestLedShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* User button */
  status |= moduleTestButtonShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);

  /* MPU6050 (accelerometer & gyroscope) */
#if (BOARD_MPU6050_CONNECTED == true)
  status |= moduleTestMpu6050ShellCb(stream, 0, targv, &result_test);
  result_total = aosTestResultAdd(result_total, result_test);
#endif /* (BOARD_MPU6050_CONNECTED == true) */

  // print total result
  chprintf(stream, "\n");
  aosTestResultPrintSummary(stream, &result_total, "entire module");

  return status;
}
AOS_SHELL_COMMAND(moduleTestAllShellCmd, "test:all", _testAllShellCmdCb);

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */
/** @} */
