/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the NUCLEO-L476RG module.
 *
 * @addtogroup NUCLEO-L476RG_module
 * @{
 */

#ifndef AMIROOS_MODULE_H
#define AMIROOS_MODULE_H

#include <amiroos.h>

#if (BOARD_MPU6050_CONNECTED == true)
#include <math.h>
#endif /* BOARD_MPU6050_CONNECTED == true */

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Serial driver of the programmer interface.
 */
#define MODULE_HAL_PROGIF                       SD2

/**
 * @brief   Configuration for the programmer serial interface driver.
 */
extern SerialConfig moduleHalProgIfConfig;

/**
 * @brief   Real-Time Clock driver.
 */
#define MODULE_HAL_RTC                          RTCD1

#if (BOARD_MPU6050_CONNECTED == true) || defined(__DOXYGEN__)

/**
 * @brief   I2C driver to access multiplexer, proximity sensors 5 to 8, power monitors for VSYS4.2, VIO 5.0 and VDD, EEPROM, touch sensor, and fuel gauge (front battery).
 */
#define MODULE_HAL_I2C3                         I2CD3

/**
 * @brief   Configuration for the I2C driver #3.
 */
extern I2CConfig moduleHalI2c3Config;

#endif /* (BOARD_MPU6050_CONNECTED == true) */

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LED output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLed;

/**
 * @brief   User button input signal.
 */
extern ROMCONST apalControlGpio_t moduleGpioUserButton;

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Event flag to be set on a USER_BUTTON interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_USERBUTTON      AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_BUTTON))

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
/**
 * @brief   Shell prompt text.
 */
extern ROMCONST char* moduleShellPrompt;
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/**
 * @brief   Interrupt initialization macro.
 */
#define MODULE_INIT_INTERRUPTS() {                                            \
  /* user button */                                                           \
  palSetLineCallback(moduleGpioUserButton.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioUserButton.gpio->line);  \
  palEnableLineEvent(moduleGpioUserButton.gpio->line, APAL2CH_EDGE(moduleGpioUserButton.meta.edge));                  \
}

/**
 * @brief   Test initialization hook.
 */
#define MODULE_INIT_TESTS() {                                                 \
  /* add test commands to shell */                                            \
  aosShellAddCommand(&aos.shell, &moduleTestLedShellCmd);                     \
  aosShellAddCommand(&aos.shell, &moduleTestButtonShellCmd);                  \
  MODULE_INIT_TEST_MPU6050();                                                 \
  aosShellAddCommand(&aos.shell, &moduleTestAllShellCmd);                     \
}
#if (BOARD_MPU6050_CONNECTED == true)
  #define MODULE_INIT_TEST_MPU6050() {                                        \
    aosShellAddCommand(&aos.shell, &moduleTestMpu6050ShellCmd);               \
  }
#else /* (BOARD_MPU6050_CONNECTED == true) */
  #define MODULE_INIT_TEST_MPU6050() {}
#endif /* (BOARD_MPU6050_CONNECTED == true) */

/**
 * @brief   Periphery communication interfaces initialization hook.
 */
#define MODULE_INIT_PERIPHERY_IF() {                                          \
  /* serial driver */                                                         \
  sdStart(&MODULE_HAL_PROGIF, &moduleHalProgIfConfig);                        \
  /* MPU6050 demo */                                                          \
  MODULE_INIT_PERIPHERY_IF_MPU6050();                                         \
}
#if (BOARD_MPU6050_CONNECTED == true)
  #define MODULE_INIT_PERIPHERY_IF_MPU6050() {                                \
    /* maximum I2C frequency is 1MHz for this MCU */                          \
    uint32_t i2c3_freq = 1000000;                                             \
    /* find minimum amon all devices connected to this bus */                 \
    i2c3_freq = (MPU6050_LLD_I2C_MAXFREQUENCY < i2c3_freq) ? MPU6050_LLD_I2C_MAXFREQUENCY : i2c3_freq;  \
    /* calculate PRESC (prescaler):                                           \
     *   target is 1/(I2CXCLK * (PRESC + 1)) = 125ns                          \
     */                                                                       \
    moduleHalI2c3Config.timingr = ((uint8_t)((0.000000125f * STM32_I2C3CLK) - 1)) << I2C_TIMINGR_PRESC_Pos; \
    /* SCL shall be low half of the time. */                                  \
    moduleHalI2c3Config.timingr |= ((uint8_t)((1.f / i2c3_freq / 2 / 0.000000125f) - 1)) << I2C_TIMINGR_SCLL_Pos; \
    /* SCL shall be high half the time of low or slightly longer. */          \
    moduleHalI2c3Config.timingr |= (uint8_t)(ceilf(((moduleHalI2c3Config.timingr & I2C_TIMINGR_SCLL_Msk) >> I2C_TIMINGR_SCLL_Pos) / 2.f)) << I2C_TIMINGR_SCLH_Pos;  \
    /* SDA shall be delayed 1/10 of SCL low, or shorter */                    \
    moduleHalI2c3Config.timingr |= (uint8_t)(((moduleHalI2c3Config.timingr & I2C_TIMINGR_SCLL_Msk) >> I2C_TIMINGR_SCLL_Pos) * 0.1f) << I2C_TIMINGR_SDADEL_Pos;  \
    /* SCL shall be delyed twice as long as SDA, but longer than 0. */        \
    moduleHalI2c3Config.timingr |= ((((moduleHalI2c3Config.timingr & I2C_TIMINGR_SDADEL_Msk) >> I2C_TIMINGR_SDADEL_Pos) * 2) + 1) << I2C_TIMINGR_SCLDEL_Pos;  \
    /* now we can start the I2C driver */                                     \
    chSysLock();                                                              \
    palSetLineMode(LINE_ARD_A4, PAL_MODE_ALTERNATE(4));                       \
    palSetLineMode(LINE_ARD_A5, PAL_MODE_ALTERNATE(4));                       \
    chSysUnlock();                                                            \
    i2cStart(&MODULE_HAL_I2C3, &moduleHalI2c3Config);                         \
  }
#else /* (BOARD_MPU6050_CONNECTED == true) */
  #define MODULE_INIT_PERIPHERY_IF_MPU6050() {}
#endif /* (BOARD_MPU6050_CONNECTED == true) */

/**
 * @brief   Periphery communication interface deinitialization hook.
 */
#define MODULE_SHUTDOWN_PERIPHERY_IF() {                                      \
  /* don't stop the serial driver so messages can still be printed */         \
}

/**
 * @brief   HOOK to toggle the LEDs when the user button is pressed.
 */
#define MODULE_MAIN_LOOP_GPIOEVENT(eventflags) {                              \
  if (eventflags & MODULE_OS_GPIOEVENTFLAG_USERBUTTON) {                      \
    button_lld_state_t buttonstate;                                           \
    button_lld_get(&moduleLldUserButton, &buttonstate);                       \
    led_lld_set(&moduleLldLed, (buttonstate == BUTTON_LLD_STATE_PRESSED) ? LED_LLD_STATE_ON : LED_LLD_STATE_OFF); \
  }                                                                           \
}

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

#if (AMIROOS_CFG_SSSP_ENABLE == true) || defined(__DOXYGEN__)
  #error "SSSP is not supported on this module."
#endif /* (AMIROOS_CFG_SSSP_ENABLE == true) */

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/
#include <alld_LED.h>
#include <alld_button.h>

/**
 * @brief   LED driver.
 */
extern LEDDriver moduleLldLed;

/**
 * @brief   Button driver.
 */
extern ButtonDriver moduleLldUserButton;

#if (BOARD_MPU6050_CONNECTED == true) || defined(__DOXYGEN__)

#include <alld_MPU6050.h>

/**
 * @brief   Accelerometer (MPU6050) driver.
 */
extern MPU6050Driver moduleLldMpu6050;

#endif /* (BOARD_MPU6050_CONNECTED == true) */

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   LED test command.
 */
extern aos_shellcommand_t moduleTestLedShellCmd;

/**
 * @brief   User button test command.
 */
extern aos_shellcommand_t moduleTestButtonShellCmd;

#if (BOARD_MPU6050_CONNECTED == true) || defined(__DOXYGEN__)

/**
 * @brief   MPU6050 (Accelerometer & Gyroscope) test command.
 */
extern aos_shellcommand_t moduleTestMpu6050ShellCmd;

#endif /* (BOARD_MPU6050_CONNECTED == true) */

/**
 * @brief   Entire module test command.
 */
extern aos_shellcommand_t moduleTestAllShellCmd;

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */

#endif /* AMIROOS_MODULE_H */

/** @} */
