/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * @brief   Structures and constant for the DiWheelDrive module.
 *
 * @addtogroup diwheeldrive_module
 * @{
 */

#ifndef AMIROOS_MODULE_H
#define AMIROOS_MODULE_H

#include <amiroos.h>

/*===========================================================================*/
/**
 * @name Module specific functions
 * @{
 */
/*===========================================================================*/

/** @} */

/*===========================================================================*/
/**
 * @name ChibiOS/HAL configuration
 * @{
 */
/*===========================================================================*/

/**
 * @brief   CAN driver to use.
 */
#define MODULE_HAL_CAN                          CAND1

/**
 * @brief   Configuration for the CAN driver.
 */
extern CANConfig moduleHalCanConfig;

/**
 * @brief   I2C driver to access the compass.
 */
#define MODULE_HAL_I2C_IMU                      I2CD1

/**
 * @brief   Configuration for the compass I2C driver.
 */
extern I2CConfig moduleHalI2cImuConfig;

/**
 * @brief   I2C driver to access multiplexer, proximity sensors, EEPROM and power monitor.
 */
#define MODULE_HAL_I2C_PROX_EEPROM_PWRMTR       I2CD2

/**
 * @brief   Configuration for the multiplexer, proximity, EEPROM and power monitor I2C driver.
 */
extern I2CConfig moduleHalI2cProxEepromPwrmtrConfig;

/**
 * @brief   PWM driver to use.
 */
#define MODULE_HAL_PWM_DRIVE                    PWMD2

/**
 * @brief   Configuration for the PWM driver.
 */
extern PWMConfig moduleHalPwmDriveConfig;

/**
 * @brief   Drive PWM channel for the left wheel forward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD     ((apalPWMchannel_t)0)

/**
 * @brief   Drive PWM channel for the left wheel backward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD    ((apalPWMchannel_t)1)

/**
 * @brief   Drive PWM channel for the right wheel forward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD    ((apalPWMchannel_t)2)

/**
 * @brief   Drive PWM channel for the right wheel backward direction.
 */
#define MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD   ((apalPWMchannel_t)3)

/**
 * @brief   Quadrature encooder for the left wheel.
 */
#define MODULE_HAL_QEI_LEFT_WHEEL               QEID3

/**
 * @brief   Quadrature encooder for the right wheel.
 */
#define MODULE_HAL_QEI_RIGHT_WHEEL              QEID4

/**
 * @brief   Configuration for both quadrature encoders.
 */
extern QEIConfig moduleHalQeiConfig;

/**
 * @brief   QEI increments per wheel revolution.
 * @details 2 signal edges per pulse * 2 signals * 16 pulses per motor revolution * 22:1 gearbox
 */
#define MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION  (apalQEICount_t)(2 * 2 * 16 * 22)

/**
 * @brief   Serial driver of the programmer interface.
 */
#define MODULE_HAL_PROGIF                       SD1

/**
 * @brief   Configuration for the programmer serial interface driver.
 */
extern SerialConfig moduleHalProgIfConfig;

/**
 * @brief   Real-Time Clock driver.
 */
#define MODULE_HAL_RTC                          RTCD1

/** @} */

/*===========================================================================*/
/**
 * @name GPIO definitions
 * @{
 */
/*===========================================================================*/

/**
 * @brief   LED output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioLed;

/**
 * @brief   POWER_EN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioPowerEn;

/**
 * @brief   IR_INT input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioIrInt;

/**
 * @brief   SYS_UART_UP bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysUartUp;

/**
 * @brief   IMU_INT input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioImuInt;

/**
 * @brief   SYS_SNYC bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysSync;

/**
 * @brief   IMU_RESET output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioImuReset;

/**
 * @brief   PATH_DCSTAT input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioPathDcStat;

/**
 * @brief   PATH_DCEN output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioPathDcEn;

/**
 * @brief   SYS_PD bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysPd;

/**
 * @brief   SYS_REG_EN input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysRegEn;

/**
 * @brief   IMU_BOOT_LOAD output signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioImuBootLoad;

/**
 * @brief   IMU_BL_IND input signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioImuBlInd;

/**
 * @brief   SYS_WARMRST bidirectional signal GPIO.
 */
extern ROMCONST apalControlGpio_t moduleGpioSysWarmrst;

/** @} */

/*===========================================================================*/
/**
 * @name AMiRo-OS core configurations
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Event flag to be set on a SYS_SYNC interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSSYNC         AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_INT_N))

/**
 * @brief   Event flag to be set on a SYS_WARMRST interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSWARMRST      AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_WARMRST_N))

/**
 * @brief   Event flag to be set on a PATH_DCSTAT interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_PATHDCSTAT      AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_PATH_DCEN))

/**
 * @brief   Event flag to be set on a SYS_PD interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSPD           AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_PD_N))

/**
 * @brief   Event flag to be set on a SYS_REG_EN interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSREGEN        AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_REG_EN))

/**
 * @brief   Event flag to be set on a IR_INT interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_IRINT           AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IR_INT))

/**
 * @brief   Event flag to be set on a SYS_UART_UP interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_SYSUARTUP       AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_SYS_UART_UP))

/**
 * @brief   Event flag to be set on a IMU_INT interrupt.
 */
#define MODULE_OS_GPIOEVENTFLAG_IMUINT          AOS_GPIOEVENT_FLAG(PAL_PAD(LINE_IMU_INT))

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
/**
 * @brief   Shell prompt text.
 */
extern ROMCONST char* moduleShellPrompt;
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) */

/**
 * @brief   Interrupt initialization macro.
 */
#define MODULE_INIT_INTERRUPTS() {                                            \
  /* IR_INT */                                                                \
  palSetLineCallback(moduleGpioIrInt.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioIrInt.gpio->line);  \
  palEnableLineEvent(moduleGpioIrInt.gpio->line, APAL2CH_EDGE(moduleGpioIrInt.meta.edge));                  \
  /* IMU_INT */                                                               \
  palSetLineCallback(moduleGpioImuInt.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioImuInt.gpio->line);  \
  palEnableLineEvent(moduleGpioImuInt.gpio->line, APAL2CH_EDGE(moduleGpioImuInt.meta.edge));                  \
  /* PATH_DCSTAT */                                                           \
  palSetLineCallback(moduleGpioPathDcStat.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioPathDcStat.gpio->line);  \
  palEnableLineEvent(moduleGpioPathDcStat.gpio->line, APAL2CH_EDGE(moduleGpioPathDcStat.meta.edge));                  \
  /* SYS_REG_EN */                                                            \
  palSetLineCallback(moduleGpioSysRegEn.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioSysRegEn.gpio->line);  \
  palEnableLineEvent(moduleGpioSysRegEn.gpio->line, APAL2CH_EDGE(moduleGpioSysRegEn.meta.edge));                  \
  /* SYS_WARMRST */                                                           \
  palSetLineCallback(moduleGpioSysWarmrst.gpio->line, aosSysGetStdGpioCallback(), &moduleGpioSysWarmrst.gpio->line);  \
  palEnableLineEvent(moduleGpioSysWarmrst.gpio->line, APAL2CH_EDGE(moduleGpioSysWarmrst.meta.edge));                  \
}

/**
 * @brief   Test initialization hook.
 */
#define MODULE_INIT_TESTS() {                                                 \
  /* add test commands to shell */                                            \
  aosShellAddCommand(&aos.shell, &moduleTestA3906ShellCmd);                   \
  aosShellAddCommand(&aos.shell, &moduleTestAt24c01bShellCmd);                \
  /* TODO: add BNO055 test command */                                         \
  aosShellAddCommand(&aos.shell, &moduleTestIna219ShellCmd);                  \
  aosShellAddCommand(&aos.shell, &moduleTestLedShellCmd);                     \
  aosShellAddCommand(&aos.shell, &moduleTestLtc4412ShellCmd);                 \
  aosShellAddCommand(&aos.shell, &moduleTestPca9544aShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestTps62113ShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestVcnl4020ShellCmd);                \
  aosShellAddCommand(&aos.shell, &moduleTestAllShellCmd);                     \
}

/**
 * @brief   Periphery communication interfaces initialization hook.
 */
#define MODULE_INIT_PERIPHERY_IF() {                                          \
  /* serial driver */                                                         \
  sdStart(&MODULE_HAL_PROGIF, &moduleHalProgIfConfig);                        \
  /* I2C */                                                                   \
  /* TODO: calculcate config depending on BNO055 */                           \
  i2cStart(&MODULE_HAL_I2C_IMU, &moduleHalI2cImuConfig);                      \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (PCA9544A_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? PCA9544A_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (VCNL4020_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? VCNL4020_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (AT24C01B_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? AT24C01B_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.clock_speed = (INA219_LLD_I2C_MAXFREQUENCY < moduleHalI2cProxEepromPwrmtrConfig.clock_speed) ? INA219_LLD_I2C_MAXFREQUENCY : moduleHalI2cProxEepromPwrmtrConfig.clock_speed; \
  moduleHalI2cProxEepromPwrmtrConfig.duty_cycle = (moduleHalI2cProxEepromPwrmtrConfig.clock_speed <= 100000) ? STD_DUTY_CYCLE : FAST_DUTY_CYCLE_2;  \
  i2cStart(&MODULE_HAL_I2C_PROX_EEPROM_PWRMTR, &moduleHalI2cProxEepromPwrmtrConfig);  \
  /* PWM */                                                                   \
  pwmStart(&MODULE_HAL_PWM_DRIVE, &moduleHalPwmDriveConfig);                  \
  /* QEI */                                                                   \
  qeiStart(&MODULE_HAL_QEI_LEFT_WHEEL, &moduleHalQeiConfig);                  \
  qeiStart(&MODULE_HAL_QEI_RIGHT_WHEEL, &moduleHalQeiConfig);                 \
  qeiEnable(&MODULE_HAL_QEI_LEFT_WHEEL);                                      \
  qeiEnable(&MODULE_HAL_QEI_RIGHT_WHEEL);                                     \
  /* CAN */                                                                   \
  canStart(&MODULE_HAL_CAN, &moduleHalCanConfig);                             \
}

/**
 * @brief   Periphery communication interface deinitialization hook.
 */
#define MODULE_SHUTDOWN_PERIPHERY_IF() {                                      \
  /* CAN */                                                                   \
  canStop(&MODULE_HAL_CAN);                                                   \
  /* PWM */                                                                   \
  pwmStop(&MODULE_HAL_PWM_DRIVE);                                             \
  /* QEI */                                                                   \
  qeiDisable(&MODULE_HAL_QEI_LEFT_WHEEL);                                     \
  qeiDisable(&MODULE_HAL_QEI_RIGHT_WHEEL);                                    \
  qeiStop(&MODULE_HAL_QEI_LEFT_WHEEL);                                        \
  qeiStop(&MODULE_HAL_QEI_RIGHT_WHEEL);                                       \
  /* I2C */                                                                   \
  i2cStop(&MODULE_HAL_I2C_IMU);                                               \
  i2cStop(&MODULE_HAL_I2C_PROX_EEPROM_PWRMTR);                                \
  /* don't stop the serial driver so messages can still be printed */         \
}

/** @} */

/*===========================================================================*/
/**
 * @name Startup Shutdown Synchronization Protocol (SSSP)
 * @{
 */
/*===========================================================================*/

#define moduleSsspSignalPD()                    (&moduleGpioSysPd)
#define moduleSsspEventflagPD()                 MODULE_OS_GPIOEVENTFLAG_SYSPD

#define moduleSsspSignalS()                     (&moduleGpioSysSync)
#define moduleSsspEventflagS()                  MODULE_OS_GPIOEVENTFLAG_SYSSYNC

#define moduleSsspSignalUP()                    (&moduleGpioSysUartUp)
#define moduleSsspEventflagUP()                 MODULE_OS_GPIOEVENTFLAG_SYSUARTUP

/** @} */

/*===========================================================================*/
/**
 * @name Low-level drivers
 * @{
 */
/*===========================================================================*/
#include <alld_A3906.h>
#include <alld_AT24C01B.h>
// TODO: add BNO055 IMU
#include <alld_INA219.h>
#include <alld_LED.h>
#include <alld_LTC4412.h>
#include <alld_PCA9544A.h>
#include <alld_TPS6211x.h>
#include <alld_VCNL4020.h>

/**
 * @brief   Motor driver.
 */
extern A3906Driver moduleLldMotors;

/**
 * @brief   EEPROM driver.
 */
extern AT24C01BDriver moduleLldEeprom;

// TODO: add BNO055 IMU

/**
 * @brief   Power monitor (VDD) driver.
 */
extern INA219Driver moduleLldPowerMonitorVdd;

/**
 * @brief   Status LED driver.
 */
extern LEDDriver moduleLldStatusLed;

/**
 * @brief   Power path controler (charging pins) driver.
 */
extern LTC4412Driver moduleLldPowerPathController;

/**
 * @brief   I2C multiplexer driver.
 */
extern PCA9544ADriver moduleLldI2cMultiplexer;

/**
 * @brief   Step down converter (VDRIVE) driver.
 */
extern TPS6211xDriver moduleLldStepDownConverterVdrive;

/**
 * @brief   Proximity sensor driver.
 */
extern VCNL4020Driver moduleLldProximity;

/** @} */

/*===========================================================================*/
/**
 * @name Tests
 * @{
 */
/*===========================================================================*/
#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   A3906 (motor driver) test command.
 */
extern aos_shellcommand_t moduleTestA3906ShellCmd;

/**
 * @brief   AT24C01BN-SH-B (EEPROM) test command.
 */
extern aos_shellcommand_t moduleTestAt24c01bShellCmd;

// TODO: add BNO055

/**
 * @brief   INA219 (power monitor) test command.
 */
extern aos_shellcommand_t moduleTestIna219ShellCmd;

/**
 * @brief   Status LED test command.
 */
extern aos_shellcommand_t moduleTestLedShellCmd;

/**
 * @brief   LTC4412 (power path controller) test command.
 */
extern aos_shellcommand_t moduleTestLtc4412ShellCmd;

/**
 * @brief   PCA9544A (I2C multiplexer) test command.
 */
extern aos_shellcommand_t moduleTestPca9544aShellCmd;

/**
 * @brief   TPS62113 (step-down converter) test command.
 */
extern aos_shellcommand_t moduleTestTps62113ShellCmd;

/**
 * @brief   VCNL4020 (proximity sensor) test command.
 */
extern aos_shellcommand_t moduleTestVcnl4020ShellCmd;

/**
 * @brief   Entire module test command.
 */
extern aos_shellcommand_t moduleTestAllShellCmd;

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */

/** @} */

#endif /* AMIROOS_MODULE_H */

/** @} */
