About & License
===============

AMiRo-OS is an operating system for the base version of the Autonomous Mini
Robot (AMiRo) [1]. It utilizes ChibiOS (a real-time operating system for
embedded devices developed by Giovanni di Sirio; see <http://chibios.org>) as
system kernel and extends it with platform specific configurations and further
functionalities and abstractions.

Copyright (C) 2016..2020  Thomas Schöpping et al.
(a complete list of all authors is given below)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

This research/work was supported by the Cluster of Excellence
Cognitive Interaction Technology 'CITEC' (EXC 277) at Bielefeld
University, which is funded by the German Research Foundation (DFG).

Authors:

-   Thomas Schöpping (tschoepp@cit-ec.uni-bielefeld.de)
-   Marc Rothmann

References:

[1] S. Herbrechtsmeier, T. Korthals, T. Schopping and U. Rückert, "AMiRo: A
    modular & customizable open-source mini robot platform," 2016 20th
    International Conference on System Theory, Control and Computing (ICSTCC),
    Sinaia, 2016, pp. 687-692.

--------------------------------------------------------------------------------

Contents
========

1.  Required Software
    1.  Git
    2.  Bootloader & Tools (AMiRi-BLT)
    3.  System Kernel (ChibiOS)
    4.  Low-Level Drivers (AMiRo-LLD)
    5.  OpenOCD
2.  Recommended Software
    1.  gtkterm and hterm
    2.  Plantuml
    3.  Doxygen & Graphviz
    4.  QtCreator IDE
3.  Building and Flashing
4.  Developer Guides
    1.  Adding a Module
    2.  Adding a Shell Command
    3.  Handling a Custom I/O Event in the Main Thread
    4.  Implementing a Low-Level Driver
    5.  Writing a Test

--------------------------------------------------------------------------------

1 Required Software
===================

In order to compile the source code, you need to install the GNU ARM Embedded
Toolchain. Since this project uses GNU Make for configuring and calling the
compiler, this tool is requried too. AMiRo-OS uses ChibiOS as system kernel,
so you need a copy of that project as well.


1.1 Git
-------

Since all main- and subprojects are available as Git repositories, installing a
recent version of the tool is mandatory. Most Linux distributions like Ubuntu
provide a sufficient version in their software repositories.


1.2 Bootloader & Tools (AMiRo-BLT)
----------------------------------

AMiRo-OS can take advantage of an installed bootloader and provides an
interface. By default, AMiRo-BLT is included as Git submodule and can easily be
initialized via the provided `./setup.sh` script. Simply run

    >$ ./setup.sh

from a command line.

If requried, is is possible to replace the used bootloader by adding an
according subfolder in the `./bootloader/` directory. Note that you will have to
adapt the makefiles and scripts, and probably the operating system as well.

AMiRo-BLT furthermore has its own required and recommended software & tools as
described in its `README.md` file. Follow the instructions to initialize the
development environment manually or use the setup script.


1.3 System Kernel (ChibiOS)
---------------------------

Since AMiRo-OS uses ChibiOS as underlying system kernel, you need to acquire a
copy of it as well. For the sake of compatibility, it is included in AMiRo-OS as
Git submodule. It is highly recommended to use the setup script for
initialization. Moreover, you have to apply the patches to ChibiOS in order to
make AMiRo-OS work properly. It is recommended to use the setup script for this
purpose as well.

If you would like to use a different kernel, you can add a subfolder in the
`./kernel/` directory and adapt the scripts and operating system source code.


1.4 Low-Level Drivers
---------------------

Any required low-level drivers for the AMiRo hardware are available in an
additional project: AMiRo-LLD. It is included as Git subodule and can be
initialized via the setup script. Since AMiRo-LLD is also used for
experimentation and prototyping, it contains drivers even for some hardware that
is not available on the AMiRo platform.


1.5 OpenOCD
-----------

When running AMiRo-OS on non-AMiRo modules (e.g. NUCLEO development boards),
those can be flashed using the OpenOCD toolchain (<http://openocd.org/>). It can
be either installed from the software repositories of your operating system
(reqiures root permissions) or built from source (no root required).  
For a list of supported boards, please refer to the OpcenOCD documentation.


2 Recommended Software
======================

The software tools named in this section are not essential for simply using or
further development of AMiRo-BLT, but can help for both scenarios.


2.1 gtkterm and hterm
---------------------

Depending on your operating system, it is recommended to install `gtkterm` for
Linux (available in the Ubuntu repositories), or `hterm` for Windows. For
`gtkterm` you need to modify the configuration file `~/.gtktermrc` (generated
automatically when you start the application for the first time). For the AMiRo
modules the configuration is:

    [AMiRo]
    port	= /dev/ttyAMiRo0
    speed	= 115200
    bits	= 8
    stopbits	= 1
    parity	= none
    flow	= none
    wait_delay	= 0
    wait_char	= -1
    rs485_rts_time_before_tx	= 30
    rs485_rts_time_after_tx	= 30
    echo	= False
    crlfauto	= True

The according configuration for all NUCLEO boards is:

    [NUCLEO]
    port	= /dev/ttyACM0
    speed	= 115200
    bits	= 8
    stopbits	= 1
    parity	= none
    flow	= none
    wait_delay	= 0
    wait_char	= -1
    rs485_rts_time_before_tx	= 30
    rs485_rts_time_after_tx	= 30
    echo	= False
    crlfauto	= True

When running `gtkterm` from the command line, you can select a defined
configuration via the `-c` option:

    >$ gtkterm -c AMiRo
    >$ gtkterm -c NUCLEO

For `hterm` you need to configure the tool analogously. With either tool the
robot can be reset by toggling the RTS signal on and off again, and you can
access the system shell of AMiRo-OS.  
If you are using an old version of AMiRo-BLT, the `/dev/ttyAMiRo` devices might
not be available. In order to enable legacy support, replace the port value by
`/dev/ttyUSB0`.

Advanced users can use several connections to multiple modules simultaneously.
Each additional programmer will be available as `/dev/ttyAMiRo<N>` (and
`/dev/ttyUSB<N>` respectively) with `<N>` being an integer starting from 0.
Please note: Those interfaces are ordered by the time when they have been
detected by the operating system, so detaching a cable and plugging it in again
may result in a different port name.


2.2 PlantUML
------------

PlantUML is a free and open source Java tool to generate UML diagrams via scrips
(see <https://plantuml.com>). AMiRo-OS provides according scripts in the
`./doc/` directory. Please refer to the PlantUML documentation for how to
generate figures from these script files.


2.3 Doxygen & Graphviz
----------------------

In order to generate the documentation from the source code, Doxygen and
Graphviz are requried. It is recommended to install these tool using the
default versions for your system. Ubuntu users should simply run

    >$ sudo apt-get install doxygen graphviz


2.4 QtCreator IDE
-----------------

AMiRo-OS provides support for the QtCreator IDE. In order to setup according
projects, use the setup script and follow the instructions. It will
automatically generate the required files and you can import the projects by
opening the `.creator` files with QtCreator IDE.  
Please note that you will need to recompile the AMiRo-OS source code after each
project generation, since the generator runs a compiler call.

Further instructions for a more advanced configuration of the IDE are provided
in the `./tools/qtcreator/README.txt` file.



3 Building and Flashing
=======================

Each time you modify any part of AMiRo-OS, you need to recompile the whole
project for the according AMiRo module. Therefore you can use the `./Makefile`
by simply executing `make` and follow the instructions:

    >$ cd /path/to/AMiRo-OS/root/
    >$ make

Alternatively, you can either use the makefiles provided per module in
`./modules/<module_to_compile>/` or the makefile in the `./modules/` folder.
After the build process has finished successfully, you always have to flash the
generated program to the module. Therefore you need an appropriate tool, such as
`SerialBoot` for the AMiRo base modules (provided by AMiRo-BLT) or OpenOCD.
Similar to the compilation procedure as described above, you can flash either
each module individually, or all modules at once by using the same makefiles.

When using `SerialBoot`, please note that you must connect the programming cable
either to the _DiWheelDrive_ or the _PowerManagement_ module for flashing the
operating system. All other modules are powered off after reset so that only
these two offer a running bootloader, which is required for flashing.



4 Developer Guides
==================

Due to the complexity of AMiRo-OS it can be quite troublesome to get started
with the framework at the beginning. The guides in this chapter will help you
getting things done, without thorough knowledge of the software structure.
Whereas the textual descriptions of the guides provide in-depth information
about the underlying concepts and mechanisms, a short summary is provided at the
end of each chapter.


4.1 Adding a Module
-------------------

The very first thing to do when adding a new module to support AMiRo-OS, is to
create an according folder in the `./modules/` directory. The name of this
folder should be as unambiguous as possible (e.g. containing name and version
number). All files, which directly depent on the hardware, and thus are not
portable, belong here. Conversely, any code that can be reused on diferent
hardware should not be placed in this module folder.

In a second step you have to initialize all requried files (see below) in the
newly created module directory. It is recommended to use another module as
template for your configuration:

*   alldconf.h  
    Configuration header for the AMiRo-LLD project, which is part of AMiRo-OS.
    There are probably only very few configurations done here, since most
    setting depend on the content of aosconf.h and are handled module
    unspecifically in the `./modules/aos_alldconf.h` file.
*   aosconf.h  
    Configuration header for the AMiRo-OS project. Existing cofiguration files
    are well documented and name all available settings.
*   board.h & board.c  
    Contains definitions of GPIO names and initialization setting of those, as
    well as initialization functions. These configurations highly depend on the
    hardware setup.
*   chconf.h  
    Configuration header for the ChibiOS/RT system kernel. There are probably
    only very few configurations done here, since most settings depend on the
    content of aosconf.h and are handled module unspecifically in the
    `./modules/aos_chconf.h` file.
*   halconf.h  
    Configuration header for ChibiOS/HAL (hardware abstraction layer). Existing
    files are well documented and name all available settings. Please refer to
    ChibiOS for further details.
*   Makefile  
    The GNU make script to build and flash AMiRo-OS for the module.
*   mcuconf.h  
    Configuration file for ChibiOS/HAL to initialize the microcontroller (MCU).
    It is recommended to check the `./kernel/ChibiOS/demos/` directory for an
    example using the according MCU and copy the mcuconf.h from there. Depending
    on your hardware setup you may have to modify it nevertheless, though.
*   module.h & module.c  
    These files act as some sort of container, where all module specific aliases
    for interfaces and GPIOs, configurations, hooks, low-level drivers, and
    tests are defined. These are the most comprehensive files in the module
    folder.
*   <mcu\>.ld  
    Linker script, defining the memory layout and region aliases. It is
    recommended to check ChibiOS (`./kernel/ChibiOS/os/common/startup/`) whether
    a linker script for the according MCU already exists.

Since all these files are specific to the module hardware, you will have to
modify the contents according to your setup in a third step. Most settings are
described in detail within the configuration files, but for others you will have
to consult the datasheet of your MCU and even take a closer look at how certain
settings are used in other modules.

Finally, you need to build and flash the project. The compiler might even help
you getting everything set up correctly. Take the time needed to understand
compilation errors and warnings and get rid of all of those (warnings should not
be ignored since they are hints that something might be amiss and the program
will not act as intended).

As you will probably notice, for most modules there is an additional 'test/'
folder. This folder contains module specific wrapper code for tests (e.g. for
hardware devices). Since tests are not essential but a more advanced feature,
a separate guide describes how to write a test in section 4.5.

**Summing up, you have to**

1.  create a module directory.
2.  initialize all files (use an existing module or a ChibiOS demo as template).
3.  configure all files according to your hardware setup and preferences.
4.  compile, flash and check for issues.


4.2 Adding a Shell Command
--------------------------

Before going into the technical details, how a new shell command is initialized
and registered to a shell, some basic concepts of the AMiRo shell should be
covered first. Most fundamentally, although for most use cases a single shell
instance on a module will suffice, there can be an arbitrary number of shells.
Each shell runs in its own thread and has an exclusive list of shell commands.
That said, each shell command can be registered to only one (or none) shell.  
Another important aspect of the AMiRo shell are the I/O streams. Each shell
reads and writes from/to a shell stream. Such a stream may again contain an
arbitrary number of channels. Whilst only one of those channels can be selected
as input, each and all channels can be configured as output. As a result, if a
hardware module features multiple I/O interfaces, according configuration of the
shell stream and its channels, allows to still use only a single shell instance.
If not disabled in the aosconf.h file, AMiRo-OS already runs a system shell in
a thread with minimum priority.

Depending on the configuration, several commands are registered to the system
shell by default (e.g. `kernel:test`, `module:info`), which are defined in the
AMiRo-OS core. In order to add additional custom command, those should be
defined in the module.h and module.c files. First you need to _declare_ the
shell command - an instance of the memory structure representing a command - in
the module.h file. Second, you have to _define_ that structure in the module.c
file via the `AOS_SHELL_COMMAND(var, name, callback)` macro function. This macro
takes three arguments:

1.  `var`  
    Name of the variable (must be identical to the _declaration_).
2.  `name`  
    Command string which will be shown and used in the shell. By convention,
    command names follow a colon notation, e.g. `module:info`, where the first
    part denotes the scope of the command (e.g. kernel, module, tests, etc.) and
    the second part specifies the command in this scope.
3.  `callback`  
    Callback function to be executed by the command.

The callback function is typically defined right before the
`AOS_SHELL_COMMAND()` macro is called and should be a mere wrapper, calling
another function. Keep in mind, though, that thos callback are executed within
the shell thread and thus inherit its (typically very low) priority and there is
no way to calling a command in a non-blocking manner.

Finally, you have to register the command to a shell. This is very important and
a common mistake, but naturally, a shell can only access commands, which are
known to it. Registration is done via the `aosShellAddCommand()` function,
preferably before the shell thread is started. Since test commands are the most
common use case, AMiRo-OS provides the hook `MODULE_INIT_TESTS()`, which is
defined in each module.h file.

**Summing up, you have to**

1.  decllare and define a command.
2.  implement a callback function.
3.  register the command to a shell.


4.3 Handling a Custom I/O Event in the Main Thread
--------------------------------------------------

In order to handle custom I/O events in the main thread, AMiRo-OS offers several
hooks to be used. First of all, you need to configure and enable the interrupt
for the according GPIO. This can be done by implementing the
`MODULE_INIT_INTERRUPTS()` hook in the module.h file. For information how to use
this hook, please have a look at existing modules. In the end, the interrupt
callback functions has to emit an I/O event with the according bit in the flags
mask set (such as the `_gpioCallback()` function in `./core/src/aos_system.c`).
As result, whenever a rising or falling edge (depends on configuration) is
detected on that particular GPIO, the interrupt service routine is executed and
hence an I/O event is emitted, which can be received by any thread in the
system.

Next, you have to explicitely whitelist the event flag for the main thread,
because by default it ignores all I/O events other than power down and such.
This is done via the optional `AMIROOS_CFG_MAIN_LOOP_GPIOEVENT_FLAGSMASK` macro,
which should be defined in the module.h file, for example:

    #define AMIROOS_CFG_MAIN_LOOP_GPIOEVENT_FLAGSMASK         \
            (AOS_GPIOEVENT_FLAG(padX) | AOS_GPIOEVENT_FLAG(padY) | AOS_GPIOEVENT_FLAG(padZ))

When `AMIROOS_CFG_MAIN_LOOP_GPIOEVENT_FLAGSMASK` has been defined correctly, the
main thread will be notified by the according events and execute its event
handling routine. Hence you have to implement another macro in module.h to
handle the custom event(s) appropriately:
`MODULE_MAIN_LOOP_GPIOEVENT(eventflags)`. As you can see, the variable
`eventflags` is propagated to the hook. This variable is a mask, that allows to
identify the GPIO pad(s), which caused the event, by the individually set bits.
Following the example above, you can check which GPIOs have caused events by
using if-clauses in the implementation of the hook:

    #define MODULE_MAIN_LOOP_GPIOEVENT(eventflags) {          \
      if (eventflags & AOS_GPIOEVENT_FLAG(padX)) {            \
        /* handle event */                                    \
      }                                                       \
      if (eventflags & (AOS_IOEVENT_FLAG(padY) |              \
            AOS_GPIOEVENT_FLAG(padZ))) {                      \
        /* handle combined event */                           \
      }                                                       \
    }

**Summing up, you have to**

1.  configure and enable the GPIO interrupt.
2.  define the AMIROOS_CFG_MAIN_LOOP_GPIOEVENT_FLAGSMASK macro.
3.  implement the MODULE_MAIN_LOOP_GPIOEVENT(eventflags) hook.


4.4 Implementing a Low-Level Driver
-----------------------------------

In the AMiRo-OS framework, low-level drivers are located in the additional Git
project AMiRo-LLD, which is included in AMiRo-OS as Git submodule at
`./periphery-lld/AMiRo-LLD/` and acts similar to a static library. When adding a
new low-level driver to the framework, you first have to implement it of course.
For details how to do so, please following the instructions givne in the
`README.md` file in the AMiRo-LLD root directory.

Now that the new driver is available, it can be enbled by simply including the
driver's makefile script in the makefile of the module, you are working on. In
order to make actuale use of the driver, you have to add according memory
structures to the module.h and module.c files - just have a look at existing
modules how this is done. In some cases you will have to configure additional
interrupts and/or alter the configuration of a communication interface
(e.g. I2C). Once again, you should take a look at existing modules and search
the module.h for the hooks `MODULE_INIT_INTERRUPTS()`,
`MODULE_INIT_PERIPHERY_IF()` and `MODULE_SHUTDOWN_PERIPHERY_IF()`.

Finally, you will probably want to validate your implementation via a test. How
this can be done is explained in detail in the next guide.

**Summing up, you have to**

1.  implement the driver in AMiRo-LLD using periphAL only.
2.  add the driver to a module (Makefile, module.h and module.c).
3.  configure interrupts and interfaces as required.
4.  write a test to verify your setup.


4.4 Writing a Test
------------------

AMiRo-OS provides a test framework for conventient testing and the ability to
opt-out all tests via the aosconf.h configuration file. There is also a
dedicated folder, where all test code belongs to. In case you want to implement
a test for a newly developed low-level driver, you should have a look at the
folder `./test/periphery-lld/`. As with the low-level drivers, tests are placed
in individual subfolders (e.g. `./test/periphery-lld/DEVICE1234_v1`) and all
files should use the prefix `aos_test_` in their name. Moreover, all code must
be fenced by guards that disable it completely if the `AMIROOS_CFG_TESTS_ENABLE`
flag is set to false in the aosconf.h configuration file.

Now you have to add the test to a specific module. Therefore, you should create
a `test/` directory in the module folder, if such does not exist yet. In this
directory, you create another subfolder, e.g. `DEVICE1234/` and three additional
files in there:

*   module_test_DEVICE1234.mk
*   module_test_DEVICE1234.h
*   module_test_DEVICE1234.c

The makefile script is not required, but recommended to achieve maintainable
code. This script file should add the folder to the `MODULE_INC` variable and
all C source files to `MODULE_CSRC`. The header and source files furthermore
define module specific data structures and a test function. In order to clearly
indicate that these files are module specific wrappers, their names should begin
with the `module_test_` prefix.

In order to be able to call such test function as a command via the AMiRo-OS
shell, you need to add an according shell command to the module.h and module.c
files. Whereas the command itself is typically very simple, just calling the
callback function defined in the `./test/DEVICE1234/module_test_DEVICE1234.h`/
`.c` files, you have to add the command to a shell. In order to make the command
available in a shell so a user can run it, it has to be associated with the
shell. AMiRo-OS provides the hook `MODULE_INIT_TESTS()` for this purpose, which
has to be implemented in the module.h file. Once again it is recommended to have
a look at an existing module, how to use this hook. Furthermore, there is more
detailled guide on adding shell commands.

**Summing up, you have to**

1.  implement the common test in the `./test/` folder.
2.  implement a module specific wrapper in the `./modules/<module>/test/`
    folder.
3.  associate the shell command to a shell via the `MODULE_INIT_TESTS()` hook in
    module.h.

