/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <amiroos.h>
#include <aos_test_MPU6050.h>

#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

aos_testresult_t aosTestMpu6050Func(BaseSequentialStream* stream, const aos_test_t* test)
{
    aos_testresult_t result;

    uint8_t data[1] = {0};
    int32_t status;

  aosTestResultInit(&result);

    status = mpu6050_lld_read_register(((aos_test_mpu6050data_t*)test->data)->mpu, MPU6050_LLD_WHO_AM_I, data, 1, ((aos_test_mpu6050data_t*)test->data)->timeout);
    if (status == APAL_STATUS_OK && data[0] == MPU6050_I_AM) {
      aosTestPassed(stream, &result);
    } else {
      aosTestFailedMsg(stream, &result, "0x%08X | 0x%02X\n", status, data[0]);
    }

  return result;
}

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */
