/*
AMiRo-OS is an operating system designed for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2016..2020  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <amiroos.h>
#include <aos_test_TPS6211x_INA219.h>

#if (AMIROOS_CFG_TESTS_ENABLE == true) || defined(__DOXYGEN__)

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

aos_testresult_t aosTestTps6211xIna219Func(BaseSequentialStream *stream, const aos_test_t* test)
{
  aosDbgCheck(test->data != NULL && ((aos_test_tps6211xina219data_t*)test->data)->tps6211x != NULL && ((aos_test_tps6211xina219data_t*)test->data)->ina219 != NULL);

  // local variables
  aos_testresult_t result;
  int32_t status;
  tps6211x_lld_power_en_t power;
  uint32_t v_buson, v_busoff;

  aosTestResultInit(&result);

  chprintf(stream, "read pin...\n");
  status = tps6211x_lld_get_power_en(((aos_test_tps6211xina219data_t*)test->data)->tps6211x, &power);
  aosThdSSleep(1);
  status |= ina219_lld_read_bus_voltage(((aos_test_tps6211xina219data_t*)test->data)->ina219, &v_buson, ((aos_test_tps6211xina219data_t*)test->data)->timeout);
  if (status == APAL_STATUS_SUCCESS && power == TPS6211x_LLD_POWER_ENABLED) {
    aosTestPassedMsg(stream, &result, "enabled, %fV\n", (float)v_buson / 1000000.0f);
  } else {
    aosTestFailedMsg(stream, &result, "0x%08X\n", status);
  }

  chprintf(stream, "write pin...\n");
  status = tps6211x_lld_set_power_en(((aos_test_tps6211xina219data_t*)test->data)->tps6211x, TPS6211x_LLD_POWER_DISABLED);
  status |= tps6211x_lld_get_power_en(((aos_test_tps6211xina219data_t*)test->data)->tps6211x, &power);
  aosThdSSleep(1);
  status |= ina219_lld_read_bus_voltage(((aos_test_tps6211xina219data_t*)test->data)->ina219, &v_busoff, ((aos_test_tps6211xina219data_t*)test->data)->timeout);
  status |= tps6211x_lld_set_power_en(((aos_test_tps6211xina219data_t*)test->data)->tps6211x, TPS6211x_LLD_POWER_ENABLED);
  if (status == APAL_STATUS_SUCCESS && power == TPS6211x_LLD_POWER_DISABLED) {
    chprintf(stream, "\tdisabled, %fV\n", (float)v_busoff / 1000000.0f);
    if (v_buson > v_busoff) {
      aosTestPassed(stream, &result);
    } else {
      aosTestFailedMsg(stream, &result, "on: %fV; off: %fV\n", (float)v_buson / 1000000.0f, (float)v_busoff / 1000000.0f);
    }
  } else {
    aosTestFailedMsg(stream, &result, "0x%08X\n", status);
  }

  return result;
}

#endif /* (AMIROOS_CFG_TESTS_ENABLE == true) */
